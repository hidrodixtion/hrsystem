/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Keyboard,
    ToastAndroid,
    FlatList
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner, Card, CardItem } from 'native-base';
import { getStaffData, getPic, saveEditSalary } from "./API";
import { ListView } from "deprecated-react-native-listview";
import _ from 'lodash';

type Props = {};
export default class AL_Details extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            st: this.props.def,
            isFocused: false,
            resultBox: 50,
            query: this.props.applicant,
            empty: true,
            loaded: false,
            data: this.props.data
        };
        console.log("now1 :", this.state.pickedMonth)
    };

    monthFormatter2 = (date) => {
        month = date.split('-')[1];
        if (month == '01') {
            month = "Jan";
        } else if (month == '02') {
            month = "Feb";
        } else if (month == '03') {
            month = "March";
        } else if (month == '04') {
            month = "April";
        } else if (month == '05') {
            month = "May";
        } else if (month == '06') {
            month = "June";
        } else if (month == '07') {
            month = "July";
        } else if (month == '08') {
            month = "Aug";
        } else if (month == '09') {
            month = "Sept";
        } else if (month == '10') {
            month = "Oct";
        } else if (month == '11') {
            month = "Nov";
        } else if (month == '12') {
            month = "Dec";
        }

        return date.split('-')[2] + ' ' + month + ' ' + date.split('-')[0];
    };

    monthFormatter = (full_date) => {
        date = full_date.split(' ')[0];
        month = date.split('-')[1];
        if (month == '01') {
            month = "Jan";
        } else if (month == '02') {
            month = "Feb";
        } else if (month == '03') {
            month = "March";
        } else if (month == '04') {
            month = "April";
        } else if (month == '05') {
            month = "May";
        } else if (month == '06') {
            month = "June";
        } else if (month == '07') {
            month = "July";
        } else if (month == '08') {
            month = "Aug";
        } else if (month == '09') {
            month = "Sept";
        } else if (month == '10') {
            month = "Oct";
        } else if (month == '11') {
            month = "Nov";
        } else if (month == '12') {
            month = "Dec";
        }

        return date.split('-')[2] + ' ' + month + ' ' + date.split('-')[0] + ' ' + full_date.split(' ')[1];
    };

    completeString(str) {
        let completed = '';
        if (str == 'final_reject' || str == 'reject') {
            completed = 'rejected.';
        } else if (str == 'approve' || str == 'final_approve') {
            completed = 'approved.';
        } else if (str == 'draft') {
            completed = 'saved as draft.';
        } else if (str == 'submit') {
            completed = 'submitted, now waiting for approval.';
        }

        return completed;
    };

    editable = (str) => {
        console.log(str);
        return str == 'reject' || str == 'draft' || str == 'final_reject';
    };

    edit_AL = (item) => {
        this.props.navigator.push({
            screen: "EditAL", // unique ID registered with Navigation.registerScreen
            passProps: {
                item: item
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                    <Body>
                        <Title>
                            Details
                    </Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => { this.props.navigator.pop() }}>
                            <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                        </Button>
                    </Right>
                </Header>
                <View style={{ flex: 0.918 }}>
                    <ScrollView style={{ paddingHorizontal: 10 }}>
                        <View style={{ width: '100%', paddingLeft: 40, marginTop: 40 }}>
                            <Text style={{ fontSize: 30, color: 'grey' }}>
                                STATUS
                            </Text>
                            <Text style={{ color: this.props.statusFormatter(this.props.item.LAST_STATUS) == 'Approved' ? 'green' : '#ef5350' }}>
                                {this.props.statusFormatter(this.props.item.LAST_STATUS)}
                            </Text>
                        </View>
                        <View style={{ width: '100%', paddingLeft: 40, marginTop: 40 }}>
                            <Text style={{ fontSize: 30, color: 'grey' }}>
                                DATE CREATED
                            </Text>
                            <Text>
                                {this.monthFormatter(this.props.item.CREATE_DATE)}
                            </Text>
                        </View>
                        <View style={{ width: '100%', paddingLeft: 40, marginTop: 40 }}>
                            <Text style={{ fontSize: 30, color: 'grey' }}>
                                CREATED BY
                            </Text>
                            <Text>
                                {this.props.item.CREATE_NAME}
                            </Text>
                        </View>
                        <View style={{ width: '100%', paddingLeft: 40, marginTop: 40 }}>
                            <Text style={{ fontSize: 30, color: 'grey' }}>
                                TYPE
                            </Text>
                            <Text>
                                {this.props.item.NOTE == 'cl' ? 'COMPENSATION LEAVE (CL)' : 'ANNUAL LEAVE (AL)'}
                            </Text>
                        </View>
                        <View style={{ width: '100%', paddingLeft: 40, marginTop: 40 }}>
                            <Text style={{ fontSize: 30, color: 'grey' }}>
                                APPLICANT
                            </Text>
                            <Text>
                                {this.props.item.APPLY_NAME}
                            </Text>
                            <Text>
                                {this.props.item.APPLY_POS_NAME}
                            </Text>
                        </View>
                        <View style={{ width: '100%', paddingLeft: 40, marginTop: 40 }}>
                            <Text style={{ fontSize: 30, color: 'grey' }}>
                                PURPOSE
                            </Text>
                            <Text>
                                {this.props.item.PURPOSE}
                            </Text>
                        </View>
                        <View style={{ width: '100%', paddingLeft: 40, marginTop: 40, marginBottom: 40 }}>
                            <Text style={{ fontSize: 30, color: 'grey' }}>
                                DURATION
                            </Text>
                            <Text>
                                {this.monthFormatter2(this.props.item.START)} until {this.monthFormatter2(this.props.item.END)} ({this.props.item.DURATION} {this.props.item.DURATION == '0' || this.props.item.DURATION == '1' ? 'day' : 'days'})
                            </Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={{ flex: 0.082, flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        <Button full style={{ backgroundColor: 'grey' }} onPress={() => this.props.navigator.pop()}>
                            <Text>Close</Text>
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

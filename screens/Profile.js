import React, { Component } from 'react';
import { Container, Header, Content, Button, Text, Form, Body, Left, Right, Item, Input, Label, Icon, Thumbnail } from 'native-base';
import { getStaffData, getAnnouncement, getPic, getNotif, getCL, edit_profile } from "./API";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    StatusBar,
    Alert,
    AsyncStorage,
    KeyboardAvoidingView,
    ActivityIndicator
} from 'react-native';
import ImageView from 'react-native-image-view';
import RNFS from 'react-native-fs';
import BouncyDrawer from 'react-native-bouncy-drawer';
import _ from 'lodash';
import ImagePicker from 'react-native-image-picker';

export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filePath: '',
            visible: false,
            data_user: [],
            privilege: [],
            username: '',
            oldpass: '',
            newpass: '',
            user: {},
            cl_count: 0,
            notifCount: 0,
            hasImage: false,
            isWaiting: false
        };
        console.log("now1 :", this.state.pickedMonth);
    };

    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        this.getData(this.state.user.id);
    };

    pushFileExplorer = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchImageLibrary(options, (response) => {
            console.log("Response = " + response);
            if (response.fileSize > 2097152) {
                Alert.alert('Warning!', 'File size is too big', [{ text: 'OK' }]);
                return;
            }

            if (response.uri) {
                this.setState({
                    filePath: response.path,
                    myPic: response,
                    hasImage: true
                });
            }
        });
    };

    getData = async (id) => {
        let formData = new FormData();
        /*
        formData.append('username','9114139KP');
        formData.append('password','pesan2gelas');
        */
        console.log('FORM : ' + this.state.user.code + '  ' + this.state.user.token);
        formData.append('id', id);
        //formData.append('password', pass);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" SALARY : " + (JSON.stringify(responseJson)));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data_user: responseJson.DATA[0],
                        privilege: responseJson.DATA[0].PRIVILEGE,
                        username: responseJson.DATA[0].CODE,
                        myPic: { uri: getPic(responseJson.DATA[0].PHOTO + '?' + new Date()) },
                        hasImage: true
                    });
                    console.log("data diri " + JSON.stringify(this.state.data_user));
                    //console.log("DATA AL : ", this.state.data['CURRENT AL'][0].QUOTA)
                }
                this.getNotifCount(this.state.data_user.ID);
            })
            .catch((error) => {
                console.error(error);
            });
    };


    onFilePicked = async (myPath) => {
        let fileExt = myPath.substr(myPath.lastIndexOf('.')).toLowerCase();
        RNFS.stat(myPath).then((result) => {
            console.log(result);
            if (result.size > 2097152) {
                Alert.alert(
                    'Warning!',
                    'File size is too big',
                    [
                        { text: 'OK' },
                    ]
                );
                return;
            }
        }
        );
        if (fileExt == '.png' || fileExt == '.jpg' || fileExt == '.jpeg') {
            await this.setState({
                filePath: myPath
            });
        } else {
            Alert.alert('Failed!', 'File chosen is not image type. Allowed type is .png, .jpg, .jpeg, and .tiff')
        }
        console.log("NOWFILE : " + myPath.substr(myPath.lastIndexOf('.')));
    };

    submitEdit = async () => {
        this.setState({
            isWaiting: true
        });
        let uname = 0;
        let formData = new FormData();
        formData.append('staff', this.state.data_user.ID);
        formData.append('username', this.state.username);
        if (this.state.username != this.state.user.code) {
            uname = 1;
        }
        if (this.state.newpass.length != 0) {
            if (this.state.oldpass != this.state.user.pass) {
                Alert.alert('Failed!', 'Wrong old password', [{ text: 'OK' }]);
                return;
            } else if (this.state.newpass == this.state.user.pass) {
                Alert.alert('Failed!', 'New password cannot be same with old password', [{ text: 'OK' }]);
                return;
            } else {
                formData.append('old_password', this.state.oldpass);
                formData.append('new_password', this.state.newpass);
            }
        }
        if (this.state.filePath.length != 0) {
            formData.append('userfile', {
                uri: 'file://' + this.state.filePath,
                type: 'image/foo',
                name: 'testFile' + this.state.filePath.substr(this.state.filePath.lastIndexOf('.'))
            });
        }
        console.log("FORM : " + JSON.stringify(formData));
        fetch(edit_profile(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" BERHASIL? " + ((JSON.stringify(responseJson))));
                if (responseJson.msg.toLowerCase() == 'ok') {
                    try {
                        await AsyncStorage.setItem('user_code', this.state.username);
                        if (this.state.newpass.length != 0 && this.state.oldpass.length != 0) {
                            await AsyncStorage.setItem('user_pass', this.state.newpass);
                        }
                        this.refresh();
                        this.showSnackbar();
                        console.log("FINISH")
                    } catch (error) {
                        // Error saving data
                    }
                } else {
                    if (uname == 1) {
                        try {
                            await AsyncStorage.setItem('user_code', this.state.username);
                            console.log("FINISH")
                        } catch (error) {
                            // Error saving data
                        }
                        uname = 0;
                        this.showSnackbar();
                        this.refresh();
                    }
                    Alert.alert('Failed!', "Please try again later", [{ text: 'OK' }])
                }
                this.setState({ isWaiting: false });
            })
            .catch((error) => {
                console.error(error);
                this.setState({ isWaiting: false });
            });
    };

    refresh = () => {
        this.props.navigator.resetTo({
            screen: 'Profile', // unique ID registered with Navigation.registerScreen
            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the pushed screen (optional)
        });
    };

    showSnackbar = () => {
        this.props.navigator.showSnackbar({
            text: 'Successfully saved',
            actionText: 'X', // optional
            actionId: 'fabClicked', // Mandatory if you've set actionText
            actionColor: 'white', // optional
            textColor: 'white', // optional
            backgroundColor: '#66BB6A', // optional
            duration: 'short' // default is `short`. Available options: short, long, indefinite
        });
    };

    getCLNotifCount = async (id) => {
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('action', 'cl');
        formData.append('seen', false);
        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notifcl " + (responseJson.DATA));
                if (responseJson.msg.toLowerCase() == "ok" || responseJson.msg.toLowerCase() != 'empty') {
                    this.setState({
                        cl_count: _.size(responseJson.DATA)
                    })
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };
    getNotifCount = async (id) => {
        await this.getCLNotifCount(id);
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('action', 'al');
        formData.append('seen', false);
        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notif " + ((responseJson.DATA)));
                if (responseJson.msg.toLowerCase() == "ok" || responseJson.msg.toLowerCase() != 'empty') {
                    this.setState({
                        notifCount: (_.size(responseJson.DATA) + this.state.cl_count)
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    goLogOut = async () => {
        let keys = ['user_token', 'user_code', 'user_pass', 'user_id'];
        await AsyncStorage.multiRemove(keys, (err) => { });
        this.props.navigator.resetTo({
            screen: 'First', // unique ID registered with Navigation.registerScreen
            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the pushed screen (optional)
        });
    };

    popNotif = () => {
        this.props.navigator.push({
            screen: 'Notification', // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {}, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };

    changeScreen = (target) => {
        this.props.navigator.resetTo({
            screen: target, // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {
                privilege: this.state.privilege
            }, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
                screenBackgroundColor: '#FFF'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };

    renderContent = () => (
        <View style={{ backgroundColor: 'rgba(95, 95, 95,0.9)', flex: 1, alignItems: 'center', paddingTop: '15%' }}>
            <StatusBar translucent backgroundColor={'transparent'} />
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.changeScreen('Profile')}>
                        <Thumbnail style={{ width: 80, height: 80, borderRadius: 40 }}
                            resizeMode='cover'
                            source={this.state.myPic}
                            onError={() => {
                                this.setState({
                                    myPic: require('../images/no_image.png')
                                })
                            }}
                        />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Profile</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={this.popNotif}>
                        <Thumbnail large source={require('../images/bell.png')} />
                    </TouchableOpacity>
                    {
                        this.state.notifCount == 0 || this.state.notifCount == null ?
                            null
                            :
                            <Text style={{
                                position: 'absolute',
                                bottom: '17%',
                                width: 40,
                                height: 40,
                                backgroundColor: '#ef5350',
                                textAlign: 'center',
                                textAlignVertical: 'center',
                                borderRadius: 20,
                                right: '0%',
                                fontSize: 15,
                                color: 'white'
                            }} onPress={() => { this.popNotif() }}>
                                {this.state.notifCount}
                            </Text>
                    }
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Notification</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%', marginVertical: 20 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.changeScreen('Home') }}>
                        <Thumbnail large source={require('../images/dashboard.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Dashboard</Text>
                </View>

            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%', marginVertical: 20 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.changeScreen('TopTabs') }}>
                        <Thumbnail large source={require('../images/absence.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Absence</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => {
                            Alert.alert(
                                'Warning!',
                                'Are you sure want to log out?',
                                [
                                    { text: 'LOG OUT', onPress: () => this.goLogOut() },
                                    { text: 'CANCEL' }
                                ]
                            )
                        }}
                    >
                        <Thumbnail large source={require('../images/logout.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Log Out</Text>
                </View>
            </View>

        </View>
    );

    render() {
        let submitButton;
        if (this.state.isWaiting) {
            submitButton = <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View>
                    <ActivityIndicator color='#66BB6A' size='small' style={{ paddingRight: 10 }} />
                </View>
                <View>
                    <Text style={{ color: '#66BB6A' }}>Please Wait</Text>
                </View>
            </View>
        } else {
            submitButton = <Body>
                <Button full style={{ backgroundColor: '#66BB6A' }} onPress={() => this.submitEdit()}>
                    <Text>SUBMIT</Text>
                </Button>
            </Body>
        }

        return (
            <Container style={{ paddingTop: 80 }}>
                <KeyboardAvoidingView style={{ flex: 0.918 }} behavior="padding">
                    <Content>
                        <Form style={{ paddingRight: 10, flex: 1 }}>
                            <Item style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }} onPress={() => this.setState({ visible: true })}>
                                    <Thumbnail style={{ width: 150, height: 150 }} square
                                        source={this.state.hasImage ?
                                            this.state.myPic
                                            :
                                            {
                                                uri: '',//'file://' + this.state.filePath + '?' + new Date(),
                                                method: 'GET',
                                                headers: {
                                                    Pragma: 'no-cache',
                                                },
                                            }
                                        }
                                        resizeMode="cover"
                                        onError={() => {
                                            this.setState({
                                                myPic: require('../images/no_image.png')
                                            })
                                        }}
                                    />
                                </TouchableOpacity>
                                <ImageView
                                    images={[{
                                        source: {
                                            uri: this.state.hasImage ? getPic(this.state.data_user.PHOTO + '?' + new Date()) : '',
                                            method: 'GET',
                                            headers: {
                                                Pragma: 'no-cache',
                                            },
                                        },
                                        title: 'Profile',
                                        width: 806,
                                        height: 720,
                                    }]}
                                    imageIndex={0}
                                    animationType='fade'
                                    isVisible={this.state.visible}
                                />
                            </Item>
                            <View style={{ width: '100%', flexDirection: 'row', marginVertical: 20, paddingLeft: 15 }}>
                                <View style={{ marginRight: 10 }}>
                                    <TouchableOpacity iconLeft onPress={() => this.pushFileExplorer()}>
                                        <Icon name='attach' style={{ marginRight: 15 }} />
                                    </TouchableOpacity>
                                </View>
                                <ScrollView horizontal style={{ flex: 1, borderWidth: 1, borderColor: '#a6a6a6', paddingLeft: 10 }}>
                                    {
                                        this.state.filePath == '' ?
                                            <Text style={{ textAlignVertical: 'center', marginRight: 20, fontSize: 14 }}>
                                                Insert an image type file only. Max 2 MB
                                        </Text>
                                            :
                                            <Text style={{ textAlignVertical: 'center', marginRight: 20 }}>
                                                {this.state.filePath}
                                            </Text>
                                    }
                                </ScrollView>
                            </View>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ fontSize: 15, textAlign: 'left', alignSelf: 'flex-start' }}>
                                    Username
                            </Label>
                                <View style={{ width: '100%', height: 50, borderWidth: 1, borderColor: '#a6a6a6', justifyContent: 'flex-start' }}>
                                    <Input
                                        value={this.state.username}
                                        autoCapitalize="none"
                                        onChangeText={(text) => this.setState({
                                            username: text
                                        })}
                                    />
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ fontSize: 15, textAlign: 'left', alignSelf: 'flex-start' }}>
                                    Old Password
                            </Label>
                                <View style={{ width: '100%', height: 50, borderWidth: 1, borderColor: '#a6a6a6', justifyContent: 'flex-start' }}>
                                    <Input
                                        value={this.state.oldpass}
                                        onChangeText={(text) => this.setState({
                                            oldpass: text
                                        })}
                                        autoCapitalize="none"
                                        secureTextEntry={true}
                                        placeholder="Minimum of 5 alphanumeric only character, without whitespace"
                                    />
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ fontSize: 15, textAlign: 'left', alignSelf: 'flex-start' }}>
                                    New Password
                            </Label>
                                <View style={{ width: '100%', height: 50, borderWidth: 1, borderColor: '#a6a6a6', justifyContent: 'flex-start' }}>
                                    <Input
                                        autoCapitalize="none"
                                        value={this.state.newpass}
                                        secureTextEntry={true}
                                        onChangeText={(text) => this.setState({
                                            newpass: text
                                        })}
                                        placeholder="Minimum of 5 alphanumeric only character, without whitespace"
                                    />
                                </View>
                            </Item>
                        </Form>
                    </Content>
                </KeyboardAvoidingView>
                <View style={{ flex: 0.082, flexDirection: 'row' }}>
                    {submitButton}
                </View>
                <BouncyDrawer
                    willOpen={() => console.log('will open')}
                    didOpen={() => console.log('did open')}
                    willClose={() => console.log('will close')}
                    didClose={() => console.log('did close')}
                    title="HR System"
                    headerHeight={70}
                    titleStyle={{ color: '#fff', fontFamily: 'Helvetica Neue', fontSize: 20, marginLeft: -15, paddingTop: 20 }}
                    closedHeaderStyle={{ height: 80, backgroundColor: 'rgba(239,83,80,0.8)' }}
                    defaultOpenButtonIconColor="#fff"
                    defaultCloseButtonIconColor="#fff"
                    renderContent={this.renderContent}
                    openedHeaderStyle={{ backgroundColor: 'rgba(95, 95, 95,0.9)', paddingTop: 20 }}
                />
            </Container>
        );
    }
}
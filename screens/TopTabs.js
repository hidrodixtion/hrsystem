import React, { Component } from 'react';
import {
    Container,
    Header,
    Content,
    Button,
    Text,
    Form,
    Item,
    Input,
    Label,
    Body,
    Left,
    Right,
    Title,
    Icon,
    Tab,
    Tabs,
    Thumbnail
} from 'native-base';
import Tab1 from './Requested';
import Tab2 from './Submission';
import Tab3 from './ApprovedAL';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    StatusBar,
    FlatList,
    AsyncStorage,
    Alert
} from 'react-native';
import BouncyDrawer from 'react-native-bouncy-drawer';
import {
    getStaffData,
    getAnnouncement,
    getPic,
    getNotif,
    readNotif,
    getSalary,
    salarySlip,
    deleteSalarySlip
} from "./API";
import _ from 'lodash';

export default class TopTabs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initial: 0,
            data: [],
            user: {},
            data_user: [],
            myPic: {},
            privilege: {
                create_own_al: true,
                approve_all_al: true
            },
            notifCount: 0,
            cl_count: 0
        };
    };

    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        this.getData(this.state.user.id);
    };

    getData = async (id) => {
        let formData = new FormData();
        /*
        formData.append('username','9114139KP');
        formData.append('password','pesan2gelas');
        */
        console.log('FORM : ' + this.state.user.code + '  ' + this.state.user.token);
        formData.append('id', id);
        //formData.append('password', pass);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" dataku toptab" + (JSON.stringify(responseJson)));
                if (responseJson.msg.toLowerCase() === "ok") {
                    this.setState({
                        data_user: responseJson.DATA[0],
                        privilege: responseJson.DATA[0].PRIVILEGE,
                        myPic: { uri: getPic(responseJson.DATA[0].PHOTO + '?' + new Date()) }
                    });
                    console.log("datasaya", this.state.data_user);
                    console.log("data diri " + JSON.stringify(this.state.data_user.ORGANIZATION));
                    this.getNotifCount(this.state.user.id);
                    //console.log("DATA AL : ", this.state.data['CURRENT AL'][0].QUOTA)
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    popNotif = () => {
        this.props.navigator.push({
            screen: 'Notification', // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {}, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };

    changeScreen = (target) => {
        this.props.navigator.resetTo({
            screen: target, // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {
                privilege: this.state.privilege
            }, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
                screenBackgroundColor: '#FFF'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };

    goLogOut = async () => {
        let keys = ['user_token', 'user_code', 'user_pass', 'user_id'];
        await AsyncStorage.multiRemove(keys, (err) => {
        });
        this.props.navigator.resetTo({
            screen: 'First', // unique ID registered with Navigation.registerScreen
            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the pushed screen (optional)
        });
    };

    getCLNotifCount = async (id) => {
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('action', 'cl');
        formData.append('seen', false);
        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notifcl " + ((_.size(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok" || responseJson.msg.toLowerCase() != 'empty') {
                    this.setState({
                        cl_count: _.size(responseJson.DATA)
                    })
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };
    getNotifCount = async (id) => {
        await this.getCLNotifCount(id);
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('action', 'al');
        formData.append('seen', false);
        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notif " + ((responseJson.DATA)));
                if (responseJson.msg.toLowerCase() == "ok" || responseJson.msg.toLowerCase() != 'empty') {
                    this.setState({
                        notifCount: (_.size(responseJson.DATA) + this.state.cl_count)
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    renderContent = () => (
        <View style={{ backgroundColor: 'rgba(95, 95, 95,0.9)', flex: 1, alignItems: 'center', paddingTop: '15%' }}>
            <StatusBar translucent backgroundColor={'transparent'} />
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.changeScreen('Profile')}>
                        <Thumbnail style={{ width: 80, height: 80, borderRadius: 40 }}
                            resizeMode='cover'
                            source={this.state.myPic}
                            onError={() => {
                                this.setState({
                                    myPic: require('../images/no_image.png')
                                })
                            }}
                        />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Profile</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={this.popNotif}>
                        <Thumbnail large source={require('../images/bell.png')} />
                    </TouchableOpacity>
                    {
                        this.state.notifCount == 0 || this.state.notifCount == null ?
                            null
                            :
                            <Text style={{
                                position: 'absolute',
                                bottom: '17%',
                                width: 40,
                                height: 40,
                                backgroundColor: '#ef5350',
                                textAlign: 'center',
                                textAlignVertical: 'center',
                                borderRadius: 20,
                                right: '0%',
                                fontSize: 15,
                                color: 'white'
                            }} onPress={() => {
                                this.popNotif()
                            }}>
                                {this.state.notifCount}
                            </Text>
                    }
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Notification</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%', marginVertical: 20 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => {
                        this.changeScreen('Home')
                    }}>
                        <Thumbnail large source={require('../images/dashboard.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Dashboard</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%', marginVertical: 20 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => {
                        this.changeScreen('TopTabs')
                    }}>
                        <Thumbnail large source={require('../images/absence.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Absence</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => {
                            Alert.alert(
                                'Warning!',
                                'Are you sure want to log out?',
                                [
                                    { text: 'LOG OUT', onPress: () => this.goLogOut() },
                                    { text: 'CANCEL' }
                                ]
                            )
                        }}
                    >
                        <Thumbnail large source={require('../images/logout.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Log Out</Text>
                </View>
            </View>

        </View>
    );


    onChangeTab = ({ i, ref, from, }) => {
        ref.tab2.onEnter();
    };

    render() {
        console.log("Render TopTabs", this.state.privilege.create_own_al);
        return (
            <Container>
                <Tabs
                    onChangeTab={({ i, ref, from }) => {
                        console.log("refresh", i);
                        let tabArray = [this.tab1, this.tab2, this.tab3];
                        if (i < tabArray.length) {
                            if (!_.isNil(tabArray[i]))
                                tabArray[i].refresh();
                        }
                    }}
                    style={{ marginTop: 70 }} initialPage={0} prerenderingSiblingsNumber={Infinity}
                    tabBarUnderlineStyle={{ backgroundColor: '#ef5350' }}>
                    {
                        !this.state.privilege.create_own_al && !this.state.privilege.create_other_al && !this.state.privilege.approve_all_al && !this.state.privilege.approve_dept_al && !this.state.privilege.approve_special_organization ?
                            <Tab heading="-"
                                tabStyle={{ backgroundColor: 'white' }}
                                activeTabStyle={{ backgroundColor: 'white' }}
                                textStyle={{ color: 'black' }}
                                activeTextStyle={{ color: '#ef5350' }}
                            >
                                <View style={{ flex: 1 }}>
                                    <Image style={{
                                        width: 100,
                                        height: 100,
                                        alignSelf: 'center',
                                        marginTop: 20,
                                        opacity: 0.5
                                    }}
                                        source={require('../images/empty.png')} />
                                    <Text style={{ color: '#e0e0e0', fontSize: 15, alignSelf: 'center' }}>
                                        You don't have privilege to create your own AL
                                    </Text>
                                </View>
                            </Tab>
                            :
                            null
                    }
                    {
                        this.state.privilege.create_own_al || this.state.privilege.create_other_al ?
                            <Tab heading="Requested"
                                tabStyle={{ backgroundColor: 'white' }}
                                activeTabStyle={{ backgroundColor: 'white' }}
                                textStyle={{ color: 'black' }}
                                activeTextStyle={{ color: '#ef5350' }}
                            >
                                <Tab1
                                    ref={(ref) => this.tab1 = ref}
                                    navigator={this.props.navigator} privilege={this.state.privilege}
                                    data_user={this.state.data_user} />
                            </Tab>
                            :
                            null
                    }
                    {
                        this.state.privilege.approve_all_al || this.state.privilege.approve_dept_al || this.state.privilege.approve_special_organization ?
                            <Tab heading="Submission"
                                tabStyle={{ backgroundColor: 'white' }}
                                activeTabStyle={{ backgroundColor: 'white' }}
                                textStyle={{ color: 'black' }}
                                activeTextStyle={{ color: '#ef5350' }}

                            >
                                <Tab2 ref={(ref) => this.tab2 = ref} navigator={this.props.navigator} privilege={this.state.privilege} data_user={this.state.data_user} />
                            </Tab>
                            :
                            null
                    }
                    {
                        this.state.privilege.approve_all_al || this.state.privilege.approve_dept_al || this.state.privilege.approve_special_organization ?
                            <Tab heading="Approved"
                                tabStyle={{ backgroundColor: 'white' }}
                                activeTabStyle={{ backgroundColor: 'white' }}
                                textStyle={{ color: 'black' }}
                                activeTextStyle={{ color: '#ef5350' }}
                            >
                                <Tab3
                                    ref={(ref) => this.tab3 = ref}
                                    navigator={this.props.navigator} privilege={this.state.privilege} data_user={this.state.data_user} />
                            </Tab>
                            :
                            null
                    }
                </Tabs>
                <BouncyDrawer
                    willOpen={() => console.log('will open')}
                    didOpen={() => console.log('did open')}
                    willClose={() => console.log('will close')}
                    didClose={() => console.log('did close')}
                    title="HR System"
                    headerHeight={70}
                    titleStyle={{
                        color: '#fff',
                        fontFamily: 'Helvetica Neue',
                        fontSize: 20,
                        marginLeft: -15,
                        paddingTop: 20
                    }}
                    closedHeaderStyle={{ height: 80, backgroundColor: 'rgba(239,83,80,0.8)' }}
                    defaultOpenButtonIconColor="#fff"
                    defaultCloseButtonIconColor="#fff"
                    renderContent={this.renderContent}
                    openedHeaderStyle={{ backgroundColor: 'rgba(95, 95, 95,0.9)', paddingTop: 20 }}
                />
            </Container>
        );
    }
}
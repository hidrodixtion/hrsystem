/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Keyboard,
    ToastAndroid,
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner } from 'native-base';
import { getStaffData, getPic, saveEditSalary, submitNewSalary, insertAL, getQuota } from "./API";
import DatePicker from 'react-native-datepicker';
import ListView from "deprecated-react-native-listview";
import _ from 'lodash';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const date = new Date();
const getday = date.getDate();
const getyear = date.getFullYear();
const getmonth = date.getUTCMonth();
function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}

let years = Array.from(range(getyear - 10, getyear + 10, 1));
console.log("BULANKU : ", getmonth);
type Props = {};
export default class FilterAL extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            st: this.props.def,
            dataSource: ds.cloneWithRows(['row 1', 'row 2']),
            isFocused: false,
            resultBox: 50,
            query: '',
            empty: true,
            loaded: false,
            pickedQuota: 'al',
            myQuota: {},
            quota_AL: '',
            pickedStaffId: '',
            purpose: '',
            dateStart: "" + getday + '-' + this.monthFormatter(getmonth) + '-' + getyear,
            dateEnd: "",
            pickedStatus: '',
            pickedAmount: ''
        };
        console.log("now1 :", this.state.dateStart);
    };

    submitNewAL = async (status) => {
        let formData = new FormData();
        formData.append('purpose', this.state.purpose);
        formData.append('start', this.submitFormat(this.state.dateStart));
        formData.append('end', this.submitFormat(this.state.dateStart));
        formData.append('approve', this.state.user.id);
        formData.append('final', this.state.user.id);
        formData.append('apply', this.state.pickedStaffId);
        formData.append('status', status);
        console.log("FORM : " + JSON.stringify(formData));
        fetch(insertAL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" BERHASIL? " + ((JSON.stringify(responseJson))));
                if (responseJson.msg.toLowerCase() == 'ok') {
                    this.props.finish();
                    this.props.navigator.pop();
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    static capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    submitFormat = (date) => {
        day = date.split('-')[0];
        month = date.split('-')[1].toLowerCase();
        year = date.split('-')[2];
        if (day.length < 2) {
            day = '0' + day;
        }
        if (month == 'jan') {
            month = '01';
        } else if (month == 'feb') {
            month = '02';
        } else if (month == 'mar') {
            month = '03';
        } else if (month == 'apr') {
            month = '04';
        } else if (month == 'MAY') {
            month = '05';
        } else if (month == 'jun') {
            month = '06';
        } else if (month == 'jul') {
            month = '07';
        } else if (month == 'aug') {
            month = '08';
        } else if (month == 'sep') {
            month = '09';
        } else if (month == 'oct') {
            month = '10';
        } else if (month == 'nov') {
            month = '11';
        } else if (month == 'dec') {
            month = '12';
        }
        console.log('READY ' + year + '-' + month + '-' + day);
        return '' + year + '-' + month + '-' + day;
    };

    monthFormatter = (month) => {
        month = month + 1;
        if (month == '1') {
            month = "Jan";
        } else if (month == '2') {
            month = "Feb";
        } else if (month == '3') {
            month = "Mar";
        } else if (month == '4') {
            month = "Apr";
        } else if (month == '5') {
            month = "May";
        } else if (month == '6') {
            month = "June";
        } else if (month == '7') {
            month = "July";
        } else if (month == '8') {
            month = "Aug";
        } else if (month == '9') {
            month = "Sept";
        } else if (month == '10') {
            month = "Oct";
        } else if (month == '11') {
            month = "Nov";
        } else if (month == '12') {
            month = "Dec";
        }
        month = month.toLowerCase();
        return month;
    };

    filterName = (query) => {
        let names = this.state.rawData;
        const filterRes = names.filter(x => {
            return x.NAME.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        console.log("MYSIZE : " + _.size(filterRes));
        if (_.size(filterRes) != 0) {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: null
            });
        } else {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: true
            });
        }

    };

    fetchStaff = async () => {
        let formData = new FormData();
        formData.append('sort', 'name');
        formData.append('sort_by', 'asc');
        if (this.props.privilege.approve_all_al == false && this.props.privilege.approve_dept_al == true) {
            formData.append('organization', this.props.data_user.ORGANIZATION);
        }
        if (this.props.privilege.approve_special_organization == true) {
            formData.append('special_organization', 't');
        }
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" STAFF " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        dataSource: ds.cloneWithRows(responseJson.DATA),
                        rawData: responseJson.DATA,
                        loaded: true
                    });
                    this.filterName("");
                }
            })
            .catch((error) => {
                console.error(error);
            });
        console.log(this.state.dataSource);
    };

    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        this.fetchStaff();
        //this.fetchMyName();
        //this.get_Quota();
    };

    submitFilter = () => {
        console.log(
            'data-data : ' +
            this.state.pickedStaffId + ' ' +
            this.state.purpose + ' ' +
            this.state.dateStart + ' ' +
            this.state.dateEnd + ' ' +
            this.state.pickedStatus + ' ' +
            this.state.pickedAmount
        )
        this.props.from == 'submission' ?
            this.props.onFinish(this.state.pickedStaffId, this.state.purpose, this.state.dateStart, this.state.dateEnd, this.state.pickedAmount)
            :
            this.props.onFinish(this.state.pickedStaffId, this.state.purpose, this.state.dateStart, this.state.dateEnd, this.state.pickedStatus, this.state.pickedAmount);
    };

    render() {
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView behavior="padding" style={{ flex: 0.918 }}>
                    <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                        <Body>
                            <Title>
                                Filter
                            </Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => { this.props.navigator.pop() }}>
                                <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                            </Button>
                        </Right>
                    </Header>
                    <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps="handled" scrollEnabled={this.state.enableScrollViewScroll}>
                        <Form style={{ marginTop: 10, paddingRight: 20 }}>
                            <Item style={{ flexDirection: 'column' }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Applicant
                                </Label>
                                <ScrollView
                                    showsVerticalScrollIndicator={false}
                                    scrollEnabled={false}
                                    keyboardDismissMode='none'
                                    keyboardShouldPersistTaps="handled"
                                    style={{
                                        width: '100%',
                                        borderWidth: 1,
                                        borderColor: '#a6a6a6',
                                        height: this.state.resultBox,
                                    }}
                                >
                                    <View style={{ width: '100%', height: 50 }}>
                                        <Input
                                            onFocus={() => {
                                                this.setState({ enableScrollViewScroll: false });
                                                if (this.state.enableScrollViewScroll === false) {
                                                    this.setState({ enableScrollViewScroll: true });
                                                }
                                                this.setState({ resultBox: 230 })
                                            }}
                                            onBlur={() => {
                                                this.setState({
                                                    resultBox: 50,
                                                    enableScrollViewScroll: true
                                                })
                                            }}
                                            style={{ width: '100%', height: '100%' }}
                                            selectTextOnFocus={true}
                                            value={this.state.query}
                                            onChangeText={(text) => {
                                                console.log('textku : ' + text);
                                                this.setState({ query: text });
                                                this.filterName(text);
                                            }}
                                            getRef={(input) => this.searchBox = input}
                                            placeholder="Search"
                                        />
                                    </View>
                                    <View onStartShouldSetResponderCapture={() => {
                                        this.setState({ enableScrollViewScroll: false });
                                        if (this.state.enableScrollViewScroll === false) {
                                            this.setState({ enableScrollViewScroll: true });
                                        }
                                    }}>
                                        {
                                            this.state.empty && this.state.loaded ?
                                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 20 }}>No results found</Text>
                                                </View>
                                                :
                                                !this.state.loaded ?
                                                    <View style={{ width: '100%' }}>
                                                        <Spinner color='blue' />
                                                    </View>
                                                    :
                                                    <ListView
                                                        keyboardShouldPersistTaps='always'
                                                        style={{ height: 180, width: '100%' }}
                                                        enableEmptySections
                                                        dataSource={this.state.dataSource}
                                                        renderRow={(rowData) => (
                                                            <TouchableOpacity
                                                                style={{
                                                                    paddingVertical: 10,
                                                                    borderBottomWidth: 0.2,
                                                                    flexDirection: 'row'
                                                                }}
                                                                onPress={() => {
                                                                    this.setState({
                                                                        query: rowData.NAME,
                                                                        pickedStaffId: rowData.ID,
                                                                        resultBox: 50
                                                                    });
                                                                    Keyboard.dismiss()
                                                                }}
                                                            >
                                                                <Left style={{ flex: 0.3, paddingLeft: 15 }}>
                                                                    <Thumbnail source={{ uri: getPic(rowData.PHOTO) }} />
                                                                </Left>
                                                                <Body>
                                                                    <Text style={{ fontSize: 20, alignSelf: 'flex-start' }}>{rowData.NAME}</Text>
                                                                </Body>

                                                            </TouchableOpacity>
                                                        )}
                                                    />
                                        }
                                    </View>
                                </ScrollView>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Purpose
                                </Label>
                                <View style={{ width: '100%', height: 50, borderWidth: 1, justifyContent: 'flex-start' }}>
                                    <Input
                                        multiline={true}
                                        value={this.state.purpose}
                                        onChangeText={(text) => this.setState({ purpose: text })}
                                        style={{ width: '100%', height: '100%', textAlignVertical: 'top' }}
                                    />
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Leaving date (Start)
                                </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <DatePicker
                                        style={{ width: '100%' }}
                                        date={this.state.dateStart}
                                        mode="date"
                                        androidMode="spinner"
                                        placeholder="select date"
                                        format="DD-MMM-YYYY"
                                        minDate={"01-01-" + (getyear - 10)}
                                        maxDate={"31-12-" + (getyear + 10)}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => {
                                            let a = this.state.dateEnd.split('-');
                                            let b = new Date(a[2], this.inverseMonth(a[1]), a[0]);
                                            let c = date.split('-');
                                            let d = new Date(c[2], this.inverseMonth(c[1]), c[0]);
                                            console.log(b);
                                            console.log(d > b);
                                            d > b ?
                                                this.setState({
                                                    dateStart: date,
                                                    dateEnd: date
                                                })
                                                :
                                                this.setState({ dateStart: date });

                                            this.fetch_leaveDuration();
                                        }
                                        }
                                    />
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Leaving date (End)
                                </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <DatePicker
                                        style={{ width: '100%' }}
                                        date={this.state.dateEnd}
                                        mode="date"
                                        androidMode="spinner"
                                        placeholder="select date"
                                        format="DD-MMM-YYYY"
                                        minDate={"01-01-" + (getyear - 10)}
                                        maxDate={"31-12-" + (getyear + 10)}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => {
                                            let a = this.state.dateStart.split('-');
                                            let b = new Date(a[2], this.inverseMonth(a[1]), a[0]);
                                            let c = date.split('-');
                                            let d = new Date(c[2], this.inverseMonth(c[1]), c[0]);
                                            d < b ?
                                                this.setState({
                                                    dateStart: date,
                                                    dateEnd: date
                                                })
                                                :
                                                this.setState({ dateEnd: date })

                                            this.fetch_leaveDuration();
                                        }}
                                    />
                                </View>
                            </Item>
                            {
                                this.props.from == 'submission' ?
                                    null
                                    :
                                    <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                        <Label style={{ alignSelf: 'flex-start' }}>
                                            Status
                                        </Label>
                                        <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                            <Picker
                                                mode="dropdown"
                                                selectedValue={this.state.pickedStatus}
                                                onValueChange={(itemValue, itemIndex) => this.setState({ pickedStatus: itemValue })}>
                                                <Picker.Item label='All status' value='' />
                                                <Picker.Item label='Drafted' value='draft' />
                                                <Picker.Item label='Submitted' value='submit' />
                                                <Picker.Item label='Rejected' value='reject' />
                                                <Picker.Item label='Approved' value='approve' />

                                            </Picker>
                                        </View>
                                    </Item>
                            }
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Numbers of data to show
                                </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <Picker
                                        mode="dropdown"
                                        selectedValue={this.state.pickedAmount}
                                        onValueChange={(itemValue, itemIndex) => this.setState({ pickedAmount: itemValue })}>
                                        <Picker.Item label='All' value='' />
                                        <Picker.Item label='100' value='100' />
                                        <Picker.Item label='50' value='50' />
                                        <Picker.Item label='30' value='30' />
                                        <Picker.Item label='10' value='10' />
                                        <Picker.Item label='1' value='1' />

                                    </Picker>
                                </View>
                            </Item>
                        </Form>
                    </ScrollView>
                </KeyboardAvoidingView>
                <View style={{ flex: 0.082, flexDirection: 'row' }}>
                    <Left>
                        <Button full light style={{ borderWidth: 0.3, borderColor: '#a6a6a6' }} onPress={() => this.props.navigator.pop()}>
                            <Text>CANCEL</Text>
                        </Button>
                    </Left>
                    <Right>
                        <Button full style={{ backgroundColor: '#66BB6A' }} onPress={() => this.submitFilter()}>
                            <Text>Filter</Text>
                        </Button>
                    </Right>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Keyboard,
    ToastAndroid,
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner } from 'native-base';
import { getStaffData, getPic, saveEditSalary, submitNewSalary, insertCL } from "./API";
import ListView from "deprecated-react-native-listview";
import _ from 'lodash';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const date = new Date();
const getyear = date.getFullYear();
const getmonth = date.getUTCMonth();
function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}

let years = Array.from(range(getyear - 10, getyear + 10, 1));
console.log("BULANKU : ", getmonth);
type Props = {};
export default class NewCL extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            st: this.props.def,
            dataSource: ds.cloneWithRows(['row 1', 'row 2']),
            isFocused: false,
            resultBox: 50,
            query: '',
            empty: true,
            loaded: false,
            quota: '',
            myQuota: {},
            quota_AL: '',
            pickedStaffId: this.props.staffId,
            purpose: '',
            myName: '',
            remarks: '',
        };
    };

    submitNewCL = async () => {
        let formData = new FormData();
        formData.append('staff', this.state.pickedStaffId);
        formData.append('remark', this.state.remarks);
        formData.append('quota', this.state.quota);
        console.log("FORM : " + JSON.stringify(formData));
        fetch(insertCL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" BERHASIL? " + ((JSON.stringify(responseJson))));
                if (responseJson.msg.toLowerCase() == 'ok') {
                    this.props.finish();
                } else if (responseJson.msg.toLowerCase() == 'failed') {
                    Alert.alert(
                        'Failed',
                        responseJson.DATA,
                        [
                            { text: 'OK' }
                        ]
                    );
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    filterName = (query) => {
        let names = this.state.rawData;
        const filterRes = names.filter(x => {
            return x.NAME.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        console.log("MYSIZE : " + _.size(filterRes));
        if (_.size(filterRes) != 0) {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: null
            });
        } else {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: true
            });
        }

    };

    fetchStaff = async () => {
        let formData = new FormData();
        formData.append('sort', 'name');
        formData.append('sort_by', 'asc');
        console.log('MY STAFF ID ' + this.props.userData.organization);
        if (this.props.privilege.insert_dept_cl == true && this.props.privilege.insert_cl_all == false) {
            formData.append('organization', this.props.userData.organization);
        }
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" STAFF " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    if (this.props.privilege.create_own_al == false) {
                        for (let i = 0; i < responseJson.DATA.length; i++) {
                            // look for the entry with a matching `code` value
                            if (responseJson.DATA[i].NAME == this.state.query) {
                                // we found it
                                // obj[i].name is the matched result
                                delete responseJson.DATA[i];
                                this.setState({
                                    query: ''
                                });
                            }
                        }
                    }
                    this.setState({
                        dataSource: ds.cloneWithRows(responseJson.DATA),
                        rawData: responseJson.DATA,
                        loaded: true
                    });
                    this.filterName("");
                }
            })
            .catch((error) => {
                console.error(error);
            });
        console.log(this.state.dataSource);
    };

    fetchMyName = async () => {
        let formData = new FormData();
        formData.append('id', this.props.staffId);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    if (this.props.privilege.create_own_al != false) {
                        console.log('SIAPA SAYA : ' + JSON.stringify(responseJson.DATA[0].NAME));
                        this.setState({
                            query: responseJson.DATA[0].NAME,
                            myName: responseJson.DATA[0].NAME
                        })
                    } else {
                        this.setState({
                            query: '',
                            myName: responseJson.DATA[0].NAME
                        })
                    }
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        this.fetchStaff();
        this.fetchMyName();
    };

    render() {
        return (
            <View style={styles.container} onStartShouldSetResponderCapture={() => {
                this.setState({ enableScrollViewScroll: true });
            }}>
                <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                    <Body>
                        <Title>
                            New CL
                        </Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => { this.props.navigator.pop() }}>
                            <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                        </Button>
                    </Right>
                </Header>
                <KeyboardAvoidingView behavior='padding' style={{ flex: 0.918 }}>
                    <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps="handled" scrollEnabled={this.state.enableScrollViewScroll}>
                        <Form style={{ marginTop: 10, paddingRight: 20 }}>
                            <Item style={{ flexDirection: 'column' }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Applicant
                            </Label>
                                <ScrollView
                                    showsVerticalScrollIndicator={false}
                                    scrollEnabled={false}
                                    keyboardDismissMode='none'
                                    keyboardShouldPersistTaps="handled"
                                    style={{
                                        width: '100%',
                                        borderWidth: 1,
                                        borderColor: '#a6a6a6',
                                        height: this.state.resultBox,
                                    }}
                                >
                                    <View style={{ width: '100%', height: 50 }}>
                                        <Input
                                            onFocus={() => this.setState({ resultBox: 230 })}
                                            onBlur={() => this.setState({ resultBox: 50 })}
                                            style={{ width: '100%', height: '100%' }}
                                            selectTextOnFocus={true}
                                            value={this.state.query}
                                            onChangeText={(text) => {
                                                console.log('textku : ' + text);
                                                this.setState({ query: text });
                                                this.filterName(text);
                                            }}
                                            getRef={(input) => this.searchBox = input}
                                            placeholder="Search"
                                        />
                                    </View>
                                    <View onStartShouldSetResponderCapture={() => {
                                        this.setState({ enableScrollViewScroll: false });
                                        if (this.state.enableScrollViewScroll === false) {
                                            this.setState({ enableScrollViewScroll: true });
                                        }
                                    }}>
                                        {
                                            this.state.empty && this.state.loaded ?
                                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 20 }}>No results found</Text>
                                                </View>
                                                :
                                                !this.state.loaded ?
                                                    <View style={{ width: '100%' }}>
                                                        <Spinner color='blue' />
                                                    </View>
                                                    :
                                                    <ListView
                                                        keyboardShouldPersistTaps='always'
                                                        style={{ height: 180, width: '100%' }}
                                                        enableEmptySections
                                                        dataSource={this.state.dataSource}
                                                        renderRow={(rowData) => (
                                                            <Button transparent
                                                                style={{
                                                                    paddingVertical: 10,
                                                                    borderBottomWidth: 0.2,
                                                                    flexDirection: 'row'
                                                                }}
                                                                onPress={() => {
                                                                    this.setState({
                                                                        query: rowData.NAME,
                                                                        pickedStaffId: rowData.ID,
                                                                        resultBox: 50
                                                                    });
                                                                    Keyboard.dismiss()
                                                                }}
                                                            >
                                                                <Left style={{ flex: 0.3, paddingLeft: 15 }}>
                                                                    <Thumbnail source={{ uri: getPic(rowData.PHOTO) }} />
                                                                </Left>
                                                                <Body>
                                                                    <Text style={{ fontSize: 20, alignSelf: 'flex-start' }}>{rowData.NAME}</Text>
                                                                </Body>

                                                            </Button>
                                                        )}
                                                    />
                                        }
                                    </View>
                                </ScrollView>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ fontSize: 15, textAlign: 'left', alignSelf: 'flex-start' }}>
                                    Number of days to be add to CL quota
                            </Label>
                                <View style={{ width: '100%', height: 50, borderWidth: 1, borderColor: '#a6a6a6', justifyContent: 'flex-start' }}>
                                    <Input
                                        value={this.state.quota}
                                        keyboardType='numeric'
                                        onChangeText={(text) => this.setState({ quota: text })}
                                        style={{ width: '100%', height: '100%', textAlignVertical: 'top' }}
                                    />
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Remarks
                            </Label>
                                <View style={{ width: '100%', height: 150, borderWidth: 1, justifyContent: 'flex-start', borderColor: '#a6a6a6' }}>
                                    <Input
                                        multiline={true}
                                        value={this.state.remarks}
                                        onChangeText={(text) => this.setState({ remarks: text })}
                                        style={{ width: '100%', height: '100%', textAlignVertical: 'top' }}
                                    />
                                </View>
                            </Item>
                        </Form>
                    </ScrollView>
                </KeyboardAvoidingView>
                <View style={{ flex: 0.082, flexDirection: 'row' }}>
                    <Left>
                        <Button full light style={{ borderWidth: 0.3, borderColor: '#a6a6a6' }} onPress={() => this.props.navigator.pop()}>
                            <Text>CANCEL</Text>
                        </Button>
                    </Left>
                    <Right>
                        <Button full style={{ backgroundColor: '#66BB6A' }} onPress={() => this.submitNewCL()}>
                            <Text>SUBMIT</Text>
                        </Button>
                    </Right>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

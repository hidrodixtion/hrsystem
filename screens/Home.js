/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    PermissionsAndroid,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    StatusBar,
    Alert,
    AsyncStorage,
    AppRegistry,
    FlatList
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Icon, Thumbnail, Spinner, Left, Right, Body } from 'native-base';
import BouncyDrawer from 'react-native-bouncy-drawer';
import Drawer, { Message } from 'react-native-bottom-drawer';
import { getStaffData, getAnnouncement, getPic, getNotif, getCL, getLogin, getAL, getQuota, submitAbsence, getAbsence } from "./API";
import _ from 'lodash';
import moment from 'moment';
import HRSystemModule from '../nativebridge';

type Props = {};

export default class first extends Component<Props> {
    getTokenAndLog = async function () {
        console.log("issubmittingpresence", this.state.isSubmittingPresence)

        if (this.state.isSubmittingPresence) {
            return;
        }

        this.setState({
            isSubmittingPresence: true
        });

        HRSystemModule.sendPresence(() => {
            this.reloadStaffAbsence();

            this.setState({
                isSubmittingPresence: false
            });
        })

    };

    async componentDidMount() {
        // HRSystemModule.getWifiList(ssids => console.log("ssid: ", ssids));

        if (Platform.OS === 'android') {
            // ask for permission
            try {
                const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        'title': 'Wifi networks',
                        'message': 'We need your permission in order to find wifi networks'
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("Thank you for your permission! :)");
                    HRSystemModule.spawnService();
                } else {
                    console.log("You will not able to retrieve wifi available networks list");
                }
            } catch (err) {
                console.warn("some err : " + err)
            }
        } else {
            HRSystemModule.spawnService();
        }
    };

    async componentWillMount() {
        // this.makeNotif();

        var today = new Date();
        let month = parseInt(today.getMonth() + 1);
        let month2 = parseInt(today.getMonth() + 1);
        let year = parseInt(today.getFullYear());
        let hari = today.getDay();
        let hour = today.getHours();
        let minute = today.getMinutes();

        if (month == '1') {
            month = "JANUARY";
        } else if (month == '2') {
            month = "FEBRUARY";
        } else if (month == '3') {
            month = "MARCH";
        } else if (month == '4') {
            month = "APRIL";
        } else if (month == '5') {
            month = "MAY";
        } else if (month == '6') {
            month = "JUNE";
        } else if (month == '7') {
            month = "JULY";
        } else if (month == '8') {
            month = "AUGUST";
        } else if (month == '9') {
            month = "SEPTEMBER";
        } else if (month == '10') {
            month = "OCTOBER";
        } else if (month == '11') {
            month = "NOVEMBER";
        } else if (month == '12') {
            month = "DECEMBER";
        }

        if (hari == 0) {
            hari = 'SUNDAY';
        } else if (hari == 1) {
            hari = 'MONDAY';
        } else if (hari == 2) {
            hari = 'TUESDAY';
        } else if (hari == 3) {
            hari = 'WEDNESDAY';
        } else if (hari == 4) {
            hari = 'THURSDAY';
        } else if (hari == 5) {
            hari = 'FRIDAY';
        } else if (hari == 6) {
            hari = 'SATURDAY';
        }

        let greet = today.getHours();
        if (greet >= 5 && greet <= 11) {
            this.setState({ greeting: 'GOOD MORNING' });
        } else if (greet >= 12 && greet <= 18) {
            this.setState({ greeting: 'GOOD AFTERNOON' });
        } else {
            this.setState({ greeting: 'GOOD EVENING' });
        }

        let date = hari + ", " + today.getDate() + " " + month + " " + today.getFullYear();
        month2.length == 1 ? month2 = '0' + month2 : null;
        this.setState({
            today: date,
            today_api: today.getFullYear() + '-' + month2 + '-' + today.getDate(),
            time: hour + '.' + minute
        });

        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass
                    }
                });
                console.log('testerwill' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }

        let formData = new FormData();
        console.log('FORM : ' + this.state.user.code + '  ' + this.state.user.token);
        formData.append('code', this.state.user.code);
        //formData.append('password', pass);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" mitossss" + (JSON.stringify(responseJson)));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data: responseJson.DATA[0],
                        privilege: responseJson.DATA[0].PRIVILEGE,
                        myPic: { uri: getPic(responseJson.DATA[0].PHOTO + '?' + new Date()) }
                    });
                    console.log("SSS" + JSON.stringify(this.state.privilege));
                    HRSystemModule.saveStaffData(this.state.data.ID);
                    this.takeAnnouncement();
                    this.getNotifCount(this.state.data.ID);
                    this.saveId(this.state.data.ID);
                    //console.log("DATA AL : ", this.state.data['CURRENT AL'][0].QUOTA)
                    this.get_Quota(this.state.data.ID);
                    this.getCL_Quota(year, this.state.data.ID);
                    this.fetchAbsenceToday(this.dateFormatter(this.state.today_api));
                    this.getStaffAbsence(this.dateFormatter(this.state.today_api));
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    getCL_Quota = async (year, id) => {
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('year', '2018');
        console.log(JSON.stringify(formData));
        fetch(getCL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" CL_QUOTA " + ((responseJson.DATA)));
                if (responseJson.msg.toLowerCase() == "ok") {
                    console.log('QUOTA : ' + JSON.stringify(responseJson.DATA));

                    this.setState({
                        CL_quota: responseJson.DATA[0].TOTAL
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    saveId = async (id) => {
        try {
            await AsyncStorage.setItem('user_id', id);
            console.log("SSS" + id);
        } catch (error) {
            // Error saving data
        }
    };
    getCLNotifCount = async (id) => {
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('action', 'cl');
        formData.append('seen', false);
        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notifcl " + ((_.size(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        cl_count: _.size(responseJson.DATA)
                    })
                } else {
                    this.setState({
                        cl_count: 0
                    })
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };
    getNotifCount = async (id) => {
        await this.getCLNotifCount(id);
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('action', 'al');
        formData.append('seen', false);
        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notif " + ((responseJson.DATA)));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        notifCount: (_.size(responseJson.DATA) + this.state.cl_count)
                    });
                }

                if (responseJson.msg.toLowerCase() == 'empty') {
                    this.setState({
                        notifCount: 0
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    goLogOut = async () => {
        HRSystemModule.stopService();
        let keys = ['user_token', 'user_code', 'user_pass', 'user_id'];
        await AsyncStorage.multiRemove(keys, (err) => { });
        this.props.navigator.resetTo({
            screen: 'First', // unique ID registered with Navigation.registerScreen
            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the pushed screen (optional)
        });
    };

    takeAnnouncement = async () => {
        let formData = new FormData();
        formData.append('active', '1');
        fetch(getAnnouncement(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" announce " + (JSON.stringify(responseJson.DATA)));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({ announce: responseJson.DATA });
                    console.log('ANNOUNCE : ' + JSON.stringify(this.state.announce));
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    popNotif = () => {
        this.props.navigator.push({
            screen: 'Notification', // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {}, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };

    dateFormatter = (date) => {
        console.log(date);

        return date;
    };

    get_Quota = async (id) => {
        let formData = new FormData();
        formData.append('staff', id);
        fetch(getQuota(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    console.log('QUOTA : ' + JSON.stringify(responseJson.DATA.AL[0].QUOTA));
                    this.setState({
                        myQuota: responseJson.DATA,
                        quota_AL: responseJson.DATA.AL[0].QUOTA
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    reloadStaffAbsence = () => {
        this.setState({
            loadedStaff: false,
            dataStaff: []
        });
        this.getStaffAbsence(this.dateFormatter(this.state.today_api));
    }

    getStaffAbsence = async () => {
        console.log('masuksini')
        let formData = new FormData();
        var today = new Date()
        let year = today.getFullYear();
        let month = parseInt(today.getMonth() + 1);
        if (month < 10) {
            month = '0' + month;
        }
        let date = today.getDate();
        if (date < 10) {
            date = '0' + date;
        }
        let calender = year + '-' + month + '-' + date;
        formData.append('date', calender);
        console.log('' + 'getstaff');
        fetch(getAbsence(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" Staff today " + ((JSON.stringify(responseJson.DATA))));
                this.setState({
                    loadedStaff: true
                });
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        dataStaff: responseJson.DATA,
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };
    fetchAbsenceToday = async (today) => {
        let formData = new FormData();
        formData.append('start', today);
        formData.append('end', today);
        formData.append('status', 'approve');
        console.log(formData);
        fetch(getAL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" Absence today " + ((JSON.stringify(responseJson.DATA))));
                this.setState({
                    loadedAbsence: true
                });
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        dataAbsence: responseJson.DATA,
                    });
                } else {
                    this.setState({
                        emptyAbsence: true
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    changeScreen = (target) => {
        this.props.navigator.resetTo({
            screen: target, // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {
                privilege: this.state.privilege
            }, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
                screenBackgroundColor: '#FFF'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };
    openNewAL() {
        this.props.navigator.push({
            screen: "NewLeave", // unique ID registered with Navigation.registerScreen
            passProps: {
                staffId: this.state.data.ID,
                finish: this.refresh,
                privilege: this.state.privilege
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };

    renderContent = () => (
        <View style={{ backgroundColor: 'rgba(95, 95, 95,0.9)', flex: 1, alignItems: 'center', paddingTop: '15%' }}>
            <StatusBar translucent backgroundColor={'transparent'} />
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.changeScreen('Profile')}>
                        <Thumbnail style={{ width: 80, height: 80, borderRadius: 40 }}
                            resizeMode='cover'
                            source={this.state.myPic}
                            onError={() => {
                                this.setState({
                                    myPic: require('../images/no_image.png')
                                })
                            }}
                        />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Profile</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={this.popNotif}>
                        <Thumbnail large source={require('../images/bell.png')} />
                    </TouchableOpacity>
                    {
                        this.state.notifCount == 0 || this.state.notifCount == null ?
                            null
                            :
                            <Text style={{
                                position: 'absolute',
                                bottom: '17%',
                                width: 40,
                                height: 40,
                                backgroundColor: '#ef5350',
                                textAlign: 'center',
                                textAlignVertical: 'center',
                                borderRadius: 20,
                                right: '0%',
                                fontSize: 15,
                                color: 'white'
                            }} onPress={() => { this.popNotif() }}>
                                {this.state.notifCount}
                            </Text>
                    }
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Notification</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%', marginVertical: 20 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.changeScreen('Home') }}>
                        <Thumbnail large source={require('../images/dashboard.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Dashboard</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%', marginVertical: 20 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.changeScreen('TopTabs') }}>
                        <Thumbnail large source={require('../images/absence.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Absence</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => {
                            Alert.alert(
                                'Warning!',
                                'Are you sure want to log out?',
                                [
                                    { text: 'LOG OUT', onPress: () => this.goLogOut() },
                                    { text: 'CANCEL' }
                                ]
                            )
                        }}
                    >
                        <Thumbnail large source={require('../images/logout.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Log Out</Text>
                </View>
            </View>

        </View>
    );
    constructor(props) {
        super(props);
        this.state = {
            data: {
                NAME: 'Loading ...',
                PHOTO: 'default.png'
            },
            user: {},
            announce: [],
            greeting: "",
            notifCount: 0,
            privilege: {},
            quota_AL: "",
            CL_quota: '',
            cl_count: 0,
            macaddres: '',
            loadedAbsence: false,
            emptyAbsence: false,
            dataAbsence: [],
            loadedStaff: false,
            dataStaff: [],
            dataStaffname: [],
            isSubmittingPresence: false
        };
    }

    openAbsence = () => {
        this.props.navigator.push({
            screen: 'UserAbsence', // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {
                today: this.state.today_api
            }, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };


    render() {
        return (
            <Container style={{ zIndex: 2, backgroundColor: '#E6E6E6' }}>
                {
                    console.log('DATAKU : ' + (this.state.data.NAME))

                }

                <Content style={{ flex: 1, marginBottom: 45, backgroundColor: '#E6E6E6' }}>
                    <View style={{ marginTop: 80, paddingHorizontal: 10, borderBottomWidth: 5, borderBottomColor: '#ef5350', marginHorizontal: 10, marginBottom: 7, backgroundColor: '#FFF', paddingVertical: 10 }}>
                        <Text style={{ fontSize: 20, fontWeight: '500' }}>
                            {this.state.greeting}
                        </Text>
                        <Text style={{ fontSize: 18, fontWeight: '100' }} uppercase={true}>
                            {this.state.data.NAME}
                        </Text>
                        <Text style={{ fontSize: 14 }}>
                            {this.state.data.POSITION_NAME}
                        </Text>
                        <Text>
                            <Text style={{ fontSize: 15, color: '#a6a6a6' }}>
                                Remaining Leave Quota:
                        </Text>
                            <Text style={{ fontSize: 15, color: '#a6a6a6', fontWeight: 'bold' }}>
                                {'\t'}{this.state.quota_AL} AL{'\t'}
                            </Text>
                            <Text style={{ fontSize: 15, color: '#a6a6a6' }}>
                                {'\t'}and{'\t'}
                            </Text>
                            <Text style={{ fontSize: 15, color: '#a6a6a6', fontWeight: 'bold' }}>
                                {'\t'}{this.state.CL_quota} CL
                        </Text>
                        </Text>
                    </View>
                    <View style={{ paddingHorizontal: 10, paddingVertical: 10, marginHorizontal: 10, borderBottomWidth: 5, borderBottomColor: '#ef5350', backgroundColor: '#FFF' }}>
                        <Text style={{ fontSize: 20, fontWeight: '500' }}>
                            TODAY IS
                    </Text>
                        <Text style={{ fontSize: 18, fontWeight: '100' }}>
                            {this.state.today}
                        </Text>
                    </View>
                    <View style={{ paddingHorizontal: 10, marginTop: 7, marginHorizontal: 10, borderBottomWidth: 5, borderBottomColor: '#ef5350', flexDirection: 'column', flex: 1, marginBottom: 7, backgroundColor: '#FFF', paddingVertical: 10 }}>
                        <View>
                            <Text style={{ fontSize: 19, fontWeight: '500' }}>
                                USERS ABSENCE/LEAVE FOR TODAY
                        </Text>
                        </View>
                        {
                            this.state.loadedAbsence ?
                                this.state.emptyAbsence ?
                                    <View>
                                        <Image style={{ width: 100, height: 100, alignSelf: 'center', marginTop: 20, opacity: 0.5 }} source={require('../images/empty.png')} />
                                        <Text style={{ color: '#a6a6a6', fontSize: 15, alignSelf: 'center' }}>
                                            There is nothing to show
                                    </Text>
                                    </View>
                                    :
                                    (
                                        this.state.dataAbsence.map(item => {
                                            return (
                                                <View style={{
                                                    paddingVertical: 10,
                                                    borderBottomWidth: 0.2,
                                                    flexDirection: 'row'
                                                }}>
                                                    <Left style={{ flex: 0.25, paddingLeft: 15 }}>
                                                        <Thumbnail source={{ uri: getPic(item.APPLY_PHOTO) }} style={{ width: 50, height: 50, borderRadius: 25 }} />
                                                        {console.log("MY PHOTO " + item.APPLY_PHOTO)}
                                                    </Left>
                                                    <Body>
                                                        <Text style={{ fontSize: 17, alignSelf: 'flex-start' }}>{item.APPLY_NAME}</Text>
                                                    </Body>
                                                </View>
                                            )
                                        })
                                    )
                                :
                                <View>
                                    <Spinner color='blue' />
                                </View>
                        }
                        <View style={{ alignSelf: 'flex-end', marginTop: 10, paddingBottom: 5, backgroundColor: '#FFF' }}>
                            <Button rounded iconRight style={{ backgroundColor: '#ef5350' }} onPress={() => this.openAbsence()}>
                                <Text>More</Text>
                                <Icon name='md-arrow-dropright' />
                            </Button>
                        </View>
                    </View>
                    <View style={{ paddingHorizontal: 10, marginTop: 7, marginHorizontal: 10, borderBottomWidth: 5, borderBottomColor: '#ef5350', flexDirection: 'column', flex: 1, marginBottom: 7, backgroundColor: '#FFF', paddingVertical: 10 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 19, fontWeight: '500', flex: 1 }}>
                                TODAY PRESENCE STATUS
                            </Text>
                            <Button rounded iconCenter style={{ backgroundColor: '#66BB6A' }} onPress={() => this.reloadStaffAbsence()}>
                                <Icon name='md-refresh' />
                            </Button>
                        </View>
                        {
                            this.state.loadedStaff ?
                                this.state.dataStaff.map(staff => {
                                    return (
                                        <View style={{
                                            paddingVertical: 10,
                                            borderBottomWidth: 0.2,
                                            flexDirection: 'row'
                                        }}>
                                            <Body>
                                                <Text style={{ fontSize: 17, alignSelf: 'flex-start' }}>{staff.staff_name}</Text>
                                            </Body>
                                            <Right>
                                                {
                                                    staff.staff_status == 'Presence' ? (
                                                        <View style={{
                                                            alignSelf: 'flex-start',
                                                            backgroundColor: '#66BB6A',
                                                            borderRadius: 150,
                                                            padding: 10,
                                                            marginLeft: 20,
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                            alignSelf: 'center',
                                                        }}>
                                                            <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }}>
                                                                {staff.staff_status}
                                                            </Text>
                                                            <Text style={{ fontSize: 9, fontWeight: 'bold', color: 'white' }}>
                                                                First Seen {staff.staff_first_seen}
                                                            </Text>
                                                            <Text style={{ fontSize: 9, fontWeight: 'bold', color: 'white' }}>
                                                                Last Seen {staff.staff_last_seen}
                                                            </Text>
                                                        </View>
                                                    )
                                                        : staff.staff_status == 'Unknown' ? (<View style={{
                                                            alignSelf: 'flex-start',
                                                            backgroundColor: '#FF6347',
                                                            borderRadius: 100,
                                                            padding: 10,
                                                            marginLeft: 20,
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                            alignSelf: 'center',
                                                        }}>
                                                            <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }}>
                                                                {staff.staff_status}
                                                            </Text>
                                                        </View>)
                                                            : staff.staff_status == 'Leave for today' ? (<View style={{
                                                                alignSelf: 'flex-start',
                                                                backgroundColor: '#00BFFF',
                                                                borderRadius: 100,
                                                                padding: 10,
                                                                marginLeft: 20,
                                                                justifyContent: 'center',
                                                                alignItems: 'center',
                                                                alignSelf: 'center',
                                                            }}>
                                                                <Text style={{ fontSize: 12, textAlign: "center", fontWeight: 'bold', color: 'white' }}>
                                                                    Absence/Leave For Today
                                                </Text>
                                                            </View>)
                                                                : staff.staff_status == 'Half Day' ? (<View style={{
                                                                    alignSelf: 'flex-start',
                                                                    backgroundColor: '#00BFFF',
                                                                    borderRadius: 100,
                                                                    padding: 10,
                                                                    marginLeft: 20,
                                                                    justifyContent: 'center',
                                                                    alignItems: 'center',
                                                                    alignSelf: 'center',
                                                                }}>
                                                                    <Text style={{ fontSize: 12, textAlign: "center", fontWeight: 'bold', color: 'white' }}>
                                                                        Half Day Leave
                                                </Text>
                                                                    <Text style={{ fontSize: 9, fontWeight: 'bold', color: 'white' }}>
                                                                        First Seen {staff.staff_first_seen}
                                                                    </Text>
                                                                    <Text style={{ fontSize: 9, fontWeight: 'bold', color: 'white' }}>
                                                                        Last Seen {staff.staff_last_seen}
                                                                    </Text>
                                                                </View>)
                                                                    : <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'black' }}>
                                                                        {staff.staff_status}
                                                                    </Text>
                                                }

                                            </Right>
                                        </View>
                                    )
                                })
                                :
                                <View>
                                    <Spinner color='blue' />
                                </View>
                        }
                    </View>

                    <View style={{ paddingHorizontal: 2, marginHorizontal: 10, borderBottomWidth: 5, borderBottomColor: '#ef5350', flexDirection: 'column', flex: 1, marginBottom: 7, backgroundColor: '#FFF', paddingVertical: 10 }}>
                        <View style={{ alignSelf: 'center', marginTop: 10, paddingBottom: 5, backgroundColor: '#FFF', flexDirection: 'row' }}>
                            <Button rounded iconLeft style={{ backgroundColor: '#66BB6A', marginRight: 5 }} onPress={() => this.getTokenAndLog()}>
                                <Icon name={this.state.isSubmittingPresence ? 'md-infinite' : 'md-add'} />
                                <Text>{this.state.isSubmittingPresence ? "Please Wait" : "Presence"}</Text>
                            </Button>
                            <Button rounded iconLeft style={{ backgroundColor: '#66BB6A' }} onPress={() => this.openNewAL()}>
                                <Icon name='md-add' />
                                <Text>New Leave</Text>
                            </Button>
                        </View>
                    </View>

                </Content>
                {
                    /*
                    <ScrollView style={{padding:10, marginHorizontal:10}}>
                        <Text style={{fontSize:20, fontWeight:'500'}}>
                            ANNOUNCEMENT
                        </Text>
                    </ScrollView>
                    */
                }


                <BouncyDrawer
                    willOpen={() => console.log('will open')}
                    didOpen={() => console.log('did open')}
                    willClose={() => console.log('will close')}
                    didClose={() => console.log('did close')}
                    title="HRSystem"
                    headerHeight={70}
                    titleStyle={{ color: '#fff', fontFamily: 'Helvetica Neue', fontSize: 20, marginLeft: -15, paddingTop: 20 }}
                    closedHeaderStyle={{ height: 80, backgroundColor: 'rgba(239,83,80,0.8)' }}
                    defaultOpenButtonIconColor="#fff"
                    defaultCloseButtonIconColor="#fff"
                    renderContent={this.renderContent}
                    openedHeaderStyle={{ backgroundColor: 'rgba(95, 95, 95,0.9)', paddingTop: 20 }}
                />
                <Drawer header="Announcement" isOpen={false} teaserHeight={95}>
                    {
                        (_.size(this.state.announce)) == 0 ?
                            <View>
                                <Image style={{ width: 100, height: 100, alignSelf: 'center', marginTop: 20, opacity: 0.5 }} source={require('../images/empty.png')} />
                                <Text style={{ color: '#e0e0e0', fontSize: 15, alignSelf: 'center' }}>
                                    There is nothing to show
                                </Text>
                            </View>
                            :
                            this.state.announce.map((data) => (
                                <Message title={data.TITLE} message={data.DATA} />
                            ))
                    }
                </Drawer>
            </Container>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f8f8f8',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

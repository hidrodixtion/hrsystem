/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Keyboard,
    ToastAndroid,
    Alert
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner } from 'native-base';
import { getStaffData, getPic, saveEditSalary, submitNewSalary } from "./API";
import ListView from "deprecated-react-native-listview";
import _ from 'lodash';


const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const date = new Date();
const getyear = date.getFullYear();
const getmonth = date.getUTCMonth();
function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}

let years = Array.from(range(getyear - 10, getyear + 10, 1));
console.log("BULANKU : ", getmonth);
type Props = {};
export default class FilterDialogBox extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            st: this.props.def,
            dataSource: ds.cloneWithRows(['row 1', 'row 2']),
            isFocused: false,
            resultBox: 50,
            query: '',
            empty: true,
            loaded: false,
            pickedMonth: this.monthFormatter(getmonth),
            pickedYear: getyear.toString(),
            pickedStaffId: this.props.staffId,
            filePath: '',
            pickedAmount: 'All'
        };
        console.log("now1 :", this.state.pickedMonth);
    };

    static capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    inverseMonthFormatter = (month) => {
        if (month == 'JANUARY') {
            month = 1;
        } else if (month == 'FEBRUARY') {
            month = 2;
        } else if (month == 'MARCH') {
            month = 3;
        } else if (month == 'APRIL') {
            month = 4;
        } else if (month == 'MAY') {
            month = 5;
        } else if (month == 'JUNE') {
            month = 6;
        } else if (month == 'JULY') {
            month = 7;
        } else if (month == 'AUGUST') {
            month = 8;
        } else if (month == 'SEPTEMBER') {
            month = 9;
        } else if (month == 'OCTOBER') {
            month = 10;
        } else if (month == 'NOVEMBER') {
            month = 11;
        } else if (month == 'DECEMBER') {
            month = 12;
        }
        return month;
    };

    monthFormatter = (month) => {
        month = month + 1;
        if (month == '1') {
            month = "JANUARY";
        } else if (month == '2') {
            month = "FEBRUARY";
        } else if (month == '3') {
            month = "MARCH";
        } else if (month == '4') {
            month = "APRIL";
        } else if (month == '5') {
            month = "MAY";
        } else if (month == '6') {
            month = "JUNE";
        } else if (month == '7') {
            month = "JULY";
        } else if (month == '8') {
            month = "AUGUST";
        } else if (month == '9') {
            month = "SEPTEMBER";
        } else if (month == '10') {
            month = "OCTOBER";
        } else if (month == '11') {
            month = "NOVEMBER";
        } else if (month == '12') {
            month = "DECEMBER";
        }
        month = month.toLowerCase();
        return month;
    };

    pushFileExplorer = () => {
        this.props.navigator.push.bind(this.props.navigator)({
            screen: 'Try', // unique ID registered with Navigation.registerScreen
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
                screenBackgroundColor: 'white'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'slide-up', // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
            passProps: {
                finish: this.onFilePicked
            }
        });
    };

    filterName = (query) => {
        let names = this.state.rawData;
        const filterRes = names.filter(x => {
            return x.NAME.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        console.log("MYSIZE : " + _.size(filterRes));
        if (_.size(filterRes) != 0) {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: null
            });
        } else {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: true
            });
        }

    };


    fetchStaff = async () => {
        let formData = new FormData();
        formData.append('sort', 'name');
        formData.append('sort_by', 'asc');
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" STAFF " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        dataSource: ds.cloneWithRows(responseJson.DATA),
                        rawData: responseJson.DATA,
                        loaded: true
                    });
                    this.filterName("");
                }
            })
            .catch((error) => {
                console.error(error);
            });
        console.log(this.state.dataSource);
    };

    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        this.fetchStaff();
    };

    onFilePicked = async (myPath) => {
        await this.setState({
            filePath: myPath
        });
        console.log("NOWFILE : " + myPath.substr(myPath.lastIndexOf('.')));
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flex: 0.918 }}>
                    <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                        <Body>
                            <Title>
                                FILTER BY
                        </Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => { this.props.navigator.pop() }}>
                                <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                            </Button>
                        </Right>
                    </Header>
                    <Form style={{ marginTop: 10, paddingRight: 20 }}>
                        <Item style={{ flexDirection: 'column' }}>
                            <Label style={{ alignSelf: 'flex-start' }}>
                                Applicant
                            </Label>
                            <ScrollView
                                showsVerticalScrollIndicator={false}
                                scrollEnabled={false}
                                keyboardDismissMode='none'
                                keyboardShouldPersistTaps="handled"
                                style={{
                                    width: '100%',
                                    borderWidth: 1,
                                    borderColor: '#a6a6a6',
                                    height: this.state.resultBox,
                                }}
                            >
                                <View style={{ width: '100%', height: 50 }}>
                                    <Input
                                        onFocus={() => this.setState({ resultBox: 230 })}
                                        onBlur={() => this.setState({ resultBox: 50 })}
                                        style={{ width: '100%', height: '100%' }}
                                        selectTextOnFocus={true}
                                        value={this.state.query}
                                        onChangeText={(text) => {
                                            console.log('textku : ' + text);
                                            this.setState({ query: text });
                                            this.filterName(text);
                                        }}
                                        getRef={(input) => this.searchBox = input}
                                        placeholder="Search"
                                    />
                                </View>
                                {
                                    this.state.empty && this.state.loaded ?
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ fontSize: 20 }}>No results found</Text>
                                        </View>
                                        :
                                        !this.state.loaded ?
                                            <View style={{ width: '100%' }}>
                                                <Spinner color='blue' />
                                            </View>
                                            :
                                            <ListView
                                                keyboardShouldPersistTaps='always'
                                                style={{ height: 180, width: '100%' }}
                                                enableEmptySections
                                                dataSource={this.state.dataSource}
                                                renderRow={(rowData) => (
                                                    <Button transparent
                                                        style={{
                                                            paddingVertical: 10,
                                                            borderBottomWidth: 0.2,
                                                            flexDirection: 'row'
                                                        }}
                                                        onPress={() => {
                                                            this.setState({
                                                                query: rowData.NAME,
                                                                pickedStaffId: rowData.ID,
                                                                resultBox: 50
                                                            });
                                                            Keyboard.dismiss()
                                                        }}
                                                    >
                                                        <Left style={{ flex: 0.3, paddingLeft: 15 }}>
                                                            <Thumbnail source={{ uri: getPic(rowData.PHOTO) }} />
                                                        </Left>
                                                        <Body>
                                                            <Text style={{ fontSize: 20, alignSelf: 'flex-start' }}>{rowData.NAME}</Text>
                                                        </Body>

                                                    </Button>
                                                )}
                                            />
                                }
                            </ScrollView>
                        </Item>
                        <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                            <Label style={{ alignSelf: 'flex-start' }}>
                                Month
                            </Label>
                            <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.pickedMonth}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ pickedMonth: itemValue.toLowerCase() })}>
                                    <Picker.Item label='All months' value='All' />
                                    {
                                        months.map((bulan) => {
                                            return <Picker.Item label={bulan} value={bulan.toLowerCase()} />
                                        })
                                    }

                                </Picker>
                            </View>
                        </Item>
                        <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                            <Label style={{ alignSelf: 'flex-start' }}>
                                Year
                            </Label>
                            <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.pickedYear}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ pickedYear: itemValue.toString() })}>
                                    <Picker.Item label='All years' value='All' />
                                    {
                                        years.map((year) => {
                                            return <Picker.Item label={year.toString()} value={year.toString()} />
                                        })
                                    }
                                </Picker>
                            </View>
                        </Item>
                        <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                            <Label style={{ alignSelf: 'flex-start' }}>
                                Number of data to show
                            </Label>
                            <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.pickedAmount}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ pickedAmount: itemValue })}>
                                    <Picker.Item label='1' value='1' />
                                    <Picker.Item label='10' value='10' />
                                    <Picker.Item label='30' value='30' />
                                    <Picker.Item label='50' value='50' />
                                    <Picker.Item label='100' value='100' />
                                    <Picker.Item label='All' value='All' />
                                </Picker>
                            </View>
                        </Item>
                    </Form>
                </View>
                <View style={{ flex: 0.082, flexDirection: 'row' }}>
                    <Left>
                        <Button full light style={{ borderWidth: 0.3, borderColor: '#a6a6a6' }} onPress={() => this.props.navigator.pop()}>
                            <Text>CANCEL</Text>
                        </Button>
                    </Left>
                    <Right>
                        <Button full style={{ backgroundColor: '#ef5350' }} onPress={() => this.props.onFinish(this.state.pickedStaffId, this.inverseMonthFormatter(this.state.pickedMonth.toUpperCase()), this.state.pickedYear, this.state.pickedAmount)}>
                            <Text>FILTER</Text>
                        </Button>
                    </Right>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

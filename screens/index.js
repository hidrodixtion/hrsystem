import { Navigation } from 'react-native-navigation';

import First from './first';
import Home from './Home';
import Notification from './Notification';
import Salary from './Salary';
import LightBox from './LightBox';
import TopTabs from './TopTabs';
import Try from './Try';
import SearchApplicant from './SearchApplicant';
import NewSalary from './NewSalary';
import FilterDialogBox from './FilterDialogBox';
import SortDialogBox from './SortDialogBox';
import AL_Log from './AL_Log';
import AL_Details from './AL_Details';
import NewAL from './NewAL';
import FilterAL from './FilterAL';
import EditAL from './EditAL';
import Submission from './Submission';
import ApprovedAL from './ApprovedAL';
import NewCL from './NewCL';
import HistoryCL from './HistoryCL';
import Profile from './Profile';
import UserAbsence from './UserAbsence';
import NewLeave from './NewLeave'
import _ from 'lodash';


// register all screens of the app (including internal ones)
export function registerScreens() {
  Navigation.registerComponent('Home', () => Home);
  Navigation.registerComponent('First', () => First);
  Navigation.registerComponent('Notification', () => Notification);
  Navigation.registerComponent('Salary', () => Salary);
  Navigation.registerComponent('LightBox', () => LightBox);
  Navigation.registerComponent('TopTabs', () => TopTabs);
  Navigation.registerComponent('Try', () => Try);
  Navigation.registerComponent('SearchApplicant', () => SearchApplicant);
  Navigation.registerComponent('NewSalary', () => NewSalary);
  Navigation.registerComponent('FilterDialogBox', () => FilterDialogBox);
  Navigation.registerComponent('SortDialogBox', () => SortDialogBox);
  Navigation.registerComponent('AL_Log', () => AL_Log);
  Navigation.registerComponent('AL_Details', () => AL_Details);
  Navigation.registerComponent('NewAL', () => NewAL);
  Navigation.registerComponent('FilterAL', () => FilterAL);
  Navigation.registerComponent('EditAL', () => EditAL);
  Navigation.registerComponent('Submission', () => Submission);
  Navigation.registerComponent('ApprovedAL', () => ApprovedAL);
  Navigation.registerComponent('NewCL', () => NewCL);
  Navigation.registerComponent('HistoryCL', () => HistoryCL);
  Navigation.registerComponent('Profile', () => Profile);
  Navigation.registerComponent('UserAbsence', () => UserAbsence);
  Navigation.registerComponent('NewLeave', () => NewLeave);
}
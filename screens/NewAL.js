/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    Switch,
    StyleSheet,
    View,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    FlatList,
    AsyncStorage,
    Keyboard,
    ToastAndroid,
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner } from 'native-base';
import { getStaffData, getPic, saveEditSalary, submitNewSalary, insertAL, getQuota, getDuration, getDateleave } from "./API";
import DatePicker from 'react-native-datepicker';
import ListView from "deprecated-react-native-listview";
//import DeviceInfo from 'react-native-device-info';
import _ from 'lodash';


const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
const date = new Date();
const getday = date.getDate();
const getyear = date.getFullYear();
const getmonth = date.getUTCMonth() + 1;
function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}

let years = Array.from(range(getyear - 10, getyear + 10, 1));
console.log("BULANKU : ", getmonth);
type Props = {};
export default class NewAL extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            st: this.props.def,
            dataSource: ds.cloneWithRows(['row 1', 'row 2']),
            isFocused: false,
            resultBox: 50,
            query: '',
            empty: true,
            loaded: false,
            pickedQuota: 'al',
            myQuota: {},
            quota_AL: '',
            quota_CL: '',
            quota_med: '',
            total: '',
            quota_medhos: '',
            quota_matl: '',
            quota_haj: '',
            qouta_umr: '',
            quota_marl: '',
            quota_patl: '',
            quota_el: '',
            quota_ql: '',
            quota_lwp: '',
            quota_compl: '',
            pickedStaffId: this.props.staffId,
            purpose: '',
            dateStart: getday + '-' + months[getmonth - 1] + '-' + getyear,
            dateEnd: getday + '-' + months[getmonth - 1] + '-' + getyear,
            myName: '',
            leave_duration: '',
            leave_duration_loading: false,
            date_leave: [],
            date_half: [],
            date_leave_loading: false,
            empty_duration: false,
            switchValue: [],
            filePath: ''
        };
        console.log("now1 :", getday);
    };

    submitNewAL = async (status) => {
        let formData = new FormData();
        if (this.state.leave_duration == '') {
            this.setState({
                empty_duration: true
            });
            return;
        }
        let arr = this.state.date_half;
        let finalstring = ''
        for (let i = 0; i < arr.length; i++) {
            finalstring += arr[i];
            if (i != arr.length - 1) {
                finalstring += ';'
            }

        }

        console.log('FinalString' + finalstring)
        formData.append('purpose', this.state.purpose);
        formData.append('start', this.submitFormat(this.state.dateStart));
        formData.append('end', this.submitFormat(this.state.dateEnd));
        formData.append('approve', this.state.user.id);
        formData.append('final', this.state.user.id);
        formData.append('apply', this.state.pickedStaffId);
        formData.append('status', status);
        formData.append('note', this.state.pickedQuota);
        formData.append('duration', this.state.leave_duration);
        formData.append('dateLeave', finalstring);
        if (this.state.filePath.length != 0) {
            formData.append('documentLeaving',
                {
                    uri: 'file://' + this.state.filePath,
                    type: 'image/foo',
                    name: 'testFile' + this.state.filePath.substr(this.state.filePath.lastIndexOf('.'))
                }

            )
        }
        formData.append('phone', '1')
        console.log("FORM : " + JSON.stringify(formData));
        fetch(insertAL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" BERHASIL? " + ((JSON.stringify(responseJson))));
                if (responseJson.msg.toLowerCase() == 'ok') {
                    this.props.finish();
                    this.props.navigator.pop();
                } else if (responseJson.msg.toLowerCase() == 'failed') {
                    Alert.alert(
                        'Failed',
                        responseJson.DATA,
                        [
                            { text: 'OK' }
                        ]
                    );
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    static capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    submitFormat = (date) => {
        day = date.split('-')[0];
        month = date.split('-')[1].toLowerCase();
        year = date.split('-')[2];
        if (day.length < 2) {
            day = '0' + day;
        }
        if (month == 'jan') {
            month = '01';
        } else if (month == 'feb') {
            month = '02';
        } else if (month == 'mar') {
            month = '03';
        } else if (month == 'apr') {
            month = '04';
        } else if (month == 'may') {
            month = '05';
        } else if (month == 'jun') {
            month = '06';
        } else if (month == 'jul') {
            month = '07';
        } else if (month == 'aug') {
            month = '08';
        } else if (month == 'sep') {
            month = '09';
        } else if (month == 'oct') {
            month = '10';
        } else if (month == 'nov') {
            month = '11';
        } else if (month == 'dec') {
            month = '12';
        }
        console.log('READY ' + year + '-' + month + '-' + day);
        return '' + year + '-' + month + '-' + day;
    };
    pushFileExplorer = () => {
        this.props.navigator.push.bind(this.props.navigator)({
            screen: 'Try', // unique ID registered with Navigation.registerScreen
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
                screenBackgroundColor: 'white'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'slide-up', // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
            passProps: {
                finish: this.onFilePicked
            }
        });
    };
    onFilePicked = async (myPath) => {
        let fileExt = myPath.substr(myPath.lastIndexOf('.')).toLowerCase();
        RNFS.stat(myPath).then((result) => {
            console.log(result);
            if (result.size > 2097152) {
                Alert.alert(
                    'Warning!',
                    'File size is too big',
                    [
                        { text: 'OK' },
                    ]
                );
                return;
            }
        }
        );
        if (fileExt == '.png' || fileExt == '.jpg' || fileExt == '.jpeg') {
            await this.setState({
                filePath: myPath
            });
        } else {
            Alert.alert('Failed!', 'File chosen is not image type. Allowed type is .png, .jpg, .jpeg, and .tiff')
        }
        console.log("NOWFILE : " + myPath.substr(myPath.lastIndexOf('.')));
    };


    inverseMonth = (month) => {
        month = month.toLowerCase();
        if (month == 'jan') {
            month = '00';
        } else if (month == 'feb') {
            month = '01';
        } else if (month == 'mar') {
            month = '02';
        } else if (month == 'apr') {
            month = '03';
        } else if (month == 'may') {
            month = '04';
        } else if (month == 'jun') {
            month = '05';
        } else if (month == 'jul') {
            month = '06';
        } else if (month == 'aug') {
            month = '07';
        } else if (month == 'sep') {
            month = '08';
        } else if (month == 'oct') {
            month = '09';
        } else if (month == 'nov') {
            month = '10';
        } else if (month == 'dec') {
            month = '11';
        }
        return month;
    };

    monthFormatter = (month) => {
        month = month + 1;
        if (month == '1') {
            month = "Jan";
        } else if (month == '2') {
            month = "Feb";
        } else if (month == '3') {
            month = "Mar";
        } else if (month == '4') {
            month = "Apr";
        } else if (month == '5') {
            month = "May";
        } else if (month == '6') {
            month = "June";
        } else if (month == '7') {
            month = "July";
        } else if (month == '8') {
            month = "Aug";
        } else if (month == '9') {
            month = "Sept";
        } else if (month == '10') {
            month = "Oct";
        } else if (month == '11') {
            month = "Nov";
        } else if (month == '12') {
            month = "Dec";
        }
        month = month.toLowerCase();
        return month;
    };

    filterName = (query) => {
        let names = this.state.rawData;
        const filterRes = names.filter(x => {
            return x.NAME.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        console.log("MYSIZE : " + _.size(filterRes));
        if (_.size(filterRes) != 0) {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: null
            });
        } else {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: true
            });
        }

    };

    fetchMyName = async () => {
        let formData = new FormData();
        formData.append('id', this.props.staffId);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    if (this.props.privilege.create_own_al != false) {
                        console.log('SIAPA SAYA : ' + JSON.stringify(responseJson.DATA[0].NAME));
                        this.setState({
                            query: responseJson.DATA[0].NAME,
                            myName: responseJson.DATA[0].NAME
                        })
                    } else {
                        this.setState({
                            query: '',
                            myName: responseJson.DATA[0].NAME
                        })
                    }
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    fetch_leaveDuration = async () => {
        this.setState({ leave_duration_loading: true });
        let formData = new FormData();
        formData.append('start', this.submitFormat(this.state.dateStart));
        formData.append('end', this.submitFormat(this.state.dateEnd));
        fetch(getDuration(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    console.log('DURASI : ' + JSON.stringify(responseJson.DATA));
                    this.setState({
                        leave_duration: responseJson.DATA.toString(),
                        leave_duration_loading: false
                    });
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };
    fetch_Dateleave = async () => {
        this.setState({ date_leave_loading: true });
        let formData = new FormData();
        formData.append('start', this.submitFormat(this.state.dateStart));
        formData.append('end', this.submitFormat(this.state.dateEnd));
        formData.append('staff', this.props.staffId);
        fetch(getDateleave(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {

                    let switchvalue = [];
                    if (responseJson.DATA == '') {
                        switchvalue = new Array(responseJson.DATA.length).fill(false)
                    }
                    console.log("LENGTH" + switchvalue)
                    console.log('DATE_LEAVE : ' + JSON.stringify(responseJson.DATA));
                    this.setState({
                        date_leave: responseJson.DATA,
                        date_leave_loading: false,
                        switchValue: switchvalue
                    });
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    get_Quota = async () => {
        let formData = new FormData();
        formData.append('staff', this.props.staffId);
        fetch(getQuota(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    console.log('QUOTA : ' + JSON.stringify(responseJson.DATA));
                    console.log('QUOTA HAJU : ' + JSON.stringify(responseJson.DATA.HAJ.QUOTA_MAX));
                    this.setState({
                        myQuota: responseJson.DATA,
                        quota_AL: responseJson.DATA.AL[0].QUOTA,
                        quota_CL: responseJson.DATA.CL.QUOTA,
                        quota_med: responseJson.DATA.MED.QUOTA,
                        quota_medhos: responseJson.DATA.MEDHOS.QUOTA,
                        quota_matl: responseJson.DATA.MATL.QUOTA_MAX,
                        quota_haj: responseJson.DATA.HAJ,
                        quota_umr: responseJson.DATA.UMR.QUOTA_MAX,
                        quota_marl: responseJson.DATA.MARL.QUOTA_MAX,
                        quota_patl: responseJson.DATA.PATL.QUOTA_MAX,
                        quota_compl: responseJson.DATA.COMPL.QUOTA_MAX,
                        //total : this.state.quota_AL+this.state.myQuota.CL
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    fetchStaff = async () => {
        let formData = new FormData();
        formData.append('sort', 'name');
        formData.append('sort_by', 'asc');
        if (this.props.privilege.create_other_al == false) {
            formData.append('id', this.props.staffId);
        }
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" STAFF " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    if (this.props.privilege.create_own_al == false) {
                        for (let i = 0; i < responseJson.DATA.length; i++) {
                            // look for the entry with a matching `code` value
                            if (responseJson.DATA[i].NAME == this.state.query) {
                                // we found it
                                // obj[i].name is the matched result
                                delete responseJson.DATA[i];
                                this.setState({
                                    query: ''
                                });
                            }
                        }
                    }
                    this.setState({
                        dataSource: ds.cloneWithRows(responseJson.DATA),
                        rawData: responseJson.DATA,
                        loaded: true
                    });
                    this.filterName("");
                }
            })
            .catch((error) => {
                console.error(error);
            });
        console.log(this.state.dataSource);
    };

    renderDateLeave(item, index) {
        //  item=this.state.date_leave
        return (
            <TouchableOpacity
                style={{
                    paddingVertical: 10,
                    borderBottomWidth: 0.2,
                    flexDirection: 'row'
                }}>
                <Left style={{ flex: 0.5, paddingLeft: 15 }}>
                    <Text style={{ fontSize: 15, alignSelf: 'flex-start' }}>{item}</Text>
                </Left>
                <Left style={{ flex: 0.2, paddingLeft: 30 }}>
                    <Text style={{ fontSize: 12, alignSelf: 'flex-start' }}>Full Day</Text>
                </Left>
                <Body style={{ flex: 0.1, paddingLeft: 30 }}>
                    <Switch
                        onValueChange={(value) => this.toggleSwitch(value, item, index)}
                        value={this.state.switchValue[index]}
                    />
                </Body>
                <Left style={{ flex: 0.2, paddingLeft: 15 }}>
                    <Text style={{ fontSize: 12, alignSelf: 'flex-start' }}>Half Day </Text>
                </Left>
            </TouchableOpacity>
        )
    }
    toggleSwitch = (value, item, index) => {
        switchvalue = this.state.switchValue
        switchvalue[index] = value


        //onValueChange of the  switch this function will be called
        this.setState({
            switchValue: switchvalue
        })
        var arr = this.state.date_half
        var index = arr.indexOf(item);
        console.log('HALFT' + this.state.date_half)
        console.log('indexa' + item)
        if (index > -1) {
            arr.splice(index, 1);
            console.log('HALF-1' + arr)
        }
        else {
            arr.push(item)
            console.log('HALF2' + arr)
        }
        let duration = this.state.date_leave.length - (this.state.date_half.length * 0.5)
        this.setState({
            date_half: arr,
            leave_duration: duration
        })

        console.log('HALF' + this.state.date_half)

        //state changes according to switch
        //which will result in re-render the text
    }

    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrievings data
        }
        this.fetchStaff();
        this.fetchMyName();
        this.fetch_Dateleave();
        this.get_Quota();
        this.fetch_leaveDuration();
    };

    render() {
        return (
            <View style={styles.container} onStartShouldSetResponderCapture={() => {
                this.setState({ enableScrollViewScroll: true });
            }}>
                <KeyboardAvoidingView behavior="padding" style={{ flex: 0.918 }}>
                    <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                        <Body>
                            <Title>
                                New Leave Request
                        </Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => { this.props.navigator.pop() }}>
                                <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                            </Button>
                        </Right>
                    </Header>
                    <ScrollView
                        style={{ flex: 1 }} keyboardShouldPersistTaps="handled" scrollEnabled={this.state.enableScrollViewScroll}>
                        <Form style={{ marginTop: 10, paddingRight: 20 }}>
                            <Item style={{ flexDirection: 'column' }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Applicant
                            </Label>
                                <ScrollView
                                    showsVerticalScrollIndicator={false}
                                    scrollEnabled={false}
                                    keyboardDismissMode='none'
                                    keyboardShouldPersistTaps="handled"
                                    style={{
                                        width: '100%',
                                        borderWidth: 1,
                                        borderColor: '#a6a6a6',
                                        height: this.state.resultBox,
                                    }}
                                >
                                    <View style={{ width: '100%', height: 50 }}>
                                        <Input
                                            onFocus={() => {
                                                this.setState({ enableScrollViewScroll: false });
                                                if (this.state.enableScrollViewScroll === false) {
                                                    this.setState({ enableScrollViewScroll: true });
                                                }
                                                this.setState({ resultBox: 230 })
                                            }}
                                            onBlur={() => {
                                                this.setState({
                                                    resultBox: 50,
                                                    enableScrollViewScroll: true
                                                })
                                            }}
                                            style={{ width: '100%', height: '100%' }}
                                            selectTextOnFocus={true}
                                            value={this.state.query}
                                            onChangeText={(text) => {
                                                console.log('textku : ' + text);
                                                this.setState({ query: text });
                                                this.filterName(text);
                                            }}
                                            getRef={(input) => this.searchBox = input}
                                            placeholder="Search"
                                        />
                                    </View>
                                    <View onStartShouldSetResponderCapture={() => {
                                        this.setState({ enableScrollViewScroll: false });
                                        if (this.state.enableScrollViewScroll === false) {
                                            this.setState({ enableScrollViewScroll: true });
                                        }
                                    }}>
                                        {
                                            this.state.empty && this.state.loaded ?
                                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 20 }}>No results found</Text>
                                                </View>
                                                :
                                                !this.state.loaded ?
                                                    <View style={{ width: '100%' }}>
                                                        <Spinner color='blue' />
                                                    </View>
                                                    :
                                                    <ListView
                                                        refs={(myLists) => this.myList = myLists}
                                                        keyboardShouldPersistTaps='always'
                                                        style={{ height: 180, width: '100%' }}
                                                        enableEmptySections
                                                        dataSource={this.state.dataSource}
                                                        renderRow={(rowData) => (
                                                            <Button transparent
                                                                style={{
                                                                    paddingVertical: 10,
                                                                    borderBottomWidth: 0.2,
                                                                    flexDirection: 'row'
                                                                }}
                                                                onPress={() => {
                                                                    this.setState({
                                                                        query: rowData.NAME,
                                                                        pickedStaffId: rowData.ID,
                                                                        resultBox: 50
                                                                    });
                                                                    Keyboard.dismiss()
                                                                }}
                                                            >
                                                                <Left style={{ flex: 0.3, paddingLeft: 15 }}>
                                                                    <Thumbnail source={{ uri: getPic(rowData.PHOTO) }} />
                                                                </Left>
                                                                <Body>
                                                                    <Text style={{ fontSize: 20, alignSelf: 'flex-start' }}>{rowData.NAME}</Text>
                                                                </Body>

                                                            </Button>
                                                        )}
                                                    />
                                        }
                                    </View>
                                </ScrollView>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Choose leave quota
                            </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <Picker
                                        mode="dropdown"
                                        selectedValue={this.state.pickedQuota}
                                        onValueChange={(itemValue, itemIndex) => this.setState({ pickedQuota: itemValue })}>
                                        <Picker.Item label='Annual Leave (AL)' value='al' />
                                        <Picker.Item label='Compassionate Leave (COMPL)' value='compl' />
                                        <Picker.Item label='Examination Leave (EL)' value='el' />
                                        <Picker.Item label='Leave Without Pay (LWP)' />
                                        <Picker.Item label='Medical Leave (MED)' value='med' />
                                        <Picker.Item label='Medical Leave Hospitalization (MEDHOS) ' value='medhos' />
                                        <Picker.Item label='Maternity Leave (MATL)' value='matl' />
                                        <Picker.Item label='Marriage Leave (MARL)' value='marl' />
                                        <Picker.item label='Paternity Leave (PATL)' value='patl' />
                                        <Picker.Item label='Pilgrimage Leave (HAJ)' value='haj' />
                                        <Picker.Item label='Prolonged Illnes Leave (PIL)' value='pil' />
                                        <Picker.item label='Quarantine Leave (QL)' value='ql' />
                                        <Picker.Item label='Umrah Leave (UMR)' value='umr' />

                                    </Picker>
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start', color: 'black' }}>

                                    {this.state.pickedQuota == 'med' ? ('REMAINING MEDICAL QUOTA: ' + this.state.quota_med)
                                        : this.state.pickedQuota == 'al' ? ('REMAINING QUOTA: AL QUOTA + CL QUOTA: ' + this.state.quota_AL + '+' + this.state.myQuota.CL)
                                            : this.state.pickedQuota == 'medhos' ? ('REMAINING MEDICAL HOSPITALIZATION QUOTA:' + this.state.quota_medhos)
                                                : this.state.pickedQuota == 'marl' ? ('REMAINING MARRIAGE QUOTA: ' + this.state.quota_marl)
                                                    : this.state.pickedQuota == 'haj' ? ('REMAINING PILGRIMAGE QUOTA: ' + this.state.quota_haj.QUOTA_MAX)
                                                        : this.state.pickedQuota == 'matl' ? ('REMAINING MATERNITY QUOTA: ' + this.state.quota_matl)
                                                            : this.state.pickedQuota == 'patl' ? ('REMAINING PATERNITY QUOTA: ' + this.state.quota_patl)
                                                                : this.state.pickedQuota == 'umr' ? ('REMAINING UMRAH QUOTA: ' + this.state.quota_umr)
                                                                    : this.state.pickedQuota == 'compl' ? ('REMAINING COMPASSIONATE QUOTA: ' + this.state.quota_compl)
                                                                        : ''}
                                </Label>
                            </Item>
                            <Item>
                                {
                                    this.state.pickedQuota == 'med' ? (
                                        <View>
                                            <Item style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                                            </Item>
                                            <View style={{ width: '100%', flexDirection: 'row', marginVertical: 20, paddingLeft: 15 }}>
                                                <View style={{ marginRight: 10 }}>
                                                    <TouchableOpacity iconLeft onPress={() => this.pushFileExplorer()}>
                                                        <Icon name='attach' style={{ marginRight: 15 }} />
                                                        <Text>Upload an Medical Document</Text>
                                                    </TouchableOpacity>

                                                </View>
                                                <ScrollView horizontal style={{ flex: 1, borderWidth: 1, borderColor: '#a6a6a6', paddingLeft: 10 }}>
                                                    {
                                                        this.state.filePath == '' ?
                                                            <Text style={{ textAlignVertical: 'center', marginRight: 20, fontSize: 14 }}>
                                                                Insert an image type file only. Max 2 MB
                                                </Text>
                                                            :
                                                            <Text style={{ textAlignVertical: 'center', marginRight: 20 }}>
                                                                {this.state.filePath}
                                                            </Text>
                                                    }
                                                </ScrollView>
                                            </View>
                                        </View>
                                    )
                                        : <Text></Text>
                                }

                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Leaving date (Start)
                            </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <DatePicker
                                        style={{ width: '100%' }}
                                        date={this.state.dateStart}
                                        mode="date"
                                        androidMode="spinner"
                                        placeholder="select date"
                                        format="DD-MMM-YYYY"
                                        minDate={"01-01-" + (getyear - 10)}
                                        maxDate={"31-12-" + (getyear + 10)}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => {
                                            let a = this.state.dateEnd.split('-');
                                            let b = new Date(a[2], this.inverseMonth(a[1]), a[0]);
                                            let c = date.split('-');
                                            let d = new Date(c[2], this.inverseMonth(c[1]), c[0]);
                                            console.log(b);
                                            console.log(d > b);
                                            d > b ?
                                                this.setState({
                                                    dateStart: date,
                                                    dateEnd: date
                                                })
                                                :
                                                this.setState({ dateStart: date });

                                            this.fetch_leaveDuration();
                                            this.fetch_Dateleave();
                                        }
                                        }
                                    />
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Leaving date (End)
                            </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <DatePicker
                                        style={{ width: '100%' }}
                                        date={this.state.dateEnd}
                                        mode="date"
                                        androidMode="spinner"
                                        placeholder="select date"
                                        format="DD-MMM-YYYY"
                                        minDate={"01-01-" + (getyear - 10)}
                                        maxDate={"31-12-" + (getyear + 10)}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => {
                                            let a = this.state.dateStart.split('-');
                                            let b = new Date(a[2], this.inverseMonth(a[1]), a[0]);
                                            let c = date.split('-');
                                            let d = new Date(c[2], this.inverseMonth(c[1]), c[0]);
                                            d < b ?
                                                this.setState({
                                                    dateStart: date,
                                                    dateEnd: date
                                                })
                                                :
                                                this.setState({ dateEnd: date })

                                            this.fetch_leaveDuration();
                                            this.fetch_Dateleave();
                                        }}
                                    />
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Leave Duration
                            </Label>
                                <View style={{ width: '100%', justifyContent: 'flex-start', flexDirection: 'row' }}>

                                    <Input
                                        multiline={true}
                                        keyboardType='numeric'
                                        value={this.state.leave_duration.toString()}
                                        onChangeText={(text) => this.setState({ leave_duration: text })}
                                        style={{ width: '75%', height: 40, textAlignVertical: 'center', borderWidth: 1, borderColor: '#a6a6a6', paddingBottom: 5, paddingTop: 5 }}
                                    />
                                    {
                                        this.state.leave_duration_loading ?
                                            <View style={{ marginLeft: 8, justifyContent: 'center', borderWidth: 0.4 }}>
                                                <Spinner style={{ width: 30, height: 30, paddingHorizontal: 10 }} color='rgba(239,83,80,0.8)' />
                                            </View>
                                            :
                                            <Button transparent style={{ marginLeft: 8, justifyContent: 'center', borderWidth: 0.4 }} onPress={() => { this.fetch_leaveDuration() }}>
                                                <Image source={require('../images/duration.png')} style={{ width: 30, height: 30, paddingHorizontal: 10 }} />
                                            </Button>
                                    }
                                </View>
                                {
                                    this.state.empty_duration ?
                                        <Label style={{ color: 'red', fontSize: 15 }}>
                                            Empty field are not allowed!
                                    </Label>
                                        :
                                        null
                                }
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Date Leave Duration
                            </Label>
                                <View style={{ width: '100%', justifyContent: 'flex-start', flexDirection: 'row' }}>
                                    <FlatList
                                        data={this.state.date_leave}
                                        extraData={this.state}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({ item, index }) => this.renderDateLeave(item, index)}
                                    />
                                </View>
                                {
                                    this.state.empty_duration ?
                                        <Label style={{ color: 'red', fontSize: 15 }}>
                                            Empty field are not allowed!
                                    </Label>
                                        :
                                        null
                                }
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Purpose of leaving
                            </Label>
                                <View style={{ width: '100%', height: 150, borderWidth: 1, justifyContent: 'flex-start' }}>
                                    <Input
                                        multiline={true}
                                        value={this.state.purpose}
                                        onChangeText={(text) => this.setState({ purpose: text })}
                                        style={{ width: '100%', height: '100%', textAlignVertical: 'top' }}
                                    />
                                </View>
                            </Item>
                        </Form>
                        <View style={{ flex: 0.082, flexDirection: 'row' }}>
                            <Left>
                                <Button full light style={{ borderWidth: 0.3, borderColor: '#a6a6a6' }} onPress={() => this.props.navigator.pop()}>
                                    <Text>CANCEL</Text>
                                </Button>
                            </Left>
                            <Body>
                                <Button full style={{ backgroundColor: '#a6a6a6' }} onPress={() => this.submitNewAL('draft')}>
                                    <Text>DRAFT</Text>
                                </Button>
                            </Body>
                            <Right>
                                <Button full style={{ backgroundColor: '#66BB6A' }} onPress={() => this.submitNewAL('submit')}>
                                    <Text>SUBMIT</Text>
                                </Button>
                            </Right>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

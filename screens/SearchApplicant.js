/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    AsyncStorage
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner } from 'native-base';
import { getStaffData, getPic } from "./API";
import ListView from "deprecated-react-native-listview";
import _ from 'lodash';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
type Props = {};
export default class SearchApplicant extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            st: this.props.def,
            dataSource: ds.cloneWithRows(['row 1', 'row 2']),
            query: '',
            empty: true,
            loaded: false
        };
        //console.log("now :" ,this.state.st)
    };

    async componentWillMount() {
        /*
        const movies = [{
            title: "Star Wars",
            category: "space",
            releaseYear: "1977",
        },
            {
                title: "Back to the Future",
                category: "time",
                releaseYear: "1985",
            },
            {
                title: "The Matrix",
                category: "scifi",
                releaseYear: "1999",
            },
            {
                title: "Inception",
                category: "fantasy",
                releaseYear: "2010",
            },
            {
                title: "Interstellar",
                category: "space",
                releaseYear: "2014",
            }
        ];

        const space = movies.filter(x =>{
            return x.title.toLowerCase().indexOf('Star Wars'.toLowerCase()) > -1;
        });
        console.log('HASILKU : '+JSON.stringify(space));
        */
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        this.fetchStaff();
    };

    filterName = (query) => {
        let names = this.state.rawData;
        const filterRes = names.filter(x => {
            return x.NAME.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        console.log("MYSIZE : " + _.size(filterRes));
        if (_.size(filterRes) != 0) {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: null
            });
        } else {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: true
            });
        }

    };

    fetchStaff = async () => {
        let formData = new FormData();
        formData.append('sort', 'name');
        formData.append('sort_by', 'asc');
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" STAFF " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        dataSource: ds.cloneWithRows(responseJson.DATA),
                        rawData: responseJson.DATA,
                        loaded: true
                    });
                    this.filterName("");
                }
            })
            .catch((error) => {
                console.error(error);
            });
        console.log(this.state.dataSource);
    };
    _focusInput(inputField) {
        this[inputField]._root.focus();
    }

    render() {
        return (
            <View style={styles.container}>
                <Header searchBar rounded style={{ marginTop: 24 }}>
                    <Item>
                        <Icon name="ios-search" />
                        <Input
                            value={this.state.query}
                            onChangeText={(text) => {
                                console.log('textku : ' + text);
                                this.setState({ query: text });
                                this.filterName(text);
                            }}
                            getRef={(input) => this.searchBox = input}
                            placeholder="Search"
                        />
                        <Icon name="ios-people" />
                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>
                {

                    <ListView
                        enableEmptySections
                        dataSource={this.state.dataSource}
                        renderRow={(rowData) => (
                            <Button transparent style={{ paddingVertical: 10, borderBottomWidth: 0.2, flexDirection: 'row' }}>
                                <Left style={{ flex: 0.3, paddingLeft: 15 }}>
                                    <Thumbnail source={{ uri: getPic(rowData.PHOTO) }} />
                                </Left>
                                <Body>
                                    <Text style={{ fontSize: 20, alignSelf: 'flex-start' }}>{rowData.NAME}</Text>
                                </Body>

                            </Button>
                        )}
                    />
                }

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#FFF',
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

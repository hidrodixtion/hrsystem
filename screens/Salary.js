/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import ActionButton from 'react-native-action-button';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    StatusBar,
    FlatList,
    AsyncStorage,
    Alert,
    ToastAndroid,
    TouchableWithoutFeedback,
    Picker
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Icon, Thumbnail, Card, CardItem, Body, Left, Right, Fab, Spinner } from 'native-base';
import BouncyDrawer from 'react-native-bouncy-drawer';
import RNFS from 'react-native-fs';
import { getStaffData, getAnnouncement, getPic, getNotif, readNotif, getSalary, salarySlip, deleteSalarySlip, getLogin } from "./API";
import _ from 'lodash';

type Props = {};
export default class Salary extends Component<Props> {
    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        this.goLogin();
    };


    goLogin = async () => {
        let formData = new FormData();
        formData.append('code', this.state.user.code);
        formData.append('password', this.state.user.pass);
        console.log('login ' + JSON.stringify(formData));
        return fetch(getLogin(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" mitossss" + (JSON.stringify(responseJson)));
                this.setState({ loading: false });
                if (responseJson.msg.toLowerCase() == 'ok') {
                    try {
                        await AsyncStorage.setItem('user_token', responseJson.DATA);
                        console.log("FINISH")
                    } catch (error) {
                        // Error saving data
                    }
                    this.setState({
                        user: {
                            token: responseJson.DATA,
                            code: this.state.user.code,
                            pass: this.state.user.pass,
                            id: this.state.user.id
                        }
                    });
                    this.fetchSalary(this.state.user.id);
                    this.getData(this.state.user.id);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    getData = async (id) => {
        let formData = new FormData();
        /*
        formData.append('username','9114139KP');
        formData.append('password','pesan2gelas');
        */
        console.log('FORM : ' + this.state.user.id + '  ' + this.state.user.token);
        formData.append('id', id);
        //formData.append('password', pass);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" SALARY : " + (JSON.stringify(responseJson)));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data_user: responseJson.DATA[0],
                        privilege: responseJson.DATA[0].PRIVILEGE,
                        myPic: { uri: getPic(responseJson.DATA[0].PHOTO + '?' + new Date()) }
                    });
                    console.log("data diri " + JSON.stringify(this.state.data_user));
                    this.getNotifCount(this.state.user.id);
                    //console.log("DATA AL : ", this.state.data['CURRENT AL'][0].QUOTA)
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    showSnackbar = (msg) => {
        this.props.navigator.showSnackbar({
            text: msg,
            actionText: 'X', // optional
            actionId: 'fabClicked', // Mandatory if you've set actionText
            actionColor: 'white', // optional
            textColor: 'white', // optional
            backgroundColor: 'green', // optional
            duration: 'indefinite' // default is `short`. Available options: short, long, indefinite
        });
    };

    getCLNotifCount = async (id) => {
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('action', 'cl');
        formData.append('seen', false);
        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notifcl " + ((_.size(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok" || responseJson.msg.toLowerCase() != 'empty') {
                    this.setState({
                        cl_count: _.size(responseJson.DATA)
                    })
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };
    getNotifCount = async (id) => {
        await this.getCLNotifCount(id);
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('action', 'al');
        formData.append('seen', false);
        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notif " + ((responseJson.DATA)));
                if (responseJson.msg.toLowerCase() == "ok" || responseJson.msg.toLowerCase() != 'empty') {
                    this.setState({
                        notifCount: (_.size(responseJson.DATA) + this.state.cl_count)
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    onFinishEdit = async () => {
        await this.setState({
            data: [],
            loaded: false
        });
        console.log("DONE!");
        this.fetchSalary(this.state.user.id);
        this.showSnackbar('Salary slip has been updated!');
    };

    onFinishSubmit = async () => {
        await this.setState({
            data: [],
            loaded: false
        });
        console.log("DONE!");
        this.fetchSalary(this.state.user.id);
        this.showSnackbar('Salary slip has been added!');
    };

    fetchSalary = async (id) => {
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        fetch(getSalary(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" SALARY " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data: responseJson.DATA,
                        loaded: true
                    });
                } else {
                    this.setState({
                        loaded: true
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    deleteSalary = async (id) => {
        let formData = new FormData();
        formData.append('id', parseInt(id));
        fetch(deleteSalarySlip(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" SALARY " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data: [],
                        loaded: false
                    });
                    this.fetchSalary(this.state.user.id);
                } else {
                    this.setState({
                        loaded: true
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    popNotif = () => {
        this.props.navigator.push({
            screen: 'Notification', // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {}, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };

    changeScreen = (target) => {
        this.props.navigator.resetTo({
            screen: target, // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {
                privilege: this.state.privilege
            }, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
                screenBackgroundColor: '#FFF'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };

    goLogOut = async () => {
        let keys = ['user_token', 'user_code', 'user_pass', 'user_id'];
        await AsyncStorage.multiRemove(keys, (err) => { });
        this.props.navigator.resetTo({
            screen: 'First', // unique ID registered with Navigation.registerScreen
            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the pushed screen (optional)
        });
    };

    renderContent = () => (
        <View style={{ backgroundColor: 'rgba(95, 95, 95,0.9)', flex: 1, alignItems: 'center', paddingTop: '15%' }}>
            <StatusBar translucent backgroundColor={'transparent'} />
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.changeScreen('Profile')}>
                        <Thumbnail style={{ width: 80, height: 80, borderRadius: 40 }}
                            resizeMode='cover'
                            source={this.state.myPic}
                            onError={() => {
                                this.setState({
                                    myPic: require('../images/no_image.png')
                                })
                            }}
                        />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Profile</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={this.popNotif}>
                        <Thumbnail large source={require('../images/bell.png')} />
                    </TouchableOpacity>
                    {
                        this.state.notifCount == 0 || this.state.notifCount == null ?
                            null
                            :
                            <Text style={{
                                position: 'absolute',
                                bottom: '17%',
                                width: 40,
                                height: 40,
                                backgroundColor: '#ef5350',
                                textAlign: 'center',
                                textAlignVertical: 'center',
                                borderRadius: 20,
                                right: '0%',
                                fontSize: 15,
                                color: 'white'
                            }} onPress={() => { this.popNotif() }}>
                                {this.state.notifCount}
                            </Text>
                    }
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Notification</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%', marginVertical: 20 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.changeScreen('Home') }}>
                        <Thumbnail large source={require('../images/dashboard.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Dashboard</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.changeScreen('Salary') }}>
                        <Thumbnail large source={require('../images/salaryslip.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Salary Slips</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%', marginVertical: 20 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { this.changeScreen('TopTabs') }}>
                        <Thumbnail large source={require('../images/absence.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Absence</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => {
                            Alert.alert(
                                'Warning!',
                                'Are you sure want to log out?',
                                [
                                    { text: 'LOG OUT', onPress: () => this.goLogOut() },
                                    { text: 'CANCEL' }
                                ]
                            )
                        }}
                    >
                        <Thumbnail large source={require('../images/logout.png')} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFF', fontSize: 15 }}>Log Out</Text>
                </View>
            </View>

        </View>
    );
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            user: {},
            go: 'tt',
            loaded: false,
            fabActive: false,
            sortBy: '',
            orderBy: '',
            filterAmount: '',
            filterMonth: '',
            filterYear: '',
            filterAmount: '',
            data_user: [],
            privilege: {},
            notifCount: 0,
            cl_count: 0
        };
    }

    download = async (pic_name) => {
        RNFS.downloadFile({
            fromUrl: salarySlip(pic_name),
            toFile: RNFS.ExternalStorageDirectoryPath + '/Download' + '/' + pic_name
        }).promise.then(res => {
            if (res.statusCode == 200) {
                this.setState({ downloaded: true });
                ToastAndroid.show("File saved to Downloads" + '/' + pic_name, ToastAndroid.LONG);
            } else {
                ToastAndroid.show("Error downloading, please try again later", ToastAndroid.SHORT);
            }
            console.log(JSON.stringify(res));
        });
    };

    openModal(id, month, year, applicant, staffId) {
        this.props.navigator.push({
            screen: "LightBox", // unique ID registered with Navigation.registerScreen
            title: "Modal", // title of the screen as appears in the nav bar (optional)
            passProps: {
                close: this.onGoBack,
                id: id,
                month: month,
                year: year,
                applicant: applicant,
                staffId: staffId,
                finish: this.onFinishEdit
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };

    newSalary(staffId) {
        this.props.navigator.push({
            screen: "NewSalary", // unique ID registered with Navigation.registerScreen
            title: "Modal", // title of the screen as appears in the nav bar (optional)
            passProps: {
                staffId: staffId,
                finish: this.onFinishSubmit
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };

    confirmDelete = (id) => {
        Alert.alert('Warning!', 'Are you sure want to delete this?',
            [
                { text: 'No' },
                { text: 'Yes', onPress: () => this.deleteSalary(id) }
            ]
        )
    };

    onGoBack = async (item) => {
        await this.setState({ go: item.data });
        this.props.navigator.pop();
        console.log(this.state.go);
    };

    dateConverter = (month) => {
        if (month == '1') {
            month = "JANUARY";
        } else if (month == '2') {
            month = "FEBRUARY";
        } else if (month == '3') {
            month = "MARCH";
        } else if (month == '4') {
            month = "APRIL";
        } else if (month == '5') {
            month = "MAY";
        } else if (month == '6') {
            month = "JUNE";
        } else if (month == '7') {
            month = "JULY";
        } else if (month == '8') {
            month = "AUGUST";
        } else if (month == '9') {
            month = "SEPTEMBER";
        } else if (month == '10') {
            month = "OCTOBER";
        } else if (month == '11') {
            month = "NOVEMBER";
        } else if (month == '12') {
            month = "DECEMBER";
        }
        return month;
    };

    closeDialogBox = () => {
        this.props.navigator.pop();
    };

    setFilter = async (applicant, month, year, amount) => {
        if (year == 'All') {
            year = '';
        }
        if (month == 'all') {
            month = '';
        }
        if (applicant == null) {
            applicant = '';
        }
        if (amount == 'All') {
            amount = ''
        }
        await this.setState({
            data: [],
            loaded: false,
            filterYear: year,
            filterMonth: month,
            filterApplicant: applicant,
            filterAmount: amount
        });
        this.props.navigator.pop();
        console.log('FILTER : ' + applicant + ' ' + month + ' ' + year + ' ' + amount);
        this.fetchFilteredSalary();
    };

    setSortandOrder = async (sort, order) => {
        await this.setState({
            data: [],
            loaded: false,
            sortBy: sort,
            orderBy: order
        });
        console.log(sort + ' ' + order);
        this.props.navigator.pop();
        this.fetchFilteredSalary();
    };

    fetchFilteredSalary = async () => {
        let formData = new FormData();
        if (_.size(this.state.filterApplicant) != 0) {
            formData.append('apply', this.state.filterApplicant);
        }
        if (_.size(this.state.filterMonth) != 0) {
            formData.append('month1', parseInt(this.state.filterMonth));
            formData.append('month2', parseInt(this.state.filterMonth));
        }
        if (_.size(this.state.filterYear) != 0) {
            formData.append('year1', parseInt(this.state.filterYear));
            formData.append('year2', parseInt(this.state.filterYear));
        }
        if (_.size(this.state.filterAmount != 0)) {
            formData.append('count', this.state.filterAmount);
        }
        formData.append('staff', this.state.user.id);
        formData.append('sort', this.state.sortBy);
        formData.append('order', this.state.orderBy);
        console.log('FORM : ' + JSON.stringify(formData));
        fetch(getSalary(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" SALARY " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data: responseJson.DATA,
                        loaded: true
                    });
                } else {
                    this.setState({
                        loaded: true
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    showDialogBox = (screen) => {
        let finishFunc = this.setFilter;
        if (screen == 'FilterDialogBox') {
            finishFunc = this.setFilter;
        } else if (screen == 'SortDialogBox') {
            finishFunc = this.setSortandOrder;
        }
        this.props.navigator.push({
            screen: screen, // unique ID registered with Navigation.registerScreen
            passProps: {
                onClose: this.closeDialogBox,
                onFinish: finishFunc,
                from: 'salary'
            }, // simple serializable object that will pass as props to the lightbox (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
            adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
        });
    };

    renderActions = (item) => {
        return (
            <Picker
                mode='dropdown'
                itemStyle={{ paddingHorizontal: 10 }}
                selectedValue={''}
                style={{ position: 'absolute', width: 25, height: 25, backgroundColor: 'transparent', top: -30 }}
                onValueChange={(itemValue, itemIndex) => {
                    if (itemValue == 'download') {
                        this.download(item.FILE);
                    } else if (itemValue == 'edit') {
                        this.openModal(item.ID, item.MONTH, item.YEAR, item.STAFF_NAME, item.STAFF_ID);
                    } else if (itemValue == 'delete') {
                        this.confirmDelete(item.ID);
                    }
                }}>
                <Picker.Item value='' label='Choose an action :' />
                <Picker.Item label="Download Attachment" value="download" />
                <Picker.Item label="Edit" value="edit" />
                <Picker.Item label="Delete" value="delete" />
            </Picker>
        );
        /*
        <TouchableOpacity onPress={()=>{this.download(item.FILE)}}>
            <Icon name="download" style={{paddingHorizontal:15, color:'black', fontSize:20}}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>{this.openModal(item.ID, item.MONTH, item.YEAR, item.STAFF_NAME, item.STAFF_ID)}}>
    <Icon name="create"  style={{paddingHorizontal:15, color:'black', fontSize:20}}/>
    </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.confirmDelete(item.ID)}>
            <Icon name="md-trash"  style={{paddingHorizontal:15, color:'black', fontSize:20}}/>
            </TouchableOpacity>
        */
    };

    render() {
        return (
            <Container style={{ backgroundColor: '#f8f8f8' }}>
                {
                    !this.state.loaded ?
                        <View style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                            <Spinner color='blue' />
                        </View>
                        :
                        _.size(this.state.data) != 0 || this.state.data.length != 0 ?
                            <FlatList
                                style={{ paddingHorizontal: 10, marginTop: 80 }}
                                data={this.state.data}
                                keyExtractor={(item, index) => item.id}
                                renderItem={
                                    ({ item }) => (
                                        <Card style={{ marginHorizontal: 10 }}>
                                            <CardItem style={{ padding: 10, paddingHorizontal: 5 }}>
                                                <Body style={{ flexDirection: 'column', paddingVertical: 5, paddingRight: 5 }}>

                                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                                                        {item.STAFF_NAME}.{'\t'}
                                                    </Text>

                                                    <Text style={{ fontSize: 15, color: '#a6a6a6', paddingTop: 5 }}>
                                                        For {this.dateConverter(item.MONTH) + ' ' + item.YEAR}
                                                    </Text>
                                                </Body>
                                                <Right style={{ flex: 0.1 }}>
                                                    {
                                                        this.renderActions(item)
                                                    }
                                                    <Icon name="md-more" style={{ position: 'absolute', fontSize: 20, top: -25, right: 10, color: 'black' }} />
                                                </Right>
                                            </CardItem>
                                        </Card>
                                    )
                                }
                            />
                            :
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Image style={{ width: 100, height: 100, alignSelf: 'center', marginTop: 20, opacity: 0.5 }} source={require('../images/empty.png')} />
                                <Text style={{ color: '#a6a6a6', fontSize: 15, alignSelf: 'center' }}>
                                    There is nothing to show
                                </Text>
                            </View>

                }

                {
                    /*
                    <Fab
                    onPress={()=>this.setState({fabActive:!this.state.fabActive})}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#5067FF' }}
                    position="bottomRight"
                    active={this.state.fabActive}>
                    {this.state.fabActive? <Icon name="close" /> : <Icon name="md-more" />}
                    <TouchableWithoutFeedback style={{ backgroundColor: '#34A34F' }} onPress={()=>this.showDialogBox('FilterDialogBox')}>
                        <Icon name="md-funnel" />
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback style={{ backgroundColor: '#3B5998' }} onPress={()=>this.showDialogBox('SortDialogBox')}>
                        <Icon name="md-list" />
                    </TouchableWithoutFeedback>
                    {
                        this.state.privilege.full_access_salary?
                            <TouchableWithoutFeedback style={{ backgroundColor: 'green' }} onPress={() => this.newSalary(this.state.user.id)}>
                                <Icon name="md-add" />
                            </TouchableWithoutFeedback>
                            :
                            null
                    }
                    </Fab>
                    */
                }
                <ActionButton
                    renderIcon={() => {
                        return (<Thumbnail source={require('../images/more-menu.png')} />)
                    }}
                    degrees={180}
                    buttonColor="#ef5350"
                    fixNativeFeedbackRadius={true}>
                    <ActionButton.Item size={40} buttonColor='#ef5350' title="Filter" onPress={() => this.showDialogBox('FilterDialogBox')}>
                        <Icon name="md-funnel" style={{ color: '#FFF', fontSize: 20 }} />
                    </ActionButton.Item>
                    <ActionButton.Item size={40} buttonColor='#ef5350' title="Sort" onPress={() => this.showDialogBox('SortDialogBox')}>
                        <Icon name="md-list" style={{ color: '#FFF', fontSize: 20 }} />
                    </ActionButton.Item>
                    <ActionButton.Item size={40} buttonColor='#66BB6A' title="New Salary" onPress={() => this.newSalary(this.state.user.id)}>
                        <Icon name="md-add" style={{ color: '#FFF', fontSize: 20 }} />
                    </ActionButton.Item>
                </ActionButton>
                <BouncyDrawer
                    willOpen={() => console.log('will open')}
                    didOpen={() => console.log('did open')}
                    willClose={() => console.log('will close')}
                    didClose={() => console.log('did close')}
                    title="HR System"
                    headerHeight={70}
                    titleStyle={{ color: '#fff', fontFamily: 'Helvetica Neue', fontSize: 20, marginLeft: -15, paddingTop: 20 }}
                    closedHeaderStyle={{ height: 80, backgroundColor: 'rgba(239,83,80,0.8)' }}
                    defaultOpenButtonIconColor="#fff"
                    defaultCloseButtonIconColor="#fff"
                    renderContent={this.renderContent}
                    openedHeaderStyle={{ backgroundColor: 'rgba(95, 95, 95,0.9)', paddingTop: 20 }}
                />
            </Container>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ef5350',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

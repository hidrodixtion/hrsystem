/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Alert,
    AsyncStorage,
    StatusBar,
    KeyboardAvoidingView
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Spinner, Icon } from 'native-base';
import { getLogin } from './API';
import HRSystemModule from '../nativebridge';
import _ from 'lodash';

type Props = {};
export default class first extends Component<Props> {
    constructor() {
        super();
        this.state = {
            username: "",
            password: "",
            loading: false,
            empty_username: false,
            empty_password: false
        };
    };

    showSnackbar = (msg) => {
        this.props.navigator.showSnackbar({
            text: msg,
            actionText: 'X', // optional
            actionId: 'fabClicked', // Mandatory if you've set actionText
            actionColor: 'white', // optional
            textColor: 'white', // optional
            backgroundColor: '#ef5350', // optional
            duration: 'indefinite' // default is `short`. Available options: short, long, indefinite
        });
    };

    cancel = () => {
        if (this.state.username != "" && this.state.password != "") {
            return false;
        } else {
            if (this.state.username == "") {
                this.setState({ empty_username: true });
            }
            if (this.state.password == "") {
                this.setState({ empty_password: true });
            }
            if (this.state.username == "" && this.state.password == "") {
                this.showSnackbar("Username and password can't be empty!");
            } else if (this.state.username == "") {
                this.showSnackbar("Username can't be empty!");
            } else {
                this.showSnackbar("Password can't be empty!");
            }

            return true;
        }
    };

    resetThis = () => {
        this.setState({
            empty_username: false,
            empty_password: false
        })
    };

    enterHome = () => {
        this.props.navigator.resetTo({
            screen: 'Home', // unique ID registered with Navigation.registerScreen
            passProps: {}, // simple serializable object that will pass as props to the pushed screen (optional)
            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
                screenBackgroundColor: 'white'
            }, // override the navigator style for the pushed screen (optional)
        });
    };

    getData = async () => {
        this.resetThis();
        if (this.cancel()) return;
        this.setState({ loading: true });
        let formData = new FormData();
        let uname = this.state.username;
        let pass = this.state.password;

        HRSystemModule.saveLoginDetail(uname, pass);
        // HRSystemModule.getLoginDetail((modUsername, modPass) => console.log("HRSystemModule", modUsername, modPass));

        /*
        formData.append('username','9114139KP');
        formData.append('password','pesan2gelas');
        */
        console.log('FORM : ' + this.state.username + '  ' + this.state.password);
        formData.append('code', uname);
        formData.append('password', pass);
        return fetch(getLogin(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" mitossss" + (JSON.stringify(responseJson)));
                this.setState({ loading: false });
                if (responseJson.msg.toLowerCase() == 'ok') {
                    try {
                        await AsyncStorage.setItem('user_code', this.state.username);
                        await AsyncStorage.setItem('user_pass', this.state.password);
                        await AsyncStorage.setItem('user_token', responseJson.DATA);
                        console.log("FINISH")
                    } catch (error) {
                        // Error saving data
                    }
                    this.enterHome();
                } else {
                    this.showSnackbar("Username or password incorrect!");
                }

                return responseJson;
            })
            .catch((error) => {
                console.error(error);
            });
    };

    _focusInput(inputField) {
        this[inputField]._root.focus();
    }

    render() {
        return (
            <Container>
                <Image
                    resizeMode="cover"
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }}
                    source={require('../images/bg1.png')}
                />
                <View style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    top: '15%'
                }}>
                    <Image style={{
                        width: 250,
                        height: 150
                    }}
                        resizeMode="contain" source={require('../images/logo.png')} />
                    <Text style={{ fontStyle: 'italic' }}>HR System</Text>
                </View>
                <View style={{
                    top: '25%',
                    paddingBottom: 15,
                    backgroundColor: 'rgba(255,255,255,0.5)',
                    marginHorizontal: 15
                }}>

                    <Form style={{
                        marginHorizontal: 20,
                        paddingBottom: 30
                    }}>
                        <Item floatingLabel error={this.state.empty_username}>
                            <Label>Username</Label>
                            <Input
                                onChangeText={(username) => { this.setState({ username: username }) }}
                                value={this.state.username}
                                autoCapitalize="none"
                                returnKeyType='next'
                                onSubmitEditing={() => this._focusInput('passwordInput')}
                            />
                            {
                                this.state.empty_username === true ?
                                    <Icon name='close-circle' />
                                    :
                                    null
                            }
                        </Item>
                        <Item floatingLabel error={this.state.empty_password}>
                            <Label>Password</Label>
                            <Input
                                onChangeText={(pass) => { this.setState({ password: pass }) }}
                                value={this.state.password}
                                secureTextEntry
                                returnKeyType='done'
                                getRef={(input) => this.passwordInput = input}
                                onSubmitEditing={() => this.getData()}
                            />
                            {
                                this.state.empty_password === true ? <Icon name='close-circle' /> : null
                            }
                        </Item>
                    </Form>

                    <Button full style={{ marginHorizontal: 10, backgroundColor: '#ef5350' }} onPress={() => { this.getData() }}>
                        {
                            this.state.loading == true ?
                                <Spinner color='white' />
                                :
                                <Text>Log In</Text>
                        }

                    </Button>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Keyboard,
    ToastAndroid,
    Alert
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner } from 'native-base';
import { getStaffData, getPic, saveEditSalary, submitNewSalary } from "./API";
import ListView from "deprecated-react-native-listview";
import _ from 'lodash';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const date = new Date();
const getyear = date.getFullYear();
const getmonth = date.getUTCMonth();
function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}

let years = Array.from(range(getyear - 10, getyear + 10, 1));
console.log("BULANKU : ", getmonth);
type Props = {};
export default class SortDialogBox extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            isFocused: false,
            resultBox: 50,
            empty: true,
            loaded: false,
            pickedSort: this.props.from == 'salary' ? 'create' : 'start',
            pickedOrder: 'desc'
        };
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flex: 0.85 }}>
                    <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                        <Body>
                            <Title>
                                SORT BY
                        </Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => { this.props.navigator.pop() }}>
                                <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                            </Button>
                        </Right>
                    </Header>
                    <Form style={{ marginTop: 10, paddingRight: 20 }}>
                        <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                            <Label style={{ alignSelf: 'flex-start' }}>
                                Sort By
                            </Label>
                            <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                {
                                    this.props.from == 'salary' ?
                                        <Picker
                                            mode="dropdown"
                                            selectedValue={this.state.pickedSort}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ pickedSort: itemValue.toLowerCase() })}>
                                            <Picker.Item label='Date created' value='create' />
                                            <Picker.Item label='Month' value='month' />
                                            <Picker.Item label='Year' value='year' />
                                            <Picker.Item label='Staff' value='staff' />
                                        </Picker>
                                        :
                                        <Picker
                                            mode="dropdown"
                                            selectedValue={this.state.pickedSort}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ pickedSort: itemValue.toLowerCase() })}>
                                            <Picker.Item label='Date created' value='start' />
                                            <Picker.Item label='Leave date' value='end' />
                                            <Picker.Item label='Purpose' value='purpose' />
                                        </Picker>
                                }
                            </View>
                        </Item>
                        <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                            <Label style={{ alignSelf: 'flex-start' }}>
                                Order By
                            </Label>
                            <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.pickedOrder}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ pickedOrder: itemValue.toString() })}>
                                    <Picker.Item label='Descending' value='desc' />
                                    <Picker.Item label='Ascending' value='asc' />
                                </Picker>
                            </View>
                        </Item>
                    </Form>
                </View>
                <View style={{ bottom: 0, flex: 0.15 }}>
                    <Body>

                    </Body>
                    <Right style={{ flexDirection: 'row' }}>
                        <Left>
                            <Button full light style={{ borderWidth: 0.3, borderColor: '#a6a6a6' }} onPress={() => this.props.navigator.pop()}>
                                <Text>CANCEL</Text>
                            </Button>
                        </Left>
                        <Right>
                            <Button full style={{ backgroundColor: '#ef5350' }} onPress={() => this.props.onFinish(this.state.pickedSort, this.state.pickedOrder)}>
                                <Text>SORT</Text>
                            </Button>
                        </Right>
                    </Right>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

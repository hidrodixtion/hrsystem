import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Alert,
    AsyncStorage,
    PixelRatio,
    TouchableOpacity,
    FlatList,
    ScrollView
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Spinner, Icon, Thumbnail } from 'native-base';
import { getLogin } from './API';
import RNFS from 'react-native-fs';
import _ from 'lodash';
export default class Try extends Component {
    state = {
        now: RNFS.ExternalStorageDirectoryPath,
        upper: "",
        data: [
            {
                name: ''
            }
        ],
    };

    constructor(props) {
        super(props);

    };

    componentWillMount() {
        this.selectFileTapped("");
    }

    selectFileTapped(folder) {
        let goTo = "";
        if (folder != "") {
            goTo = this.state.now + '/' + folder;
        } else {
            goTo = this.state.now;
        }
        RNFS.readDir(goTo) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
            .then((result) => {
                console.log("SIZE : " + (_.size(result)).toString());

                if (result == 0) {
                    Alert.alert('Empty Folder', 'This folder is empty');
                    return;
                }
                result.sort(this.compare);
                console.log("ISI : " + JSON.stringify(result));
                this.setState({
                    now: goTo,
                    data: result
                });
                let x = this.state.data[0].path;

                console.log('back ', this.state.upper);
                console.log('NOW', goTo);
                // stat the first file
            })
    }

    goUpOne() {
        let path = this.state.now.substr(0, this.state.now.lastIndexOf('/'));
        console.log("BACK TO : " + this.state.now.substr(0, this.state.now.lastIndexOf('/')));
        RNFS.readDir(path) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
            .then((result) => {
                console.log("SIZE : " + (_.size(result)).toString());
                if (result == 0) {
                    Alert.alert('KOSONG');
                    return;
                }
                result.sort(this.compare);
                console.log("ISI : " + JSON.stringify(result));
                this.setState({
                    data: result,
                    now: path
                });
                // stat the first file
            })
    };

    compare(a, b) {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    }

    fileOrFolder(item) {
        RNFS.stat(item.path).then((result) => {
            console.log(result);
            if (item.isFile()) {
                Alert.alert(
                    'Confirmation',
                    'Select this file?',
                    [
                        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        {
                            text: 'OK', onPress: () => {
                                this.props.finish(item.path);
                                this.props.navigator.pop();
                            }
                        },
                    ],
                    { cancelable: false }
                )
            } else {
                this.selectFileTapped(item.name);
            }
        }
        )
    };

    toShow(name) {
        let type = name.lastIndexOf('.');
        console.log("TIPENYA " + type + ' ' + name.length);
        if (type == -1) return require('../images/folder.png');
        let fileExt = name.substr(type + 1).toLowerCase();
        console.log("INI FILE : " + fileExt);
        if (fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'tiff') {
            return require('../images/picture.png');
        } else if (fileExt == 'doc' || fileExt == 'pdf') {
            return require('../images/document.png');
        } else if (fileExt == 'xlx' || fileExt == 'xlsx') {
            return require('../images/excel.png');
        } else {
            return require('../images/unknown.png');
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Button iconLeft bordered dark
                            onPress={() => this.goUpOne()}
                            style={{ alignSelf: 'flex-start' }}
                        >
                            <Icon name='arrow-back' />
                            <Text>Back</Text>
                        </Button>
                    </View>
                    <View>
                        <Button iconRight bordered dark
                            style={{ alignSelf: 'flex-end', paddingRight: 0, marginRight: 0 }}
                            onPress={() => this.props.navigator.pop()}
                        >
                            <Text>Close</Text>
                            <Icon name='close' style={{ paddingRight: 15, paddingLeft: 5, marginLeft: 0 }} />
                        </Button>
                    </View>
                </View>
                {console.log("MITOS? " + JSON.stringify(this.state.data[0].name))}
                <ScrollView
                    style={{ width: '100%' }}
                >
                    {this.state.data.map((item, key) => {
                        return (
                            <TouchableOpacity onPress={() => { this.fileOrFolder(item) }} style={{ alignItems: 'flex-start', borderBottomWidth: 1, width: '100%', padding: 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Thumbnail style={{ width: 30, height: 30, borderRadius: 15 }} source={this.toShow(item.name)} />
                                    <Text style={{ fontSize: 20, marginLeft: 15 }}>{item.name}</Text>
                                </View>
                            </TouchableOpacity>
                        );
                    })}
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        paddingTop: 25
    },
    button: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        margin: 5,
        padding: 5,
    },
    fileInfo: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        margin: 5,
        padding: 5,
    },
})

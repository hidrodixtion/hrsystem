/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import ActionButton from 'react-native-action-button';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    StatusBar,
    FlatList,
    AsyncStorage,
    Alert,
    ToastAndroid,
    Dimensions,
    TouchableWithoutFeedback,
    Picker
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Icon, Thumbnail, Card, CardItem, Body, Left, Right, Spinner, Fab } from 'native-base';
import BouncyDrawer from 'react-native-bouncy-drawer';
import { Navigation } from 'react-native-navigation';
import { getStaffData, getAnnouncement, getPic, getNotif, getCL, getAL, deleteAL, insertAL, edit_AL, changeStatusAL } from "./API";
import _ from 'lodash';

let index = 0;
const date = new Date();
const getyear = date.getFullYear();
const data = [
    { key: index++, section: true, label: 'Fruits' },
    { key: index++, label: 'Red Apples' },
    { key: index++, label: 'Cherries' },
    { key: index++, label: 'Cranberries', accessibilityLabel: 'Tap here for cranberries' },
    // etc...
    // Can also add additional custom keys which are passed to the onChange callback
    { key: index++, label: 'Vegetable', customKey: 'Not a fruit' }
];
type Props = {};
export default class Requested extends Component<Props> {
    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        //console.log('privi '+JSON.stringify(this.props.privilege));
        this.getData(this.state.user.id);
    };

    getData = async (id) => {
        let formData = new FormData();
        /*
        formData.append('username','9114139KP');
        formData.append('password','pesan2gelas');
        */
        console.log('FORM : ' + this.state.user.code + '  ' + this.state.user.token);
        formData.append('id', id);
        //formData.append('password', pass);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" dataku requested" + (JSON.stringify(responseJson)));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data_user: responseJson.DATA[0],
                        privilege: responseJson.DATA[0].PRIVILEGE
                    });
                    console.log("privilege " + JSON.stringify(responseJson.DATA[0].PRIVILEGE));
                    this.fetchMyRequest(this.state.user.id);
                    //console.log("DATA AL : ", this.state.data['CURRENT AL'][0].QUOTA)
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    fetchMyRequest = async (id) => {
        let formData = new FormData();
        formData.append('create', parseInt(id));
        formData.append('apply', parseInt(id));
        formData.append('sort', 'start');
        formData.append('sort_by', 'desc');
        if (this.state.privilege.approve_special_organization == true) {
            formData.append('special_organization', 't');
        }
        fetch(getAL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" MYREQUEST " + ((JSON.stringify(responseJson.DATA))));
                this.setState({
                    loaded: true
                });
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data: responseJson.DATA,
                        loaded: true
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    doDeleteAL = async (id, item) => {
        let formData = new FormData();
        formData.append('id', id);
        //formData.append('sort', 'create');
        console.log(formData);
        console.log(this.state.user.token);
        fetch(deleteAL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" DELETE? " + ((JSON.stringify(responseJson))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data2: responseJson.DATA,
                        loaded: false,
                        data: []
                    });
                    this.fetchFiltered(this.state.user.id);
                } else {
                    if (item.LAST_STATUS != 'draft') {
                        Alert.alert('Failed!',
                            'Cannot delete submitted request!',
                            [
                                { text: 'OK' }
                            ]
                        )
                    }
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    showDialogBox = (screen) => {
        let finishFunc = this.setSortandOrder;
        if (screen == 'FilterAL') {
            finishFunc = this.onFilterBack;
        } else if (screen == 'SortDialogBox') {
            finishFunc = this.setSortandOrder;
        }
        this.props.navigator.push({
            screen: screen, // unique ID registered with Navigation.registerScreen
            passProps: {
                onClose: this.closeDialogBox,
                onFinish: finishFunc,
                from: 'al',
                privilege: this.state.privilege,
                data_user: this.state.data_user
            }, // simple serializable object that will pass as props to the lightbox (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
            adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
        });
    };

    setSortandOrder = async (sort, order) => {
        if (sort.length == 0) {
            sort = 'desc';
        }
        if (order.length == 0) {
            order = 'start';
        }
        await this.setState({
            data: [],
            loaded: false,
            sortBy: sort,
            orderBy: order
        });
        console.log(sort + ' ' + order);
        this.props.navigator.pop();
        this.fetchFiltered(this.state.user.id);

    };

    submitAL = async (id, status) => {
        let formData = new FormData();
        formData.append('id', parseInt(id));
        formData.append('status', status);
        fetch(changeStatusAL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" MYREQUEST " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data: [],
                        loaded: false
                    });
                    this.fetchFiltered(this.state.user.id);
                    if (status == 'draft') {
                        ToastAndroid.show('Canceled succesfully!', ToastAndroid.SHORT);
                    } else if (status == 'submit') {
                        ToastAndroid.show('Submitted succesfully!', ToastAndroid.SHORT);
                    } else if (status == 'reject') {
                        ToastAndroid.show('Rejected succesfully!', ToastAndroid.SHORT);
                    } else if (status == 'approve') {
                        ToastAndroid.show('Approved succesfully!', ToastAndroid.SHORT);
                    }
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            go: 'tt',
            loaded: false,
            fabActive: false,
            sortBy: 'start',
            orderBy: 'desc',
            filterStaff: '',
            filterPurpose: '',
            filterDateStart: getyear + '-01-01',
            filterDateEnd: '',
            filterStatus: '',
            filterAmount: '',
            data_user: [],
            privilege: {},
            fabCLActive: false
        };
    }

    openLog(item) {
        this.props.navigator.push({
            screen: "AL_Log", // unique ID registered with Navigation.registerScreen
            passProps: {
                data: item.LOG,
                note: item.NOTE
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };

    openDetail(item) {
        this.props.navigator.push({
            screen: "AL_Details", // unique ID registered with Navigation.registerScreen
            passProps: {
                item: item,
                statusFormatter: this.statusFormatter
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };

    refresh = () => {
        this.setState({
            data: [],
            loaded: false
        });
        this.fetchFiltered(this.state.user.id);
    };

    successCL = () => {
        this.props.navigator.pop();
        this.props.navigator.showSnackbar({
            text: 'CL successfully added',
            actionText: 'X', // optional
            actionId: 'fabClicked', // Mandatory if you've set actionText
            actionColor: 'white', // optional
            textColor: 'white', // optional
            backgroundColor: '#66BB6A', // optional
            duration: 'short' // default is `short`. Available options: short, long, indefinite
        });
    };

    openNewCl() {
        this.props.navigator.push({
            screen: "NewCL", // unique ID registered with Navigation.registerScreen
            passProps: {
                staffId: this.state.user.id,
                finish: this.successCL,
                privilege: this.state.privilege,
                userData: this.state.data_user
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };
    openCL_History() {
        this.props.navigator.push({
            screen: "HistoryCL", // unique ID registered with Navigation.registerScreen
            passProps: {
                staffId: this.state.user.id,
                privilege: this.state.privilege,
                userData: this.state.data_user
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };
    openNewAL() {
        this.props.navigator.push({
            screen: "NewAL", // unique ID registered with Navigation.registerScreen
            passProps: {
                staffId: this.state.user.id,
                finish: this.refresh,
                privilege: this.state.privilege
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };

    monthFormatter = (date) => {
        month = date.split('-')[1];
        if (month == '01') {
            month = "Jan";
        } else if (month == '02') {
            month = "Feb";
        } else if (month == '03') {
            month = "March";
        } else if (month == '04') {
            month = "April";
        } else if (month == '05') {
            month = "May";
        } else if (month == '06') {
            month = "June";
        } else if (month == '07') {
            month = "July";
        } else if (month == '08') {
            month = "Aug";
        } else if (month == '09') {
            month = "Sept";
        } else if (month == '10') {
            month = "Oct";
        } else if (month == '11') {
            month = "Nov";
        } else if (month == '12') {
            month = "Dec";
        }

        return date.split('-')[2] + '-' + month + '-' + date.split('-')[0];
    };

    onFilterBack = async (staff, purpose, dateStart, dateEnd, status, amount) => {
        await this.setState({
            filterStaff: staff,
            filterPurpose: purpose,
            filterDateStart: dateStart,
            filterDateEnd: dateEnd,
            filterStatus: status,
            filterAmount: amount,
            data: [],
            loaded: false
        });
        this.props.navigator.pop();
        this.fetchFiltered(this.state.user.id);
    };

    fetchFiltered = async (id) => {
        let formData = new FormData();
        if (this.state.filterStaff.length != 0) {
            formData.append('staff', this.state.filterStaff);
        }
        if (this.state.filterPurpose.length != 0) {
            formData.append('purpose', this.state.filterPurpose);
        }
        if (this.state.filterDateStart.length != 0) {
            formData.append('start', this.state.filterDateStart);
        }
        if (this.state.filterDateEnd.length != 0) {
            formData.append('end', this.state.filterDateEnd);
        }
        if (this.state.filterStatus.length != 0) {
            formData.append('status', this.state.filterStatus);
        }
        if (this.state.filterAmount.length != 0) {
            formData.append('count', this.state.filterAmount);
        }
        if (this.state.privilege.approve_special_organization == true) {
            formData.append('special_organization', 't');
        }
        formData.append('create', parseInt(id));
        formData.append('apply', parseInt(id));
        formData.append('sort', this.state.sortBy);
        formData.append('sort_by', this.state.orderBy);
        //formData.append('sort', 'create');
        console.log(formData);
        fetch(getAL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" MYREQUEST " + ((JSON.stringify(responseJson.DATA))));
                this.setState({
                    loaded: true
                });
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data: responseJson.DATA,
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    statusFormatter = (str) => {
        let res = '';
        if (str == 'reject') {
            res = 'Rejected'
        } else if (str == 'approve') {
            res = 'Approved'
        } else if (str == 'submit') {
            res = 'Submitted'
        } else if (str == 'draft') {
            res = 'Drafted'
        }
        return res;
    };
    bgColor = (stats) => {
        let col = '';
        if (stats == 'Approved') {
            col = '#66BB6A'
        } else if (stats == 'Rejected') {
            col = '#ef5350'
        } else if (stats == 'Submitted') {
            col = 'yellow'
        } else if (stats == 'Drafted') {
            col = '#a6a6a6'
        }
        return col;
    };

    submitable = (str) => {
        return str == 'draft';

    };

    edit_AL = (item) => {
        this.props.navigator.push({
            screen: "EditAL", // unique ID registered with Navigation.registerScreen
            passProps: {
                staffId: this.state.user.id,
                finish: this.refresh,
                item: item
            }, // simple serializable object that will pass as props to the modal (optional)
            animationType: 'slide-up',// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up'),
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
            },
        });
    };

    submitDraft = async (item) => {
        /*let formData = new FormData();
        if(item.PURPOSE!=null){
            formData.append('purpose', item.PURPOSE);
        }
        formData.append('id', item.ID);
        formData.append('start', item.START);
        formData.append('end', item.END);
        formData.append('duration', item.DURATION);
        formData.append('approve', this.state.user.id);
        formData.append('final', this.state.user.id);
        formData.append('apply', item.APPLY_ID);
        formData.append('status', 'submit');
        console.log("FORM : " + JSON.stringify(formData));
        fetch(edit_AL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token' : this.state.user.token
            },
            body : formData
        })
            .then((response) =>response.json())
            .then(async (responseJson) => {
                console.log(" BERHASIL? " + ((JSON.stringify(responseJson))));
                if(responseJson.msg.toLowerCase() == 'ok'){
                    this.setState({
                        data : [],
                        loaded : false
                    });
                    ToastAndroid.show('Submitted!', ToastAndroid.SHORT);
                }else if(responseJson.msg.toLowerCase() == 'failed'){
                    Alert.alert(
                        'Failed',
                        responseJson.DATA,
                        [
                            {text: 'OK'}
                        ]
                    );
                }
                this.fetchFiltered(this.state.user.id);
            })
            .catch((error) => {
                console.error(error);
            });
            */
    };

    _renderForSubmitted(item) {
        if (item.CREATE_ID == this.state.user.id) {
            return (
                <Picker
                    mode='dropdown'
                    itemStyle={{ paddingHorizontal: 10 }}
                    selectedValue={''}
                    style={{ position: 'absolute', width: 25, height: 25, backgroundColor: 'transparent', top: -40 }}
                    onValueChange={(itemValue, itemIndex) => {
                        if (itemValue == 'det') {
                            this.openDetail(item)
                        } else if (itemValue == 'log') {
                            this.openLog(item)
                        }
                        //else if(itemValue == 'approve'){
                        //     Alert.alert(
                        //         'Warning!',
                        //         'Are you sure want to approve this ' + item.NOTE.toUpperCase() + ' request?',
                        //         [
                        //             {text: 'APPROVE', onPress : ()=>this.submitAL(item.ID,'approve')},
                        //             {text: 'CANCEL'}
                        //         ]
                        //     )
                        // }else if(itemValue == 'reject'){
                        //     Alert.alert(
                        //         'Warning!',
                        //         'Are you sure want to reject this ' + item.NOTE.toUpperCase() + ' submission?',
                        //         [
                        //             {text: 'REJECT', onPress : ()=>this.submitAL(item.ID,'reject')},
                        //             {text: 'CLOSE'}
                        //         ]
                        //     )
                        // }
                        else if (itemValue == 'cancel') {
                            Alert.alert(
                                'Warning!',
                                'Are you sure want to cancel this ' + item.NOTE.toUpperCase() + ' submission?',
                                [
                                    { text: 'CANCEL SUBMISSION', onPress: () => this.submitAL(item.ID, 'draft') },
                                    { text: 'CLOSE' }
                                ]
                            )
                        }
                    }}>
                    <Picker.Item value='' label='Choose an action :' />
                    <Picker.Item label="Detail" value="det" />
                    <Picker.Item label="Log" value="log" />
                    <Picker.Item label="Cancel" value="cancel" />
                </Picker>
            )
        } else {
            return <Picker
                mode='dropdown'
                itemStyle={{ paddingHorizontal: 10 }}
                selectedValue={''}
                style={{ position: 'absolute', width: 25, height: 25, backgroundColor: 'transparent', top: -40 }}
                onValueChange={(itemValue, itemIndex) => {
                    if (itemValue == 'det') {
                        this.openDetail(item)
                    } else if (itemValue == 'log') {
                        this.openLog(item)
                    } else if (itemValue == 'approve') {
                        Alert.alert(
                            'Warning!',
                            'Are you sure want to approve this ' + item.NOTE.toUpperCase() + ' request?',
                            [
                                { text: 'APPROVE', onPress: () => this.submitAL(item.ID, 'approve') },
                                { text: 'CANCEL' }
                            ]
                        )
                    } else if (itemValue == 'reject') {
                        Alert.alert(
                            'Warning!',
                            'Are you sure want to reject this ' + item.NOTE.toUpperCase() + ' submission?',
                            [
                                { text: 'REJECT', onPress: () => this.submitAL(item.ID, 'reject') },
                                { text: 'CLOSE' }
                            ]
                        )
                    }
                }}>
                <Picker.Item value='' label='Choose an action :' />
                <Picker.Item label="Detail" value="det" />
                <Picker.Item label="Log" value="log" />


            </Picker>
        }
    };

    _renderForDraft(item) {
        return (
            <Picker
                mode='dropdown'
                itemStyle={{ paddingHorizontal: 10 }}
                selectedValue={''}
                style={{ position: 'absolute', width: 25, height: 25, backgroundColor: 'transparent', top: -40 }}
                onValueChange={(itemValue, itemIndex) => {
                    if (itemValue == 'det') {
                        console.log('why me')
                        this.openDetail(item)
                    } else if (itemValue == 'log') {
                        this.openLog(item)
                    } else if (itemValue == 'send') {
                        Alert.alert(
                            'Warning!',
                            'Are you sure want to submit this ' + item.NOTE.toUpperCase() + ' request?',
                            [
                                { text: 'SUBMIT', onPress: () => this.submitAL(item.ID, 'submit') },
                                { text: 'CANCEL' }
                            ]
                        )
                    } else if (itemValue == 'edit') {
                        this.edit_AL(item)
                    } else if (itemValue == 'delete') {
                        Alert.alert(
                            'Warning!',
                            'Are you sure want to delete this ' + item.NOTE.toUpperCase() + ' request?',
                            [
                                { text: 'DELETE', onPress: () => this.doDeleteAL(item.ID, item) },
                                { text: 'CANCEL' }
                            ]
                        )
                    }
                }}>
                <Picker.Item value='' label='Choose an action :' />
                <Picker.Item label="Detail" value="det" />
                <Picker.Item label="Log" value="log" />
                <Picker.Item label="Send" value="send" />
                <Picker.Item label="Edit" value="edit" />
                <Picker.Item label="Delete" value="delete" />
            </Picker>
        );
    }

    _renderForApproved(item) {
        return (
            <Picker
                mode='dropdown'
                itemStyle={{ paddingHorizontal: 10 }}
                selectedValue={''}
                style={{ position: 'absolute', width: 25, height: 25, backgroundColor: 'transparent', top: -40 }}
                onValueChange={(itemValue, itemIndex) => {
                    if (itemValue == 'det') {
                        console.log('why me')
                        this.openDetail(item)
                    } else if (itemValue == 'log') {
                        this.openLog(item)
                    } else if (itemValue == 'reject') {
                        Alert.alert(
                            'Warning!',
                            'Are you sure want to reject this ' + item.NOTE.toUpperCase() + ' submission?',
                            [
                                { text: 'REJECT', onPress: () => this.submitAL(item.ID, 'reject') },
                                { text: 'CLOSE' }
                            ]
                        )
                    }
                }}>
                <Picker.Item value='' label='Choose an action :' />
                <Picker.Item label="Detail" value="det" />
                <Picker.Item label="Log" value="log" />
                <Picker.Item label="Reject" value="reject" />
            </Picker>
        );
    };


    render() {
        return (
            <Container style={{ backgroundColor: '#f8f8f8' }}>
                {
                    !this.state.loaded ?
                        <View style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                            <Spinner color='blue' />
                        </View>
                        :
                        _.size(this.state.data) != 0 ?
                            <FlatList
                                style={{ paddingHorizontal: 10, }}
                                contentContainerStyle={{ paddingBottom: 70 }}
                                data={this.state.data}
                                keyExtractor={(item, index) => item.id}
                                renderItem={
                                    ({ item }) => (
                                        <Card style={{ marginHorizontal: 10, paddingHorizontal: 10, paddingBottom: 5 }}>
                                            <CardItem cardBody style={{ padding: 10, paddingHorizontal: 5 }}>
                                                <Body style={{ flexDirection: 'column', paddingVertical: 5, paddingRight: 5 }}>
                                                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                                        <View style={{ flex: 1 }}>
                                                            <Text style={{ fontSize: 18, fontWeight: 'bold', textAlignVertical: 'center' }}>
                                                                {item.NOTE.toUpperCase()} : {item.PURPOSE}{'\t'}
                                                            </Text>
                                                        </View>
                                                        <View style={{
                                                            backgroundColor: this.bgColor(this.statusFormatter(item.LAST_STATUS)),
                                                            borderRadius: 100,
                                                            padding: 5,
                                                            marginLeft: 10,
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                            alignSelf: 'center',
                                                        }}>
                                                            <Text style={{ fontSize: 15, fontWeight: 'bold', color: item.LAST_STATUS == 'submit' ? 'black' : '#FFF' }}>
                                                                {this.statusFormatter(item.LAST_STATUS)}
                                                            </Text>
                                                        </View>
                                                    </View>

                                                    <Text style={{ fontSize: 15, paddingTop: 5, textAlignVertical: 'center' }}>
                                                        <Icon style={{ color: '#a6a6a6', fontSize: 15 }} name="md-calendar" />
                                                        <Text style={{ fontSize: 15, color: '#a6a6a6', textAlignVertical: 'center' }}>
                                                            {'\t'}{'\t'}{this.monthFormatter(item.START)} until {this.monthFormatter(item.END)}{'\n'}{'\t'}{'\t'}{'\t'}({item.DURATION} days)
                                                    </Text>
                                                    </Text>
                                                    <Text style={{ fontSize: 15, paddingTop: 5, textAlignVertical: 'center' }}>
                                                        <Icon style={{ color: '#a6a6a6', fontSize: 15 }} name="contact" />
                                                        <Text style={{ fontSize: 15, color: '#a6a6a6', textAlignVertical: 'center' }}>
                                                            {'\t'}{'\t'}{item.APPLY_NAME}
                                                        </Text>
                                                    </Text>
                                                </Body>
                                                <Right style={{ flex: 0.1 }}>
                                                    {
                                                        item.LAST_STATUS == 'draft' || item.LAST_STATUS == 'final_reject' || item.LAST_STATUS == 'reject' ?
                                                            this._renderForDraft(item)
                                                            :
                                                            null
                                                    }
                                                    {
                                                        item.LAST_STATUS == 'approve' || item.LAST_STATUS == 'final_approve' ?
                                                            this._renderForApproved(item) : null
                                                    }
                                                    {
                                                        item.LAST_STATUS == 'submit' ?
                                                            this._renderForSubmitted(item) : null
                                                    }
                                                    <Icon name="md-more" style={{ position: 'absolute', fontSize: 20, top: -35, right: 10, color: 'black' }} />
                                                </Right>
                                            </CardItem>
                                        </Card>
                                    )
                                }
                            />
                            :
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Image style={{ width: 100, height: 100, alignSelf: 'center', marginTop: 20, opacity: 0.5 }} source={require('../images/empty.png')} />
                                <Text style={{ color: '#e0e0e0', fontSize: 15, alignSelf: 'center' }}>
                                    There is nothing to show
                                </Text>
                            </View>
                }
                {
                    this.state.privilege.insert_cl_all || this.state.privilege.insert_dept_cl ?
                        <ActionButton
                            renderIcon={() => {
                                return (<Thumbnail source={require('../images/more-menu.png')} />)
                            }}
                            degrees={180}
                            buttonColor="#ef5350"
                            fixNativeFeedbackRadius={true}>
                            <ActionButton.Item size={40} buttonColor='transparent' title="CL History" onPress={() => this.openCL_History()}>
                                <Thumbnail source={require('../images/history.png')} />
                            </ActionButton.Item>
                            <ActionButton.Item size={40} buttonColor='#a6a6a6' title="New CL" onPress={() => this.openNewCl()}>
                                <Icon name="md-add" style={{ color: '#FFF', fontSize: 20 }} />
                            </ActionButton.Item>
                            <ActionButton.Item size={40} buttonColor='#ef5350' title="Filter" onPress={() => this.showDialogBox('FilterAL')}>
                                <Icon name="md-funnel" style={{ color: '#FFF', fontSize: 20 }} />
                            </ActionButton.Item>
                            <ActionButton.Item size={40} buttonColor='#ef5350' title="Sort" onPress={() => this.showDialogBox('SortDialogBox')}>
                                <Icon name="md-list" style={{ color: '#FFF', fontSize: 20 }} />
                            </ActionButton.Item>
                            <ActionButton.Item size={40} buttonColor='#66BB6A' title="New Leave" onPress={() => this.openNewAL()}>
                                <Icon name="md-add" style={{ color: '#FFF', fontSize: 20 }} />
                            </ActionButton.Item>
                        </ActionButton>
                        :
                        <ActionButton
                            renderIcon={() => {
                                return (<Thumbnail source={require('../images/more-menu.png')} />)
                            }}
                            degrees={180}
                            buttonColor="#ef5350"
                            fixNativeFeedbackRadius={true}>
                            <ActionButton.Item size={40} buttonColor='transparent' title="CL History" onPress={() => this.openCL_History()}>
                                <Thumbnail source={require('../images/history.png')} />
                            </ActionButton.Item>
                            <ActionButton.Item size={40} buttonColor='#ef5350' title="Filter" onPress={() => this.showDialogBox('FilterAL')}>
                                <Icon name="md-funnel" style={{ color: '#FFF', fontSize: 20 }} />
                            </ActionButton.Item>
                            <ActionButton.Item size={40} buttonColor='#ef5350' title="Sort" onPress={() => this.showDialogBox('SortDialogBox')}>
                                <Icon name="md-list" style={{ color: '#FFF', fontSize: 20 }} />
                            </ActionButton.Item>
                            <ActionButton.Item size={40} buttonColor='#66BB6A' title="New Leave" onPress={() => this.openNewAL()}>
                                <Icon name="md-add" style={{ color: '#FFF', fontSize: 20 }} />
                            </ActionButton.Item>
                        </ActionButton>
                }

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ef5350',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

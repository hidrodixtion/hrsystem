/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    ListView,
    AsyncStorage,
    Keyboard,
    ToastAndroid,
    FlatList
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner, Card, CardItem } from 'native-base';
import { getStaffData, getPic, saveEditSalary } from "./API";
import _ from 'lodash';

type Props = {};
export default class AL_Log extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            st: this.props.def,
            isFocused: false,
            resultBox: 50,
            query: this.props.applicant,
            empty: true,
            loaded: false,
            data: this.props.data
        };
        console.log("now1 :", this.state.pickedMonth)
    };

    monthFormatter = (full_date) => {
        date = full_date.split(' ')[0];
        month = date.split('-')[1];
        if (month == '01') {
            month = "Jan";
        } else if (month == '02') {
            month = "Feb";
        } else if (month == '03') {
            month = "March";
        } else if (month == '04') {
            month = "April";
        } else if (month == '05') {
            month = "May";
        } else if (month == '06') {
            month = "June";
        } else if (month == '07') {
            month = "July";
        } else if (month == '08') {
            month = "Aug";
        } else if (month == '09') {
            month = "Sept";
        } else if (month == '10') {
            month = "Oct";
        } else if (month == '11') {
            month = "Nov";
        } else if (month == '12') {
            month = "Dec";
        }

        return date.split('-')[2] + '-' + month + '-' + date.split('-')[0] + ' ' + full_date.split(' ')[1];
    };

    completeString(str) {
        let completed = '';
        if (str == 'final_reject' || str == 'reject') {
            completed = 'rejected.';
        } else if (str == 'approve' || str == 'final_approve') {
            completed = 'approved.';
        } else if (str == 'draft') {
            completed = 'saved as draft.';
        } else if (str == 'submit') {
            completed = 'submitted, now waiting for approval.';
        }

        return completed;
    };

    render() {
        return (
            <View style={styles.container}>
                <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                    <Body>
                        <Title>
                            Logs
                    </Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => { this.props.navigator.pop() }}>
                            <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                        </Button>
                    </Right>
                </Header>
                <FlatList
                    style={{ paddingHorizontal: 10 }}
                    data={this.state.data}
                    keyExtractor={(item, index) => item.id}
                    renderItem={
                        ({ item }) => (
                            <Card>
                                <CardItem header>
                                    <Text style={{ color: 'grey' }}>{this.monthFormatter(item.date)}</Text>
                                </CardItem>
                                <CardItem style={{ marginTop: -20 }}>
                                    <View>
                                        <Text>{this.props.note.toUpperCase()} request {this.completeString(item.status)}</Text>
                                        <Text style={{ fontSize: 14 }}>by {item.staff_name}</Text>
                                    </View>
                                </CardItem>
                                <CardItem footer>

                                </CardItem>
                            </Card>
                        )
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

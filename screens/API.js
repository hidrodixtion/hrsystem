/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Alert,
    AsyncStorage
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label } from 'native-base';
import _ from 'lodash';

type Props = {};

//let server = 'http://10.151.30.141/tmy_human_resource/';
// let server = 'https://human-resource.telinmy.hash.id/';
let server = 'https://hr.telin.com.my/';
let login = server + 'API/auth/login';
let staffData = server + 'API/staff/getStaff';
let announcement = server + 'API/announcement/getAnnouncement';
let pic = 'https://telinmystore.blob.core.windows.net/human-resource/';
let notif = server + 'API/staff/inboxStaff';
let read = server + 'API/staff/inboxSee';
let salary = server + 'API/salary/getSalary';
let editSalary = server + 'API/salary/editSalary';
let newSalary = server + 'API/salary/insertSalary';
let deleteSalary = server + 'API/salary/deleteSalary';
let cl_quota = server + 'API/absence_leave/get_cl';
let absence_and_leave = server + 'API/absence_leave/getLeave';
let delete_AL = server + 'API/absence_leave/deleteLeave';
let newAL = server + 'API/absence_leave/insertLeave';
let quota = server + 'API/absence_leave/getQuota';
let date_leave = server + 'API/absence_leave/date_leave';
let editAL = server + 'API/absence_leave/editLeave';
let changeAL = server + 'API/absence_leave/changeStatus';
let newCL = server + 'API/absence_leave/insertCl';
let CLHistory = server + 'API/absence_leave/get_cl';
let delete_CL = server + 'API/absence_leave/deleteCl';
let editProfile = server + 'API/staff/editMinor';
let get_duration = server + 'API/absence_leave/find_duration';
let mac_absence = server + 'API/presence_api/insertpresence';
let get_absence = server + 'API/presence_api/getAbsencePresence';
export function getLogin() {
    return login;
}

export function getStaffData() {
    return staffData;
}

export function getAnnouncement() {
    return announcement;
}

export function getPic(name) {
    return pic + name;
}

export function getNotif() {
    return notif;
}

export function readNotif() {
    return read;
}

export function getSalary() {
    return salary;
}

export function salarySlip(item) {
    return salarySlipLink + item;
}

export function saveEditSalary() {
    return editSalary;
}

export function submitNewSalary() {
    return newSalary;
}

export function deleteSalarySlip() {
    return deleteSalary;
}

export function getCL() {
    return cl_quota;
}

export function getAL() {
    return absence_and_leave;
}

export function deleteAL() {
    return delete_AL;
}

export function insertAL() {
    return newAL;
}

export function getQuota() {
    return quota;
}
export function getDateleave() {
    return date_leave;
}

export function edit_AL() {
    return editAL;
}

export function changeStatusAL() {
    return changeAL;
}

export function insertCL() {
    return newCL;
}

export function getCL_History() {
    return CLHistory;
}

export function deleteCL() {
    return delete_CL;
}

export function edit_profile() {
    return editProfile;
}

export function getDuration() {
    return get_duration;
}

export function submitAbsence() {
    return mac_absence;
}

export function getAbsence() {
    return get_absence;
}

export default class API extends Component<Props> {
    constructor() {
        super();
    };

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

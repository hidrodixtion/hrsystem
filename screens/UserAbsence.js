/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Keyboard,
    ToastAndroid,
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner, Card, CardItem } from 'native-base';
import { getStaffData, getPic, saveEditSalary, submitNewSalary, insertAL, getQuota, getAL } from "./API";
import DatePicker from 'react-native-datepicker';
import ListView from "deprecated-react-native-listview";
import _ from 'lodash';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const date = new Date();
const getday = date.getDate();
const getyear = date.getFullYear();
const getmonth = date.getUTCMonth();
function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}

type Props = {};
export default class UserAbsence extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            st: this.props.def,
            isFocused: false,
            resultBox: 50,
            emptyAbsence: false,
            loadedAbsence: false,
            data: [],
            dateStart: "" + getday + '-' + this.monthFormatter(getmonth) + '-' + getyear,
            dateEnd: "" + getday + '-' + this.monthFormatter(getmonth) + '-' + getyear,
            lower: true,
            dataAbsence: []
        };
        console.log("now1 :", this.state.pickedMonth)
    };

    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
            this.fetchAbsenceToday(this.props.today, this.props.today)
        } catch (error) {
            // Error retrieving data
        }
    };

    monthFormatter = (month) => {
        month = month + 1;
        if (month == '1') {
            month = "Jan";
        } else if (month == '2') {
            month = "Feb";
        } else if (month == '3') {
            month = "Mar";
        } else if (month == '4') {
            month = "Apr";
        } else if (month == '5') {
            month = "May";
        } else if (month == '6') {
            month = "June";
        } else if (month == '7') {
            month = "July";
        } else if (month == '8') {
            month = "Aug";
        } else if (month == '9') {
            month = "Sept";
        } else if (month == '10') {
            month = "Oct";
        } else if (month == '11') {
            month = "Nov";
        } else if (month == '12') {
            month = "Dec";
        }
        month = month.toLowerCase();
        return month;
    };

    dateFormatter = (date) => {
        date_splitted = date.split('-');
        year = date_splitted[0];
        month = date_splitted[1];
        day = date_splitted[2];
        if (month == '01') {
            month = "January";
        } else if (month == '02') {
            month = "February";
        } else if (month == '03') {
            month = "March";
        } else if (month == '04') {
            month = "April";
        } else if (month == '05') {
            month = "May";
        } else if (month == '06') {
            month = "June";
        } else if (month == '07') {
            month = "July";
        } else if (month == '08') {
            month = "August";
        } else if (month == '09') {
            month = "September";
        } else if (month == '10') {
            month = "October";
        } else if (month == '11') {
            month = "November";
        } else if (month == '12') {
            month = "December";
        }
        //month = month.toLowerCase();
        return day + ' ' + month + ' ' + year;
    };

    _refresh = () => {
        if (this.state.lower) {
            this.setState({
                dataAbsence: [],
                loadedAbsence: false,
                emptyAbsence: false
            });
            this.fetchAbsenceToday(this.state.dateStart, this.state.dateEnd)
        }
        this.setState({ lower: !this.state.lower });
    };

    fetchAbsenceToday = async (start, end) => {
        let formData = new FormData();
        formData.append('start', start);
        formData.append('end', end);
        formData.append('status', 'approve');
        console.log(formData);
        fetch(getAL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" Absence today " + ((JSON.stringify(responseJson.DATA))));
                this.setState({
                    loadedAbsence: true
                });
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        dataAbsence: responseJson.DATA,
                    });
                } else {
                    this.setState({
                        emptyAbsence: true
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    inverseMonth = (month) => {
        month = month.toLowerCase();
        if (month == 'jan') {
            month = '00';
        } else if (month == 'feb') {
            month = '01';
        } else if (month == 'mar') {
            month = '02';
        } else if (month == 'apr') {
            month = '03';
        } else if (month == 'may') {
            month = '04';
        } else if (month == 'jun') {
            month = '05';
        } else if (month == 'jul') {
            month = '06';
        } else if (month == 'aug') {
            month = '07';
        } else if (month == 'sep') {
            month = '08';
        } else if (month == 'oct') {
            month = '09';
        } else if (month == 'nov') {
            month = '10';
        } else if (month == 'dec') {
            month = '11';
        }
        return month;
    };
    changeScreen = (target) => {
        this.props.navigator.resetTo({
            screen: target, // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {
                privilege: this.state.privilege
            }, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light',
                screenBackgroundColor: '#FFF'
            }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'fade' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    };
    render() {
        return (
            <View style={styles.container}>
                <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                    <Body>
                        <Title>
                            User Absence
                    </Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => { this.props.navigator.pop() }}>
                            <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                        </Button>
                    </Right>
                </Header>
                {
                    this.state.lower ?
                        <Form style={{ paddingHorizontal: 20, marginBottom: 10 }}>
                            <View style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start', color: 'black' }}>
                                    Leaving date (Start)
                                </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <DatePicker
                                        style={{ width: '100%' }}
                                        date={this.state.dateStart}
                                        mode="date"
                                        androidMode="spinner"
                                        placeholder="select date"
                                        format="DD-MMM-YYYY"
                                        minDate={"01-01-" + (getyear - 10)}
                                        maxDate={"31-12-" + (getyear + 10)}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => {
                                            let a = this.state.dateEnd.split('-');
                                            let b = new Date(a[2], this.inverseMonth(a[1]), a[0]);
                                            let c = date.split('-');
                                            let d = new Date(c[2], this.inverseMonth(c[1]), c[0]);
                                            console.log(b);
                                            console.log(d > b);
                                            d > b ?
                                                this.setState({
                                                    dateStart: date,
                                                    dateEnd: date
                                                })
                                                :
                                                this.setState({ dateStart: date })
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start', color: 'black' }}>
                                    Leaving date (End)
                                </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <DatePicker
                                        style={{ width: '100%' }}
                                        date={this.state.dateEnd}
                                        mode="date"
                                        androidMode="spinner"
                                        placeholder="select date"
                                        format="DD-MMM-YYYY"
                                        minDate={"01-01-" + (getyear - 10)}
                                        maxDate={"31-12-" + (getyear + 10)}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => {
                                            let a = this.state.dateStart.split('-');
                                            let b = new Date(a[2], this.inverseMonth(a[1]), a[0]);
                                            let c = date.split('-');
                                            let d = new Date(c[2], this.inverseMonth(c[1]), c[0]);
                                            d < b ?
                                                this.setState({
                                                    dateStart: date,
                                                    dateEnd: date
                                                })
                                                :
                                                this.setState({ dateEnd: date })
                                        }}
                                    />
                                </View>
                            </View>
                        </Form>
                        :
                        null
                }
                <Button full
                    style={{ backgroundColor: '#ef5350' }}
                    onPress={() => this._refresh()}>
                    {
                        this.state.lower ?
                            <Text>
                                Set Date
                            </Text>
                            :
                            <Thumbnail style={{ height: 20, width: 20 }} source={require('../images/down.png')} />
                    }
                </Button>
                <ScrollView style={{ paddingHorizontal: 10 }}>
                    {
                        this.state.loadedAbsence ?
                            this.state.emptyAbsence ?
                                <View>
                                    <Image style={{ width: 100, height: 100, alignSelf: 'center', marginTop: 20, opacity: 0.5 }} source={{ uri: 'http://188.166.242.112/telinhk_dev/frontend/assets/images/empty.png' }} />
                                    <Text style={{ color: '#a6a6a6', fontSize: 15, alignSelf: 'center' }}>
                                        There is nothing to show
                                </Text>
                                </View>
                                :
                                (
                                    this.state.dataAbsence.map(item => {
                                        return (
                                            <Card style={{ backgroundColor: '#f8f8f8', flexDirection: 'row' }}>
                                                <View style={{ flex: 1 }}>
                                                    <CardItem header style={{ flexDirection: 'row', backgroundColor: 'transparent' }}>
                                                        <Left style={{ flex: 0.25 }}>
                                                            <Thumbnail source={{ uri: getPic(item.APPLY_PHOTO) }} style={{ width: 50, height: 50, borderRadius: 25 }} />
                                                        </Left>
                                                        <Body style={{ justifyContent: 'center' }}>
                                                            <Text style={{ fontSize: 17, alignSelf: 'flex-start', textAlignVertical: 'center', paddingLeft: 8 }}>{item.APPLY_NAME}</Text>
                                                        </Body>
                                                    </CardItem>
                                                    <CardItem style={{ marginTop: -15, backgroundColor: 'transparent' }}>
                                                        <Left style={{ flex: 0.35, flexDirection: 'column' }}>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Text style={{ fontWeight: '700' }}>Start  </Text>
                                                                <Text>:</Text>
                                                            </View>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Text style={{ fontWeight: '700' }}>Finish</Text>
                                                                <Text style={{ textAlign: 'right' }}>:</Text>
                                                            </View>
                                                        </Left>
                                                        <Body style={{ flexDirection: 'column', paddingLeft: 20 }}>
                                                            <Text style={{ fontSize: 17, alignSelf: 'flex-start' }}>{this.dateFormatter(item.START)}</Text>
                                                            <Text style={{ fontSize: 17, alignSelf: 'flex-start' }}>{this.dateFormatter(item.END)}</Text>
                                                        </Body>
                                                    </CardItem>
                                                </View>

                                                <View style={{ backgroundColor: item.NOTE.toLowerCase() == 'al' ? '#ef5350' : '#a6a6a6', height: 50, width: 50, borderRadius: 25, justifyContent: 'center', alignItems: 'center', top: 50, right: 20 }}>
                                                    <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#FFF' }}>
                                                        {item.NOTE.toUpperCase()}
                                                    </Text>
                                                </View>
                                            </Card>
                                        )
                                    })
                                )
                            :
                            <View>
                                <Spinner color='blue' />
                            </View>
                    }
                </ScrollView>
                <View style={{ alignSelf: 'center', marginTop: 10, paddingBottom: 5, backgroundColor: '#FFF' }}>
                    <Button rounded iconLeft style={{ backgroundColor: '#ef5350' }} onPress={() => { this.changeScreen('Home') }}>

                        <Icon name='md-arrow-dropleft' />
                        <Text>BACK</Text>
                    </Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#f8f8f8',
        borderRadius: 5,
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

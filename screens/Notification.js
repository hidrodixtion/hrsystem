/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    StatusBar,
    TouchableOpacity,
    FlatList,
    ToastAndroid,
    AsyncStorage,
    Alert
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Left, Body, Right, Title, Icon, Card, CardItem, Thumbnail, Spinner } from 'native-base';
import { getStaffData, getAnnouncement, getPic, getNotif, readNotif } from "./API";
import _ from 'lodash';

type Props = {};
export default class Notification extends Component<Props> {
    pop = () => {
        this.props.navigator.pop({
            animated: true, // does the pop have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
        });
    };

    moveScreen = (action) => {
        this.props.navigator.resetTo({
            screen: 'TopTabs', // unique ID registered with Navigation.registerScreen
            passProps: {
                initialPage: action
            }, // simple serializable object that will pass as props to the pushed screen (optional)
            animated: true, // does the resetTo have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the resetTo have different transition animation (optional)
            navigatorStyle: {
                navBarHidden: true,
                drawUnderStatusBar: true,
                statusBarColor: 'transparent',
                statusBarTextColorScheme: 'light'
            }, // override the navigator style for the pushed screen (optional)
        });
    };

    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        this.fetchNotifCL(this.state.user.id);
    };
    compare(a, b) {
        if (a.last_nom < b.last_nom)
            return -1;
        if (a.last_nom > b.last_nom)
            return 1;
        return 0;
    }
    fetchNotifCL = async (id) => {
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('seen', 'true');

        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notifCL " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        dataCL: responseJson.DATA,
                    });
                }
                this.fetchNotif(id);
                console.log("JUM " + _.size(this.state.dataCL))

            })
            .catch((error) => {
                console.error(error);
            });
    };

    fetchNotif = async (id) => {
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('seen', 'false');

        fetch(getNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" notif " + ((JSON.stringify(responseJson))));
                this.setState({
                    data: _.merge(this.state.dataCL, responseJson.DATA),
                    loaded: true
                });

            })
            .catch((error) => {
                console.error(error);
            });
    };

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            user: {},
            dataCL: [],
            loaded: false
        };
    };
    showSnackbar = () => {
        this.props.navigator.showSnackbar({
            text: 'All notification has been read!',
            textColor: 'white', // optional
            backgroundColor: 'green', // optional
            duration: 'short' // default is `short`. Available options: short, long, indefinite
        });
    };

    _read = async (id, status) => {
        let formData = new FormData();
        if (id != 'all') {
            formData.append('id', parseInt(id));
        } else {
            formData.append('id', "");
            this.showSnackbar()
        }
        let act = "";
        if (status == 'submit') {
            act = 0;
        } else if (status == 'approve') {
            act = 1;
        }
        fetch(readNotif(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(JSON.stringify(responseJson));
                console.log("SCREEN : " + act);
                if (id != 'all') {
                    this.moveScreen(act);
                }
                this.fetchNotifCL(this.state.user.id);
            })
            .catch((error) => {
                console.error(error);
            });
    };

    render() {
        return (
            <Container>
                <Image
                    resizeMode="cover"
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }}
                    source={{ uri: "http://188.166.242.112/telinhk_dev/frontend/assets/images/telinhk_bg.png" }}
                />
                <StatusBar translucent backgroundColor='transparent' />
                <Header style={{ marginTop: 25, elevation: 0, backgroundColor: 'transparent', borderBottomWidth: 0.3, marginHorizontal: 10 }}>
                    <Left style={{ flex: 1 }}>
                        <Text style={{ alignSelf: 'flex-end', color: 'black', fontSize: 17 }}>
                            NOTIFICATION
                        </Text>
                    </Left>
                    <Body style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Button transparent onPress={() => { this._read('all') }}>
                            <Text style={{ fontSize: 12, padding: 10, color: '#a6a6a6' }}>
                                Read All
                            </Text>
                        </Button>
                    </Body>
                    <Right style={{ flex: 0.7 }}>
                        <Button transparent iconLeft onPress={() => { this.pop() }}>
                            <Icon name="close" style={{ padding: 10, color: '#000000' }} />
                        </Button>
                    </Right>
                </Header>
                {
                    this.state.loaded == true ?
                        _.size(this.state.data) != 0 ?
                            <FlatList
                                style={{ paddingHorizontal: 10, marginTop: 15 }}
                                data={this.state.data}
                                extraData={this.state}
                                keyExtractor={(item, index) => item.ID}
                                renderItem={
                                    ({ item }) => (
                                        <TouchableOpacity onPress={() => this._read(item.ID, item.STATUS)}>
                                            <Card style={{ marginHorizontal: 10 }}>
                                                {
                                                    item.ACTION == 'al' ?
                                                        <CardItem cardBody style={{
                                                            padding: 10,
                                                            paddingHorizontal: 5,
                                                            backgroundColor: item.SEEN == true ?
                                                                '#FFF' : '#e0e0e0'
                                                        }}>
                                                            <Left style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center', padding: 5 }}>
                                                                <Thumbnail source={{ uri: getPic(item.FROM_PHOTO) }} />
                                                            </Left>
                                                            <Body style={{ flexDirection: 'column', paddingVertical: 5, paddingRight: 5 }}>
                                                                <Text style={{ fontSize: 15, fontStyle: 'italic' }}>
                                                                    Absence and Leave
                                                            </Text>
                                                                <Text>
                                                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                                                                        {item.FROM_NAME}{'\t'}
                                                                    </Text>
                                                                    {
                                                                        item.STATUS == 'final_approve' || item.STATUS == 'approve' ?
                                                                            <Text>
                                                                                approved an Absence and Leave request
                                                                            </Text>
                                                                            :
                                                                            item.STATUS == 'submit' ?
                                                                                <Text>
                                                                                    sent you an Absence and Leave request
                                                                            </Text>
                                                                                :
                                                                                item.STATUS == 'reject' ?
                                                                                    <Text>
                                                                                        rejected an Absence and Leave request
                                                                                </Text>
                                                                                    :
                                                                                    null

                                                                    }
                                                                </Text>
                                                                <Text style={{ fontSize: 15, color: 'black', paddingTop: 5 }}>
                                                                    {item.DATETIME}
                                                                </Text>
                                                            </Body>
                                                        </CardItem>
                                                        :
                                                        <CardItem cardBody style={{
                                                            padding: 10,
                                                            paddingHorizontal: 5,
                                                            backgroundColor: item.SEEN == true ?
                                                                '#FFF' : '#e0e0e0'
                                                        }}>
                                                            <Left style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center', padding: 5 }}>
                                                                <Thumbnail source={{ uri: getPic(item.FROM_PHOTO) }} />
                                                            </Left>
                                                            <Body style={{ flexDirection: 'column', paddingVertical: 5, paddingRight: 5 }}>
                                                                <Text style={{ fontSize: 15, fontStyle: 'italic' }}>
                                                                    Compensation leave
                                                            </Text>
                                                                <Text>
                                                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                                                                        {item.FROM_NAME}{'\t'}
                                                                    </Text>
                                                                    <Text>
                                                                        give you additional Compensation leave
                                                                </Text>
                                                                </Text>
                                                                <Text style={{ fontSize: 15, color: 'black', paddingTop: 5 }}>
                                                                    {item.DATETIME}
                                                                </Text>
                                                            </Body>
                                                        </CardItem>
                                                }
                                            </Card>
                                        </TouchableOpacity>
                                    )
                                }
                            />
                            :
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Image style={{ width: 100, height: 100, alignSelf: 'center', marginTop: 20, opacity: 0.5 }} source={require('../images/empty.png')} />
                                <Text style={{ color: '#e0e0e0', fontSize: 15, alignSelf: 'center' }}>
                                    There is nothing to show
                                </Text>
                            </View>
                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Spinner color='blue' />
                        </View>

                }

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

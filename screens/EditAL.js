/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    FlatList,
    Image,
    Dimensions,
    Picker,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Keyboard,
    ToastAndroid,
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import { Container, Header, Content, Button, Text, Form, Item, Input, Label, Body, Left, Right, Title, Icon, Thumbnail, Spinner } from 'native-base';
import { getStaffData, getPic, saveEditSalary, submitNewSalary, insertAL, getQuota, getDuration, edit_AL } from "./API";
import DatePicker from 'react-native-datepicker';
import ListView from "deprecated-react-native-listview";
import _ from 'lodash';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const date = new Date();
const getday = date.getUTCDay();
const getyear = date.getFullYear();
const getmonth = date.getUTCMonth();
function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}

let years = Array.from(range(getyear - 10, getyear + 10, 1));
console.log("BULANKU : ", getmonth);
type Props = {};
export default class EditAL extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            st: this.props.def,
            dataSource: ds.cloneWithRows(['row 1', 'row 2']),
            isFocused: false,
            resultBox: 50,
            query: this.props.item.APPLY_NAME,
            empty: true,
            loaded: false,
            pickedQuota: this.props.item.NOTE.toLowerCase(),
            myQuota: {},
            quota_AL: '',
            quota_CL: '',
            quota_med: '',
            total: '',
            quota_medhos: '',
            quota_matl: '',
            quota_haj: '',
            qouta_umr: '',
            quota_marl: '',
            quota_patl: '',
            quota_el: '',
            quota_ql: '',
            quota_lwp: '',
            quota_compl: '',
            leave_duration: '',
            leave_duration_loading: false,
            date_leave: [],
            date_half: [],
            date_leave_loading: false,
            pickedStaffId: this.props.item.APPLY_ID,
            purpose: this.props.item.PURPOSE,
            dateStart: this.myFormat(this.props.item.START),
            dateEnd: this.myFormat(this.props.item.END),
        };
        console.log("sekarang :", this.state.dateStart);
        this.myFormat(this.state.dateStart);
    };

    submitNewAL = async (status) => {
        let formData = new FormData();
        let arr = this.state.date_half;
        let finalstring = ''
        for (let i = 0; i < arr.length; i++) {
            finalstring += arr[i];
            if (i != arr.length - 1) {
                finalstring += ';'
            }

        }
        formData.append('id', this.props.item.ID);
        formData.append('purpose', this.state.purpose);
        formData.append('start', this.submitFormat(this.state.dateStart));
        formData.append('end', this.submitFormat(this.state.dateStart));
        formData.append('approve', this.state.user.id);
        formData.append('final', this.state.user.id);
        formData.append('apply', this.state.pickedStaffId);
        formData.append('status', status);
        formData.append('duration', this.props.item.DURATION);
        formData.append('dateLeave', finalstring)
        formData.append('phone', "1")
        console.log("FORM : " + JSON.stringify(formData));
        fetch(edit_AL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" BERHASIL? " + ((JSON.stringify(responseJson))));
                if (responseJson.msg.toLowerCase() == 'ok') {
                    this.props.finish();
                    this.props.navigator.pop();
                } else if (responseJson.msg.toLowerCase() == 'failed') {
                    Alert.alert(
                        'Failed',
                        responseJson.DATA,
                        [
                            { text: 'OK' }
                        ]
                    );
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    static capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    submitFormat = (date) => {
        day = date.split('-')[0];
        month = date.split('-')[1].toLowerCase();
        year = date.split('-')[2];
        if (day.length < 2) {
            day = '0' + day;
        }
        if (month == 'jan') {
            month = '01';
        } else if (month == 'feb') {
            month = '02';
        } else if (month == 'mar') {
            month = '03';
        } else if (month == 'apr') {
            month = '04';
        } else if (month == 'MAY') {
            month = '05';
        } else if (month == 'jun') {
            month = '06';
        } else if (month == 'jul') {
            month = '07';
        } else if (month == 'aug') {
            month = '08';
        } else if (month == 'sep') {
            month = '09';
        } else if (month == 'oct') {
            month = '10';
        } else if (month == 'nov') {
            month = '11';
        } else if (month == 'dec') {
            month = '12';
        }
        console.log('READY ' + year + '-' + month + '-' + day);
        return '' + year + '-' + month + '-' + day;
    };

    myFormat = (date) => {
        part = date.split('-');
        console.log("FULL " + part[2] + '-' + this.monthFormatter(part[1]) + '-' + part[0]);
        return part[2] + '-' + this.monthFormatter(part[1]) + '-' + part[0];
    };

    inverseMonth = (month) => {
        month = month.toLowerCase();
        if (month == 'jan') {
            month = '00';
        } else if (month == 'feb') {
            month = '01';
        } else if (month == 'mar') {
            month = '02';
        } else if (month == 'apr') {
            month = '03';
        } else if (month == 'MAY') {
            month = '04';
        } else if (month == 'jun') {
            month = '05';
        } else if (month == 'jul') {
            month = '06';
        } else if (month == 'aug') {
            month = '07';
        } else if (month == 'sep') {
            month = '08';
        } else if (month == 'oct') {
            month = '09';
        } else if (month == 'nov') {
            month = '10';
        } else if (month == 'dec') {
            month = '11';
        }
        return month;
    };

    monthFormatter = (month) => {
        if (month == '01') {
            month = "Jan";
        } else if (month == '02') {
            month = "Feb";
        } else if (month == '03') {
            month = "Mar";
        } else if (month == '04') {
            month = "Apr";
        } else if (month == '05') {
            month = "May";
        } else if (month == '06') {
            month = "June";
        } else if (month == '07') {
            month = "July";
        } else if (month == '08') {
            month = "Aug";
        } else if (month == '09') {
            month = "Sept";
        } else if (month == '10') {
            month = "Oct";
        } else if (month == '11') {
            month = "Nov";
        } else if (month == '12') {
            month = "Dec";
        }
        month = month.toLowerCase();
        return month;
    };

    filterName = (query) => {
        let names = this.state.rawData;
        const filterRes = names.filter(x => {
            return x.NAME.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        console.log("MYSIZE : " + _.size(filterRes));
        if (_.size(filterRes) != 0) {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: null
            });
        } else {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: true
            });
        }

    };
    fetch_leaveDuration = async () => {
        this.setState({ leave_duration_loading: true });
        let formData = new FormData();
        formData.append('start', this.submitFormat(this.state.dateStart));
        formData.append('end', this.submitFormat(this.state.dateEnd));
        fetch(getDuration(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    console.log('DURASI : ' + JSON.stringify(responseJson.DATA));
                    this.setState({
                        leave_duration: responseJson.DATA.toString(),
                        leave_duration_loading: false
                    });
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    fetchMyName = async () => {
        let formData = new FormData();
        formData.append('id', this.props.staffId);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    console.log('SIAPA SAYA : ' + JSON.stringify(responseJson.DATA[0].NAME));
                    this.setState({
                        query: responseJson.DATA[0].NAME
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    fetch_Dateleave = async () => {
        this.setState({ date_leave_loading: true });
        let formData = new FormData();
        formData.append('start', this.submitFormat(this.state.dateStart));
        formData.append('end', this.submitFormat(this.state.dateEnd));
        formData.append('staff', this.props.staffId);
        fetch(getDateleave(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    let switchvalue = new Array(responseJson.DATA.length).fill(false)
                    console.log('DATE_LEAVE : ' + JSON.stringify(responseJson.DATA));
                    this.setState({
                        date_leave: responseJson.DATA,
                        date_leave_loading: false,
                        switchValue: switchvalue
                    });
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    get_Quota = async () => {
        let formData = new FormData();
        formData.append('staff', this.props.staffId);
        fetch(getQuota(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    console.log('QUOTA : ' + JSON.stringify(responseJson.DATA));
                    console.log('QUOTA HAJU : ' + JSON.stringify(responseJson.DATA.HAJ.QUOTA_MAX));
                    this.setState({
                        myQuota: responseJson.DATA,
                        quota_AL: responseJson.DATA.AL[0].QUOTA,
                        quota_CL: responseJson.DATA.CL.QUOTA,
                        quota_med: responseJson.DATA.MED.QUOTA,
                        quota_medhos: responseJson.DATA.MEDHOS.QUOTA,
                        quota_matl: responseJson.DATA.MATL.QUOTA_MAX,
                        quota_haj: responseJson.DATA.HAJ,
                        quota_umr: responseJson.DATA.UMR.QUOTA_MAX,
                        quota_marl: responseJson.DATA.MARL.QUOTA_MAX,
                        quota_patl: responseJson.DATA.PATL.QUOTA_MAX,
                        quota_compl: responseJson.DATA.COMPL,
                        //total : this.state.quota_AL+this.state.myQuota.CL
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    fetchStaff = async () => {
        let formData = new FormData();
        formData.append('sort', 'name');
        formData.append('sort_by', 'asc');
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" STAFF " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        dataSource: ds.cloneWithRows(responseJson.DATA),
                        rawData: responseJson.DATA,
                        loaded: true
                    });
                    this.filterName("");
                }
            })
            .catch((error) => {
                console.error(error);
            });
        console.log(this.state.dataSource);
    };
    renderDateLeave(item, index) {
        //  item=this.state.date_leave
        return (
            <TouchableOpacity
                style={{
                    paddingVertical: 10,
                    borderBottomWidth: 0.2,
                    flexDirection: 'row'
                }}>
                <Left style={{ flex: 0.5, paddingLeft: 15 }}>
                    <Text style={{ fontSize: 15, alignSelf: 'flex-start' }}>{item}</Text>
                </Left>
                <Left style={{ flex: 0.2, paddingLeft: 30 }}>
                    <Text style={{ fontSize: 12, alignSelf: 'flex-start' }}>Full Day</Text>
                </Left>
                <Body style={{ flex: 0.1, paddingLeft: 30 }}>
                    <Switch
                        onValueChange={(value) => this.toggleSwitch(value, item, index)}
                        value={this.state.switchValue[index]}
                    />
                </Body>
                <Left style={{ flex: 0.2, paddingLeft: 15 }}>
                    <Text style={{ fontSize: 12, alignSelf: 'flex-start' }}>Half Day </Text>
                </Left>
            </TouchableOpacity>
        )
    }
    toggleSwitch = (value, item, index) => {
        switchvalue = this.state.switchValue
        switchvalue[index] = value


        //onValueChange of the  switch this function will be called
        this.setState({
            switchValue: switchvalue
        })
        var arr = this.state.date_half
        var index = arr.indexOf(item);
        console.log('HALFT' + this.state.date_half)
        console.log('indexa' + item)
        if (index > -1) {
            arr.splice(index, 1);
            console.log('HALF-1' + arr)
        }
        else {
            arr.push(item)
            console.log('HALF2' + arr)
        }
        let duration = this.state.date_leave.length - (this.state.date_half.length * 0.5)
        this.setState({
            date_half: arr,
            leave_duration: duration
        })

        console.log('HALF' + this.state.date_half)

        //state changes according to switch
        //which will result in re-render the text
    }

    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        this.fetchStaff();
        this.fetchMyName();
        this.get_Quota();
        this.fetch_Dateleave();
        this.fetch_leaveDuration();
    };

    render() {
        return (
            <View style={styles.container}>
                <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                    <Body>
                        <Title>
                            Edit AL Request
                    </Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => { this.props.navigator.pop() }}>
                            <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                        </Button>
                    </Right>
                </Header>
                <KeyboardAvoidingView behavior="padding" style={{ flex: 0.918 }}>
                    <ScrollView style={{ flex: 1 }}>
                        <Form style={{ marginTop: 10, paddingRight: 20 }}>
                            <Item style={{ flexDirection: 'column' }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Applicant
                                </Label>
                                <ScrollView
                                    showsVerticalScrollIndicator={false}
                                    scrollEnabled={false}
                                    keyboardDismissMode='none'
                                    keyboardShouldPersistTaps="handled"
                                    style={{
                                        width: '100%',
                                        borderWidth: 1,
                                        borderColor: '#a6a6a6',
                                        height: this.state.resultBox,
                                    }}
                                >
                                    <View style={{ width: '100%', height: 50 }}>
                                        <Input
                                            onFocus={() => this.setState({ resultBox: 230 })}
                                            onBlur={() => this.setState({ resultBox: 50 })}
                                            style={{ width: '100%', height: '100%' }}
                                            selectTextOnFocus={true}
                                            value={this.state.query}
                                            onChangeText={(text) => {
                                                console.log('textku : ' + text);
                                                this.setState({ query: text });
                                                this.filterName(text);
                                            }}
                                            getRef={(input) => this.searchBox = input}
                                            placeholder="Search"
                                        />
                                    </View>
                                    {
                                        this.state.empty && this.state.loaded ?
                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={{ fontSize: 20 }}>No results found</Text>
                                            </View>
                                            :
                                            !this.state.loaded ?
                                                <View style={{ width: '100%' }}>
                                                    <Spinner color='blue' />
                                                </View>
                                                :
                                                <ListView
                                                    keyboardShouldPersistTaps='always'
                                                    style={{ height: 180, width: '100%' }}
                                                    enableEmptySections
                                                    dataSource={this.state.dataSource}
                                                    renderRow={(rowData) => (
                                                        <TouchableOpacity
                                                            style={{
                                                                paddingVertical: 10,
                                                                borderBottomWidth: 0.2,
                                                                flexDirection: 'row'
                                                            }}
                                                            onPress={() => {
                                                                this.setState({
                                                                    query: rowData.NAME,
                                                                    pickedStaffId: rowData.ID,
                                                                    resultBox: 50
                                                                });
                                                                Keyboard.dismiss()
                                                            }}
                                                        >
                                                            <Left style={{ flex: 0.3, paddingLeft: 15 }}>
                                                                <Thumbnail source={{ uri: getPic(rowData.PHOTO) }} />
                                                            </Left>
                                                            <Body>
                                                                <Text style={{ fontSize: 20, alignSelf: 'flex-start' }}>{rowData.NAME}</Text>
                                                            </Body>

                                                        </TouchableOpacity>
                                                    )}
                                                />
                                    }
                                </ScrollView>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Choose leave quota
                                </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <Picker
                                        mode="dropdown"
                                        selectedValue={this.state.pickedQuota}
                                        onValueChange={(itemValue, itemIndex) => this.setState({ pickedQuota: itemValue })}>
                                        <Picker.Item label='Annual Leave (AL)' value='al' />
                                        <Picker.Item label='Compassionate Leave (COMPL)' value='compl' />
                                        <Picker.Item label='Examination Leave (EL)' value='el' />
                                        <Picker.Item label='Leave Without Pay (LWP)' />
                                        <Picker.Item label='Medical Leave (MED)' value='med' />
                                        <Picker.Item label='Medical Leave Hospitalization (MEDHOS) ' value='medhos' />
                                        <Picker.Item label='Maternity Leave (MATL)' value='matl' />
                                        <Picker.Item label='Marriage Leave (MARL)' value='marl' />
                                        <Picker.item label='Paternity Leave (PATL)' value='patl' />
                                        <Picker.Item label='Pilgrimage Leave (HAJ)' value='haj' />
                                        <Picker.Item label='Prolonged Illnes Leave (PIL)' value='pil' />
                                        <Picker.item label='Quarantine Leave (QL)' />
                                        <Picker.Item label='Umrah Leave (UMR)' value='umr' />


                                    </Picker>
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start', color: 'black' }}>

                                    {this.state.pickedQuota == 'med' ? ('REMAINING MEDICAL QUOTA: ' + this.state.quota_med)
                                        : this.state.pickedQuota == 'al' ? ('REMAINING QUOTA: AL QUOTA + CL QUOTA: ' + this.state.quota_AL + '+' + this.state.myQuota.CL)
                                            : this.state.pickedQuota == 'medhos' ? ('REMAINING MEDICAL HOSPITALIZATION QUOTA:' + this.state.quota_medhos)
                                                : this.state.pickedQuota == 'marl' ? ('REMAINING MARRIAGE QUOTA: ' + this.state.quota_marl)
                                                    : this.state.pickedQuota == 'haj' ? ('REMAINING PILGRIMAGE QUOTA: ' + this.state.quota_haj.QUOTA_MAX)
                                                        : this.state.pickedQuota == 'matl' ? ('REMAINING MATERNITY QUOTA: ' + this.state.quota_matl)
                                                            : this.state.pickedQuota == 'patl' ? ('REMAINING PATERNITY QUOTA: ' + this.state.quota_patl)
                                                                : this.state.pickedQuota == 'umr' ? ('REMAINING UMRAH QUOTA: ' + this.state.quota_umr)
                                                                    : this.state.pickedQuota == 'compl' ? ('REMAINING COMPASSIONATE QUOTA: ' + this.state.quota_compl)
                                                                        : ''}
                                </Label>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Leaving date (Start)
                                </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <DatePicker
                                        style={{ width: '100%' }}
                                        date={this.state.dateStart}
                                        mode="date"
                                        androidMode="spinner"
                                        placeholder="select date"
                                        format="DD-MMM-YYYY"
                                        minDate={"01-01-" + (getyear - 10)}
                                        maxDate={"31-12-" + (getyear + 10)}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => {
                                            let a = this.state.dateEnd.split('-');
                                            let b = new Date(a[2], this.inverseMonth(a[1]), a[0]);
                                            let c = date.split('-');
                                            let d = new Date(c[2], this.inverseMonth(c[1]), c[0]);
                                            console.log(b);
                                            console.log(d > b);
                                            d > b ?
                                                this.setState({
                                                    dateStart: date,
                                                    dateEnd: date
                                                })
                                                :
                                                this.setState({ dateStart: date })
                                            this.fetch_leaveDuration();
                                            this.fetch_Dateleave();
                                        }
                                        }
                                    />
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Leaving date (End)
                                </Label>
                                <View style={{ width: '100%', borderWidth: 1, borderColor: '#a6a6a6' }}>
                                    <DatePicker
                                        style={{ width: '100%' }}
                                        date={this.state.dateEnd}
                                        mode="date"
                                        androidMode="spinner"
                                        placeholder="select date"
                                        format="DD-MMM-YYYY"
                                        minDate={"01-01-" + (getyear - 10)}
                                        maxDate={"31-12-" + (getyear + 10)}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => {
                                            let a = this.state.dateStart.split('-');
                                            let b = new Date(a[2], this.inverseMonth(a[1]), a[0]);
                                            let c = date.split('-');
                                            let d = new Date(c[2], this.inverseMonth(c[1]), c[0]);
                                            console.log('asdasd ' + d + ' - ' + b);
                                            console.log('fuuuck ' + d < b);
                                            d < b ?
                                                this.setState({
                                                    dateStart: date,
                                                    dateEnd: date
                                                })
                                                :
                                                this.setState({ dateEnd: date })
                                            this.fetch_leaveDuration();
                                            this.fetch_Dateleave();
                                        }}
                                    />
                                </View>
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Leave Duration
                            </Label>
                                <View style={{ width: '100%', justifyContent: 'flex-start', flexDirection: 'row' }}>

                                    <Input
                                        multiline={true}
                                        keyboardType='numeric'
                                        value={this.state.leave_duration.toString()}
                                        onChangeText={(text) => this.setState({ leave_duration: text })}
                                        style={{ width: '75%', height: 40, textAlignVertical: 'center', borderWidth: 1, borderColor: '#a6a6a6', paddingBottom: 5, paddingTop: 5 }}
                                    />
                                    {
                                        this.state.leave_duration_loading ?
                                            <View style={{ marginLeft: 8, justifyContent: 'center', borderWidth: 0.4 }}>
                                                <Spinner style={{ width: 30, height: 30, paddingHorizontal: 10 }} color='rgba(239,83,80,0.8)' />
                                            </View>
                                            :
                                            <TouchableOpacity style={{ marginLeft: 8, justifyContent: 'center', borderWidth: 0.4 }} onPress={() => { this.fetch_leaveDuration() }}>
                                                <Image source={require('../images/duration.png')} style={{ width: 30, height: 30, paddingHorizontal: 10 }} />
                                            </TouchableOpacity>
                                    }
                                </View>

                                {
                                    this.state.empty_duration ?
                                        <Label style={{ color: 'red', fontSize: 15 }}>
                                            Empty field are not allowed!
                                    </Label>
                                        :
                                        null
                                }
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Date Leave Duration
                            </Label>
                                <View style={{ width: '100%', justifyContent: 'flex-start', flexDirection: 'row' }}>
                                    <FlatList
                                        data={this.state.date_leave}
                                        extraData={this.state}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({ item, index }) => this.renderDateLeave(item, index)}
                                    />
                                </View>
                                {
                                    this.state.empty_duration ?
                                        <Label style={{ color: 'red', fontSize: 15 }}>
                                            Empty field are not allowed!
                                    </Label>
                                        :
                                        null
                                }
                            </Item>
                            <Item style={{ flexDirection: 'column', paddingTop: 10 }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Purpose of leaving
                                </Label>
                                <View style={{ width: '100%', height: 150, borderWidth: 1, justifyContent: 'flex-start' }}>
                                    <Input
                                        multiline={true}
                                        value={this.state.purpose}
                                        onChangeText={(text) => this.setState({ purpose: text })}
                                        style={{ width: '100%', height: '100%', textAlignVertical: 'top' }}
                                    />
                                </View>
                            </Item>
                        </Form>
                    </ScrollView>
                </KeyboardAvoidingView>
                <View style={{ flex: 0.082, flexDirection: 'row' }}>
                    <Left>
                        <Button full light style={{ borderWidth: 0.3, borderColor: '#a6a6a6' }} onPress={() => this.props.navigator.pop()}>
                            <Text>CANCEL</Text>
                        </Button>
                    </Left>
                    <Body>
                        <Button full style={{ backgroundColor: '#a6a6a6' }} onPress={() => this.submitNewAL('draft')}>
                            <Text>DRAFT</Text>
                        </Button>
                    </Body>
                    <Right>
                        <Button full style={{ backgroundColor: '#66BB6A' }} onPress={() => this.submitNewAL('submit')}>
                            <Text>SUBMIT</Text>
                        </Button>
                    </Right>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    title: {
        fontSize: 17,
        fontWeight: '700',
    },
    content: {
        marginTop: 10,
    },
});

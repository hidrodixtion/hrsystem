/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    StatusBar,
    FlatList,
    AsyncStorage,
    Alert,
    ToastAndroid,
    Dimensions,
    Keyboard
} from 'react-native';
import { Container, Header, Title, Content, Button, Text, Form, Item, Input, Label, Icon, Thumbnail, Card, CardItem, Body, Left, Right, Spinner, Fab } from 'native-base';
import BouncyDrawer from 'react-native-bouncy-drawer';
import { Navigation } from 'react-native-navigation';
import { getStaffData, getAnnouncement, getPic, getNotif, getCL, getAL, deleteAL, insertAL, edit_AL, deleteCL, getCL_History } from "./API";
import ListView from "deprecated-react-native-listview";
import _ from 'lodash';

type Props = {};
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
export default class HistoryCL extends Component<Props> {
    async componentWillMount() {
        try {
            const mem_token = await AsyncStorage.getItem("user_token");
            const mem_code = await AsyncStorage.getItem("user_code");
            const mem_pass = await AsyncStorage.getItem("user_pass");
            const mem_id = await AsyncStorage.getItem("user_id");
            if (mem_token !== null) {
                // We have data!!
                this.setState({
                    user: {
                        token: mem_token,
                        code: mem_code,
                        pass: mem_pass,
                        id: mem_id
                    }
                });
                console.log('tester ' + (this.state.user.code));
            } else console.log('test' + (value));
        } catch (error) {
            // Error retrieving data
        }
        //console.log('privi '+JSON.stringify(this.props.privilege));
        this.getData(this.state.user.id);
        this.fetchStaff();
        this.fetchMyName();
    };

    fetchStaff = async () => {
        let formData = new FormData();
        formData.append('sort', 'name');
        formData.append('sort_by', 'asc');
        console.log('MY STAFF ID ' + this.props.staffId);
        if (this.props.privilege.insert_dept_cl == true && this.props.privilege.insert_cl_all == false) {
            formData.append('organization', this.props.userData.organization);
        }
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" STAFFku " + ((JSON.stringify(responseJson.DATA))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    if (this.props.privilege.create_own_al == false) {
                        for (let i = 0; i < responseJson.DATA.length; i++) {
                            // look for the entry with a matching `code` value
                            if (responseJson.DATA[i].NAME == this.state.query) {
                                // we found it
                                // obj[i].name is the matched result
                                delete responseJson.DATA[i];
                                this.setState({
                                    query: ''
                                });
                            }
                        }
                    }
                    this.setState({
                        dataSource: ds.cloneWithRows(responseJson.DATA),
                        rawData: responseJson.DATA,
                        loaded: true
                    });
                    this.filterName("");
                }
            })
            .catch((error) => {
                console.error(error);
            });
        console.log(this.state.dataSource);
    };

    filterName = (query) => {
        let names = this.state.rawData;
        const filterRes = names.filter(x => {
            return x.NAME.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        console.log("MYSIZE : " + _.size(filterRes));
        if (_.size(filterRes) != 0) {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: null
            });
        } else {
            this.setState({
                dataSource: ds.cloneWithRows(filterRes),
                empty: true
            });
        }

    };

    getData = async (id) => {
        let formData = new FormData();
        /*
        formData.append('username','9114139KP');
        formData.append('password','pesan2gelas');
        */
        console.log('FORM : ' + this.state.user.code + '  ' + this.state.user.token);
        formData.append('id', id);
        //formData.append('password', pass);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" dataku historyCL" + (JSON.stringify(responseJson)));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data_user: responseJson.DATA[0],
                        privilege: responseJson.DATA[0].PRIVILEGE
                    });
                    console.log("data diri " + JSON.stringify(this.state.data_user.ORGANIZATION));
                    this.fetchMyRequest(this.state.user.id);
                    //console.log("DATA AL : ", this.state.data['CURRENT AL'][0].QUOTA)
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    fetchMyRequest = async (id) => {
        let year = new Date().getFullYear();
        let formData = new FormData();
        formData.append('staff', parseInt(id));
        formData.append('year', year);
        console.log(formData);
        fetch(getCL_History(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" MYREQUEST " + ((JSON.stringify(responseJson.DATA[0].CL))));
                this.setState({
                    loaded: true
                });
                let tempHistory = responseJson.DATA[0].CL;
                let historyData = tempHistory.filter((item) => item.IS_EXPIRED == false);
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        data: responseJson.DATA[0],
                        history: historyData,
                        loaded: true
                    });
                }

            })
            .catch((error) => {
                console.error(error);
            });
    };

    doDeleteCL = async (id) => {
        let formData = new FormData();
        formData.append('id', id);
        fetch(deleteCL(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(" DELETE? " + ((JSON.stringify(responseJson))));
                if (responseJson.msg.toLowerCase() == "ok") {
                    this.setState({
                        loaded: false,
                        data: [],
                        history: []
                    });
                    this.fetchMyRequest(this.state.user.id);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    fetchMyName = async () => {
        let formData = new FormData();
        formData.append('id', this.props.staffId);
        fetch(getStaffData(), {
            method: 'POST',
            mode: "no-cors",
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'content-type': 'multipart/form-data',
                'token': this.state.user.token
            },
            body: formData
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                if (responseJson.msg.toLowerCase() == "ok") {
                    if (this.props.privilege.create_own_al != false) {
                        console.log('SIAPA SAYA : ' + JSON.stringify(responseJson.DATA[0].NAME));
                        this.setState({
                            query: responseJson.DATA[0].NAME,
                            myName: responseJson.DATA[0].NAME
                        })
                    } else {
                        this.setState({
                            query: '',
                            myName: responseJson.DATA[0].NAME
                        })
                    }
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };



    constructor(props) {
        super(props);
        this.state = {
            data: {},
            go: 'tt',
            loaded: false,
            history: [],
            dataSource: ds.cloneWithRows(['row 1', 'row 2']),
            resultBox: 50,
            lower: true,
            query: '',
            pickedStaffId: this.props.staffId
        };
    }

    _refresh = () => {
        if (this.state.lower) {
            this.setState({
                loaded: false,
                history: []
            });
            this.fetchMyRequest(this.state.pickedStaffId);
        }
        this.setState({ lower: !this.state.lower });
    };

    numToStringMonth(dateTime) {
        let [date, time] = dateTime.split(' ');
        let [year, month, day] = date.split('-');
        return day + ' ' + months[parseInt(month) - 1] + ' ' + year + ' ' + time;
    }

    _renderList() {
        return (
            <FlatList
                style={{ paddingHorizontal: 10, flex: 1 }}
                contentContainerStyle={{ paddingBottom: 70 }}
                data={this.state.history}
                keyExtractor={(item, index) => item.id}
                renderItem={
                    ({ item }) => (
                        <Card style={{ marginHorizontal: 10, paddingHorizontal: 10 }}>
                            <CardItem cardBody style={{ paddingTop: 20, paddingHorizontal: 10 }}>
                                <Body style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
                                    <Text style={{ fontSize: 13, fontWeight: '500' }}>Creation date : </Text>
                                    <Text>{this.numToStringMonth(item.CREATE_DATE)}</Text>
                                    <Text style={{ fontSize: 13, fontWeight: '500', marginTop: 10 }}>Created by : </Text>
                                    <Text>
                                        {item.CREATE_NAME}
                                    </Text>
                                    <Text style={{ fontSize: 13, fontWeight: '500', marginTop: 10 }}>Remark : </Text>
                                    <Text>{item.REMARK}</Text>
                                    <Text style={{ fontSize: 13, fontWeight: '500', marginTop: 10 }}>Expiry date : </Text>
                                    <Text>{this.numToStringMonth(item.EXPIRED_DATE)}</Text>
                                </Body>
                                <Right style={{ flexDirection: 'column', alignItems: 'center', flex: 0.3 }}>
                                    {
                                        this.props.privilege.insert_cl_all || this.props.privilege.insert_dept_cl ?
                                            <View style={{ position: 'absolute', top: -15, right: -15, justifyContent: 'center', alignItems: 'center' }}>
                                                <TouchableOpacity style={{ padding: 10 }} onPress={() => {
                                                    Alert.alert(
                                                        'Warning',
                                                        "Delete this CL quota?",
                                                        [
                                                            { text: 'DELETE', onPress: () => this.doDeleteCL(item.ID) },
                                                            { text: 'CANCEL' }
                                                        ]
                                                    );
                                                }}>
                                                    <Icon style={{ width: 35, height: 35, fontSize: 25, color: 'white', borderRadius: 17.5, backgroundColor: '#ef5350', textAlignVertical: 'center', textAlign: 'center' }} name="trash" />
                                                </TouchableOpacity>
                                            </View>
                                            :
                                            null
                                    }
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 20 }}>{item.QUOTA}</Text>
                                        <Text style={{ fontSize: 13, fontWeight: '500' }}>days</Text>
                                    </View>
                                </Right>
                            </CardItem>
                            <CardItem footer>
                            </CardItem>
                        </Card>
                    )
                }
            />
        )
    }

    render() {
        return (
            <Container style={{ backgroundColor: '#f8f8f8' }}>
                <Header style={{ backgroundColor: '#a6a6a6', height: 85, paddingTop: 24 }}>
                    <Body>
                        <Title>
                            History
                    </Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => { this.props.navigator.pop() }}>
                            <Icon name="close" style={{ color: '#FFF', padding: 10 }} />
                        </Button>
                    </Right>
                </Header>
                {
                    this.state.lower && (this.props.privilege.insert_cl_all || this.props.privilege.insert_dept_cl) ?
                        <Form style={{ marginTop: 10, paddingRight: 20, marginBottom: 20 }}>
                            <Item style={{ flexDirection: 'column' }}>
                                <Label style={{ alignSelf: 'flex-start' }}>
                                    Applicant
                                </Label>
                                <ScrollView
                                    showsVerticalScrollIndicator={false}
                                    scrollEnabled={false}
                                    keyboardDismissMode='none'
                                    keyboardShouldPersistTaps="handled"
                                    style={{
                                        width: '100%',
                                        borderWidth: 1,
                                        borderColor: '#a6a6a6',
                                        height: this.state.resultBox,
                                    }}
                                >
                                    <View style={{ width: '100%', height: 50 }}>
                                        <Input
                                            onFocus={() => this.setState({ resultBox: 230 })}
                                            onBlur={() => this.setState({ resultBox: 50 })}
                                            style={{ width: '100%', height: '100%' }}
                                            selectTextOnFocus={true}
                                            value={this.state.query}
                                            onChangeText={(text) => {
                                                console.log('textku : ' + text);
                                                this.setState({ query: text });
                                                this.filterName(text);
                                            }}
                                            getRef={(input) => this.searchBox = input}
                                            placeholder="Search"
                                        />
                                    </View>
                                    <View onStartShouldSetResponderCapture={() => {
                                        this.setState({ enableScrollViewScroll: false });
                                        if (this.state.enableScrollViewScroll === false) {
                                            this.setState({ enableScrollViewScroll: true });
                                        }
                                    }}>
                                        {
                                            this.state.empty && this.state.loaded ?
                                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 20 }}>No results found</Text>
                                                </View>
                                                :
                                                !this.state.loaded ?
                                                    <View style={{ width: '100%' }}>
                                                        <Spinner color='blue' />
                                                    </View>
                                                    :
                                                    <ListView
                                                        keyboardShouldPersistTaps='always'
                                                        style={{ height: 180, width: '100%' }}
                                                        enableEmptySections
                                                        dataSource={this.state.dataSource}
                                                        renderRow={(rowData) => (
                                                            <TouchableOpacity
                                                                style={{
                                                                    paddingVertical: 10,
                                                                    borderBottomWidth: 0.2,
                                                                    flexDirection: 'row'
                                                                }}
                                                                onPress={() => {
                                                                    this.setState({
                                                                        query: rowData.NAME,
                                                                        pickedStaffId: rowData.ID,
                                                                        resultBox: 50
                                                                    });
                                                                    Keyboard.dismiss()
                                                                }}
                                                            >
                                                                <Left style={{ flex: 0.3, paddingLeft: 15 }}>
                                                                    <Thumbnail source={{ uri: getPic(rowData.PHOTO) }} />
                                                                </Left>
                                                                <Body>
                                                                    <Text style={{ fontSize: 20, alignSelf: 'flex-start' }}>{rowData.NAME}</Text>
                                                                </Body>

                                                            </TouchableOpacity>
                                                        )}
                                                    />
                                        }
                                    </View>
                                </ScrollView>
                            </Item>
                        </Form>
                        :
                        null
                }
                {
                    this.props.privilege.insert_cl_all || this.props.privilege.insert_dept_cl ?
                        <Button full
                            style={{ backgroundColor: '#ef5350' }}
                            onPress={() => this._refresh()}>
                            {
                                this.state.lower ?
                                    <Text>
                                        Set Staff
                                </Text>
                                    :
                                    <Thumbnail style={{ height: 20, width: 20 }} source={require('../images/down.png')} />
                            }
                        </Button>
                        :
                        null
                }
                <View style={{ flex: 1 }}>
                    {
                        !this.state.loaded ?
                            <View style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                                <Spinner color='blue' />
                            </View>
                            :
                            _.size(this.state.history) != 0 ?
                                this._renderList()
                                :
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Image style={{ width: 100, height: 100, alignSelf: 'center', marginTop: 20, opacity: 0.5 }} source={{ uri: 'http://188.166.242.112/telinhk_dev/frontend/assets/images/empty.png' }} />
                                    <Text style={{ color: '#a6a6a6', fontSize: 15, alignSelf: 'center' }}>
                                        There is nothing to show
                                </Text>
                                </View>
                    }
                </View>
                {
                    console.log('dd ' + JSON.stringify(this.state.data))
                }
                <View style={{ flex: 0.3, flexDirection: 'column', borderTopWidth: 0.2 }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 1, alignItems: 'center', paddingRight: 10 }}>
                            <Text style={{ fontSize: 20, fontWeight: '500' }}>TOTAL</Text>
                        </View>
                        <View style={{ flex: 0.6, paddingLeft: 10, alignItems: 'center' }}>
                            <Text style={{ fontSize: 20 }}>{this.state.data.HAVE}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 1, alignItems: 'center', paddingRight: 10 }}>
                            <Text style={{ fontSize: 20, fontWeight: '500' }}>USED</Text>
                        </View>
                        <View style={{ flex: 0.6, paddingLeft: 10, alignItems: 'center' }}>
                            <Text style={{ fontSize: 20 }}>{this.state.data.USED}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 1, alignItems: 'center', paddingRight: 10 }}>
                            <Text style={{ fontSize: 20, fontWeight: '500' }}>BALANCE</Text>
                        </View>
                        <View style={{ flex: 0.6, paddingLeft: 10, alignItems: 'center' }}>
                            <Text style={{ fontSize: 20, fontWeight: '500' }}>{this.state.data.TOTAL}</Text>
                        </View>
                    </View>
                </View>
            </Container>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ef5350',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

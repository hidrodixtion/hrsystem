package com.hrsystem;

import android.support.annotation.Nullable;

import com.androidnetworking.AndroidNetworking;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.imagepicker.ImagePickerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.reactnativenavigation.NavigationApplication;
import com.rnfs.RNFSPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends NavigationApplication {

  protected List<ReactPackage> getPackages() {
    return Arrays.asList(
            new MainReactPackage(),
            new ImagePickerPackage(),
            new RNFSPackage(),
            new RNDeviceInfo(),
            new HRSystemPackage()
    );
  }

  @Nullable
  @Override
  public String getJSMainModuleName() {
    return "index";
  }

  @Nullable
  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }

  @Override
  public boolean isDebug() {
    return BuildConfig.DEBUG;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);

    AndroidNetworking.initialize(getApplicationContext());
    AndroidNetworking.enableLogging();
  }
}

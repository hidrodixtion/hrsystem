package com.hrsystem.util

import android.content.Context
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import com.facebook.react.bridge.ReactContext

/**
 * Created by adinugroho
 */
object WifiUtil {
    fun getWifiList(context: ReactContext): MutableList<ScanResult>? {
        val wifi = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return wifi.scanResults
    }
}
package com.hrsystem.util

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.ParsedRequestListener
import com.hrsystem.BuildConfig
import com.hrsystem.model.InboxResponse
import com.hrsystem.model.Login
import com.hrsystem.model.StaffResponse
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by adinugroho
 */
object APIUtil {
    private fun login(username: String, password: String, callback: (token: String) -> Unit) {
        AndroidNetworking.post("https://human-resource.telinmy.hash.id/API/auth/login")
                .addBodyParameter("code", username)
                .addBodyParameter("password", password)
                .setTag("login")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(Login::class.java, object: ParsedRequestListener<Login> {
                    override fun onResponse(response: Login?) {
                        response?.let {
                            it.DATA?.let { token ->
                                callback.invoke(token)
                            }
                        }
                    }

                    override fun onError(anError: ANError?) {
                        Log.e("APIUtil - Login", anError?.errorDetail)
                    }
                })
    }

    /**
     * GET STAFF DATA
     * ONLY RETURN THE ID FOR NOW
     */
    private fun getStaffData(username: String, token: String, callback: (ID: String) -> Unit) {
        AndroidNetworking.post("https://human-resource.telinmy.hash.id/API/staff/getStaff")
                .addBodyParameter("code", username)
                .addHeaders("token", token)
                .setTag("staff_data")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(StaffResponse::class.java, object: ParsedRequestListener<StaffResponse> {
                    override fun onResponse(response: StaffResponse?) {
                        response?.let {
                            it.DATA?.let { data ->
                                callback.invoke(data.first().ID)
                            }
                        }
                    }

                    override fun onError(anError: ANError?) {
                        Log.e("APIUtil - GetStaff", anError?.errorDetail)
                    }
                })
    }

    fun sendPresence(bssids: String, username: String, password: String, callback: (() -> Unit)? = null) {
        val now = Date()
        val sdfDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val sdfHour = SimpleDateFormat("HH.mm", Locale.getDefault())

        login(username, password) { token ->
            AndroidNetworking.post("https://human-resource.telinmy.hash.id/API/presence_api/insertpresence")
                    .addHeaders("token", token)
                    .addBodyParameter("macaddress", bssids)
                    .addBodyParameter("date", sdfDate.format(now))
                    .addBodyParameter("hour", sdfHour.format(now))
                    .setTag("presence")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsObject(Login::class.java, object: ParsedRequestListener<Login> {
                        override fun onResponse(response: Login?) {
                            response?.let {
                                Log.v("APIUtil - SendPresence", "Data ${it.DATA}")
                            }
                            callback?.invoke()
                        }

                        override fun onError(anError: ANError?) {
                            Log.e("APIUtil - SendPresence", anError?.errorDetail)
                            callback?.invoke()
                        }
                    })
        }
    }

    fun getNotif(username: String, password: String, callback: (size: Int) -> Unit) {
        login(username, password) { token ->
            getStaffData(username, token) { id ->
                AndroidNetworking.post("https://human-resource.telinmy.hash.id/API/staff/inboxStaff")
                        .addHeaders("token", token)
                        .addBodyParameter("staff", id)
                        .addBodyParameter("action", "al")
                        .addBodyParameter("seen", "false")
                        .setTag("notif")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsObject(InboxResponse::class.java, object: ParsedRequestListener<InboxResponse> {
                            override fun onResponse(response: InboxResponse?) {
                                response?.let {
                                    it.DATA?.let { list ->
                                        callback.invoke(list.size)
                                    }
                                }
                            }

                            override fun onError(anError: ANError?) {
                                Log.e("APIUtil - Inbox", anError?.errorDetail)
                            }
                        })
            }
        }
    }

    fun sendPermission(username: String, password: String, wifiAllowed: Boolean) {
        login(username, password) { token ->
            val params = hashMapOf("device" to "android", "version" to BuildConfig.VERSION_CODE.toString())
            if (wifiAllowed) {
                params["permission"] = "wifi"
            }

            AndroidNetworking.post("https://human-resource.telinmy.hash.id/API/staff/pushPermissionVersion")
                    .addHeaders("token", token)
                    .addBodyParameter(params)
                    .setTag("permission")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(object: JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            Log.v("APIUtil - Permission", params.values.joinToString(", "))
                        }

                        override fun onError(anError: ANError?) {
                            Log.e("APIUtil - Permission", anError?.errorDetail)
                        }
                    })
        }
    }
}
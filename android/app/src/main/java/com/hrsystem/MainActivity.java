package com.hrsystem;

import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.react.modules.core.PermissionListener;
import com.imagepicker.permissions.OnImagePickerPermissionsCallback;
import com.reactnativenavigation.controllers.SplashActivity;

public class MainActivity extends SplashActivity implements OnImagePickerPermissionsCallback {

    @Override
    public View createSplashLayout() {
        LinearLayout view = new LinearLayout(this);
        ImageView iv = new ImageView(view.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(512, 512);
        iv.setLayoutParams(layoutParams);


        view.setGravity(Gravity.CENTER);
        //iv.setScaleType(ImageView.ScaleType.CENTER);
        iv.setAdjustViewBounds(false);
        iv.setImageResource(R.drawable.splash);

        view.addView(iv);
        return view;
    }

    private PermissionListener listener;

    @Override
    public void setPermissionListener(@NonNull PermissionListener listener) {
        this.listener = listener;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (listener != null) {
            listener.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}

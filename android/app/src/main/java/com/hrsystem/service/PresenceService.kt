package com.hrsystem.service

import android.app.*
import android.content.Intent
import android.content.Context
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.hrsystem.R

class PresenceService : Service() {

    val notifId = 1
    val notifClass = NotificationClass()

    companion object {
        const val ACTION_STOP_FOREGROUND = "stop_foreground"
        const val ACTION_START_FOREGROUND = "start_foreground"

        // NEEDS TO BE INITIALISED
        var smallIconDrawable = R.mipmap.ic_launcher
        var notifTitle: String = "HRSystem"
        var notifContent: String = "AutoPresence is running..."
        val channelId = "channel_hrsystem_high"
        val channelName = "presence"

        lateinit var contentIntent: PendingIntent
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()
    }

    override fun onStartCommand(anIntent: Intent?, flags: Int, startId: Int): Int {
        val intent = anIntent ?: return START_NOT_STICKY
        val action = intent.action ?: return START_NOT_STICKY

        when (action) {
            ACTION_STOP_FOREGROUND -> {
                Log.v("COMMAND", "STOP FOREGROUND")
                stopSelf()
                stopForeground(true)
                removeAllNotification()
            }
            ACTION_START_FOREGROUND -> {
                initForegroundService()
            }
        }

        return START_NOT_STICKY
    }

    private fun initForegroundService() {
        val notif: Notification
        val notifBuilder = NotificationCompat.Builder(applicationContext, channelId)
                .setSmallIcon(smallIconDrawable)
                .setContentTitle(notifTitle)
                .setContentText(notifContent)
                .setContentIntent(contentIntent)
                .setOngoing(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notifMan = getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW)
            notifBuilder.setChannelId(channelId)
            notifMan?.createNotificationChannel(channel)

            notif = notifBuilder.build()
            notifMan?.notify(notifId, notif)
        } else {
            notif = notifBuilder.build()
        }

        startForeground(1, notif)
    }

    private fun removeAllNotification() {
        val notifMan = getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
        notifMan?.cancel(1)
    }
}

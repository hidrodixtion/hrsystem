package com.hrsystem.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi

/**
 * Created by adinugroho
 */
class NotificationClass {
    @get:RequiresApi(Build.VERSION_CODES.O)
    val mainNotificationId = "123"
    private val CHANNEL_NAME = "Your human readable notification channel name"
    private val CHANNEL_DESCRIPTION = "description"

    @RequiresApi(Build.VERSION_CODES.O)
    fun createMainNotificationChannel(c: Context) {
        val id = mainNotificationId
        val name = CHANNEL_NAME
        val description = CHANNEL_DESCRIPTION
        val importance = NotificationManager.IMPORTANCE_LOW
        val mChannel = NotificationChannel(id, name, importance)
        mChannel.description = description
        mChannel.enableLights(true)
        mChannel.lightColor = Color.RED
        mChannel.enableVibration(true)
        val mNotificationManager = c.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // NotificationManager mNotificationManager = c.getSystemService(Context.NOTIFICATION_SERVICE) as android.app.NotificationManager
        mNotificationManager.createNotificationChannel(mChannel)


    }
}
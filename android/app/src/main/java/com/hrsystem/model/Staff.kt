package com.hrsystem.model

/**
 * Created by adinugroho
 */
data class Staff(
        val ID: String,
        val CODE: String
)

data class StaffResponse(
        val msg: String,
        val DATA: List<Staff>?
)
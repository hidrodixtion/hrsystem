package com.hrsystem.model

/**
 * Created by adinugroho
 */
data class Inbox(
        val ID: String,
        val STATUS: String,
        val DO: String
)

data class InboxResponse(
    val msg: String,
    val DATA: List<Inbox>?
)
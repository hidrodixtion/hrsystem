package com.hrsystem

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Handler
import android.preference.PreferenceManager
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.getSystemService
import android.view.View
import com.facebook.react.bridge.Callback
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.hrsystem.service.PresenceService
import com.hrsystem.util.APIUtil
import com.hrsystem.util.WifiUtil
import java.util.*
import kotlin.concurrent.fixedRateTimer

/**
 * Created by adinugroho
 */
class HRSystemModule(private val reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
    override fun getName(): String = "HRSystemModule"

    lateinit var preferences: SharedPreferences
    lateinit var timer: Timer

    private fun initPref() {
        preferences = PreferenceManager.getDefaultSharedPreferences(reactContext)
    }

    @ReactMethod
    fun saveLoginDetail(username: String, password: String) {
        initPref()

        preferences.edit().putString("username", username).apply()
        preferences.edit().putString("password", password).apply()
    }

    @ReactMethod
    fun saveStaffData(id: String) {
        initPref()

        preferences.edit().putString("staffId", id).apply()
    }

    @ReactMethod
    fun getLoginDetail(callback: Callback) {
        initPref()

        val username = preferences.getString("username", "")
        val password = preferences.getString("password", "")
        callback.invoke(username, password)
    }

    @ReactMethod
    fun getWifiList(callback: Callback) {
        val list = WifiUtil.getWifiList(reactContext)
        list?.let {
            callback.invoke(list.map { wifi -> wifi.BSSID }.joinToString(";"))
        }
    }

    @ReactMethod
    fun spawnService() {
        initPref()

        val pendingIntent = PendingIntent.getActivity(reactContext, 0, Intent(reactContext, MainActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT)
        PresenceService.contentIntent = pendingIntent

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            reactContext.startForegroundService(Intent(reactContext, PresenceService::class.java).apply {
                action = PresenceService.ACTION_START_FOREGROUND
            })
        } else {
            reactContext.startService(Intent(reactContext, PresenceService::class.java).apply {
                action = PresenceService.ACTION_START_FOREGROUND
            })
        }

        timer = fixedRateTimer("timer", false, 0, 300 * 1_000) {
            val list = WifiUtil.getWifiList(reactContext)
            list?.let { results ->
                val username = preferences.getString("username", "")!!
                val password = preferences.getString("password", "")!!
                val bssids = results.joinToString(";", transform = { it.BSSID })

                APIUtil.sendPresence(bssids, username, password)
                APIUtil.getNotif(username, password) { notifSize ->
                    createNotif(notifSize)
                }
                APIUtil.sendPermission(username, password, ContextCompat.checkSelfPermission(reactContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            }
        }
    }

    @ReactMethod
    fun sendPresence(callback: Callback) {
        val list = WifiUtil.getWifiList(reactContext)
        list?.let { results ->
            val username = preferences.getString("username", "")!!
            val password = preferences.getString("password", "")!!
            val bssids = results.joinToString(";", transform = { it.BSSID })

            APIUtil.sendPresence(bssids, username, password) {
                callback.invoke()
            }

            APIUtil.sendPermission(username, password, ContextCompat.checkSelfPermission(reactContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        }
    }

    @ReactMethod
    fun stopService() {
        if (::timer.isInitialized)
            timer.cancel()

        val intent = Intent(reactContext, PresenceService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            intent.action = PresenceService.ACTION_STOP_FOREGROUND
            reactContext.startService(intent)
        } else {
            reactContext.stopService(intent)
        }
    }

    private fun createNotif(notifSize: Int) {
        val channelId = "channel_hrsystem"
        val channelName = "notif"

        val pendingIntent = PendingIntent.getActivity(reactContext, 0, Intent(reactContext, MainActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT)
        val notifMan = reactContext.getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
        val notifBuilder = NotificationCompat.Builder(reactContext, channelId)
                .setSmallIcon(PresenceService.smallIconDrawable)
                .setContentTitle(PresenceService.notifTitle)
                .setContentText("You have $notifSize unread notification")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            notifBuilder.setChannelId(channelId)
            notifMan?.createNotificationChannel(channel)
        }

        val notification = notifBuilder.build()
        notifMan?.notify(2, notification)
    }
}
/**
 * @format
 */

import {
    AppRegistry,
    AsyncStorage,
    Platform,
    PermissionsAndroid
} from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { getStaffData, getAnnouncement, getPic, getNotif, getCL, getLogin, getAL, getQuota, submitAbsence, getAbsence } from "./screens/API";
import _ from 'lodash';
import moment from 'moment';

AppRegistry.registerComponent(appName, () => App);

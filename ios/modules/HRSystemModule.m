//
//  HRSystemModule.m
//  hrsystem
//
//  Created by Adi Nugroho on 17/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "React/RCTBridgeModule.h"

@interface RCT_EXTERN_MODULE(HRSystemModule, NSObject)
//RCT_EXTERN_METHOD(returnSomething:(NSString)name callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(saveLoginDetail:(NSString)name password:(NSString)password)
RCT_EXTERN_METHOD(saveStaffData:(NSString)staffId)
RCT_EXTERN_METHOD(spawnService)
RCT_EXTERN_METHOD(stopService)
RCT_EXTERN_METHOD(sendPresence:(RCTResponseSenderBlock)callback)
@end

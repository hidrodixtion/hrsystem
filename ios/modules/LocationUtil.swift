//
//  LocationUtil.swift
//  hrsystem
//
//  Created by Adi Nugroho on 10/09/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftDate

class LocationUtil: NSObject, CLLocationManagerDelegate {
  static let shared = LocationUtil()
  let locationMan = CLLocationManager()
  
  var postPermission = false
  var isPositionUploaded = false
  var lastCoordinate: CLLocationCoordinate2D?
  var lastUpdated = Date() - 6.minutes
  
  private func requestLocation() {
    locationMan.delegate = self
    locationMan.desiredAccuracy = kCLLocationAccuracyBest
    locationMan.requestAlwaysAuthorization()
    locationMan.allowsBackgroundLocationUpdates = true
  }
  
  func sendPresence() {
    print("presence")
    requestLocation()
  }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    print("auth status: \(status)")
    if (status == .authorizedAlways) {
      APIUtil.shared.sendPermissionInfo(locationAuthorized: true)
      locationMan.startUpdatingLocation()
    } else {
      APIUtil.shared.sendPermissionInfo(locationAuthorized: false)
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.last else {
      print("no last location")
      return
    }
    
    lastCoordinate = location.coordinate
    print("coordinate: \(lastCoordinate)")

    let over5Minutes = ((Date() - lastUpdated).minute ?? 0) > 2
    if (over5Minutes) {
      lastUpdated = Date()
      print(lastUpdated)
      if let lat = lastCoordinate?.latitude, let long = lastCoordinate?.longitude {
        APIUtil.shared.sendPresence(lat: lat, long: long)
      }
    }
  }
}

//
//  HRSystemModule.swift
//  hrsystem
//
//  Created by Adi Nugroho on 17/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import Alamofire
import UserNotifications
import Defaults
import UIKit

@objc(HRSystemModule)
class HRSystemModule : NSObject {
  
  @objc
  func saveLoginDetail(_ name: String, password: String) {
    print("name + pass", name, password)
    
    Defaults[.username] = name
    Defaults[.password] = password
  }
  
  @objc
  func saveStaffData(_ staffId: String) {
    Defaults[.staffId] = staffId
    print("staff id \(staffId)")
  }
  
  @objc
  func getWifiList(_ callback: RCTResponseSenderBlock) {
    
  }
  
  @objc
  func spawnService() {
    DispatchQueue.main.async {
      UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
      LocationUtil.shared.sendPresence()
    }
    
    print("Spawn Service")
    activateService()
  }
  
  @objc
  func stopService() {
    DispatchQueue.main.async {
      UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalNever)
    }
    print("Stop Service")
  }
  
  @objc
  func sendPresence(_ callback: @escaping RCTResponseSenderBlock) {
    print("last coordinate: \(LocationUtil.shared.lastCoordinate)")
    guard let coordinate = LocationUtil.shared.lastCoordinate else {
      callback(nil)
      return
    }
    
    APIUtil.shared.sendPresence(lat: coordinate.latitude, long: coordinate.longitude) {
      callback(nil)
    }
  }
  
  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
  @objc
  func activateService() {
    // get notifications and shows them
    APIUtil.shared.getNotification { notifCount in
      print("notification: \(notifCount)")
      if (notifCount > 0) {
        APIUtil.shared.createNewNotification(title: "Notif", body: "You have \(notifCount) notifications.")
      }
    }
    
    // send last presence (this will automatically send the permission too)
    guard let coordinate = LocationUtil.shared.lastCoordinate else {
      return
    }
    
    APIUtil.shared.sendPresence(lat: coordinate.latitude, long: coordinate.longitude)
    LocationUtil.shared.lastUpdated = Date()
  }
}

//
//  Ext+Defaults.swift
//  hrsystem
//
//  Created by Adi Nugroho on 20/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import Defaults

extension Defaults.Keys {
  static let username = Key<String>("username", default: "")
  static let password = Key<String>("password", default: "")
  static let staffId = Key<String>("staffId", default: "")
}

//
//  APIUtil.swift
//  hrsystem
//
//  Created by Adi Nugroho on 09/09/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import Alamofire
import Defaults
import UserNotifications

class APIUtil {
  static let shared = APIUtil()
  
//  private let baseUrl = "https://human-resource.telinmy.hash.id/API"
  private let baseUrl = "https://hr.telin.com.my/API"
  private var token = ""
  
  /// Get Username & Password
  private func getUsernamePass() -> (username: String, password: String)? {
    let username = Defaults[.username]
    let password = Defaults[.password]
    
    if (username.isEmpty) {
      return nil
    }

    return (username, password)
  }
  
  /// Login
  func login(onLoginSuccess: @escaping (String) -> Void) {
    print("username, pass: \(getUsernamePass())")
    guard let userpass = getUsernamePass() else {
      return
    }
    
    let params = [
      "code": userpass.username,
      "password": userpass.password
    ]
    print("login params \(params)")
    AF.request("\(baseUrl)/auth/login", method: .post, parameters: params, encoder: URLEncodedFormParameterEncoder.default).responseJSON { [unowned self] (response) in
      switch response.result {
      case .success(let data):
        let json = JSON(data)
        print("login: \(json)")
        if let token = json["DATA"].string {
          self.token = token
          onLoginSuccess(token)
        }
      case .failure(let error):
        print(error.localizedDescription)
      }
    }
  }
  
  /// Get Staff Data
  func getStaffData(onOpSuccess: @escaping (JSON?) -> Void) {
    guard let userpass = getUsernamePass() else {
      return
    }
    
    let params = [
      "code": userpass.username
    ]
    login { [unowned self] (token) in
      let headers: HTTPHeaders = [.init(name: "token", value: token)]
      AF.request("\(self.baseUrl)/staff/getStaff", method: .post, parameters: params, encoder: URLEncodedFormParameterEncoder.default, headers: headers).responseJSON { (response) in
        switch response.result {
        case .success(let data):
          let staffData = JSON(data)
          onOpSuccess(staffData["DATA"])
        case .failure(let error):
          print(error.localizedDescription)
        }
      }
    }
  }
  
  /// Get notification
  func getNotification(onOpSuccess: @escaping (Int) -> Void) {
    getStaffData { [unowned self] (json) in
//      print("staff: \(json)")
      guard let json = json else {
        return
      }
      
      let params = [
        "staff": json[0]["ID"].stringValue,
        "action": "al",
        "seen": "false"
      ]
      
      print(params)
      let headers: HTTPHeaders = [.init(name: "token", value: self.token)]
      
      AF.request("\(self.baseUrl)/staff/inboxStaff", method: .post, parameters: params, encoder: URLEncodedFormParameterEncoder.default, headers: headers).responseJSON { (response) in
        switch response.result {
        case .success(let value):
          print(value)
          onOpSuccess(JSON(value)["DATA"].array?.count ?? 0)
        case .failure(let error):
          print(error.localizedDescription)
        }
      }
    }
  }
  
  /// Send Presence
  func sendPresence(lat: Double, long: Double, callback: (() -> Void)? = nil) {
    print("sending presence. Lat: \(lat), Long: \(long)")
    login { (token) in
      let params = [
        "lat": lat,
        "lon": long
      ]
      print(params)
      let headers: HTTPHeaders = [.init(name: "token", value: token)]
      AF.request("\(self.baseUrl)/presence_api/locationbasedPresence", method: .post, parameters: params, encoder: URLEncodedFormParameterEncoder.default, headers: headers).responseJSON { (response) in
        switch response.result {
        case .success(let value):
          callback?()
          print("presence: \(value)")
        case .failure(let error):
          print(error.localizedDescription)
          callback?()
        }
      }
    }
  }
  
  /// Send Permission info
  func sendPermissionInfo(locationAuthorized: Bool) {
    login { (token) in
      let appVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
      
      var params = [
        "device": "ios",
        "version": appVersion ?? ""
      ]
      
      if (locationAuthorized) {
        params["permission"] = "location"
      }
      
      let headers: HTTPHeaders = [.init(name: "token", value: token)]
      AF.request("\(self.baseUrl)/staff/pushPermissionVersion", method: .post, parameters: params, encoder: URLEncodedFormParameterEncoder.default, headers: headers).responseJSON { (response) in
        switch response.result {
        case .success(_):
          print("permission pushed")
        case .failure(let error):
          print(error.localizedDescription)
        }
      }
    }
  }
  
  /// Create new notification
  func createNewNotification(title: String, body: String) {
    let notificationCenter = UNUserNotificationCenter.current()
    
    notificationCenter.getNotificationSettings { (settings) in
      if (settings.authorizationStatus == .denied) {
        
      } else {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let identifier = "Local.Notif"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
          if let error = error {
            print(error.localizedDescription)
          }
        }
      }
    }
  }
}
